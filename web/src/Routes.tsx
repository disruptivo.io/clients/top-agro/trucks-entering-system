// In this file, all Page components from 'src/pages` are auto-imported. Nested
// directories are supported, and should be uppercase. Each subdirectory will be
// prepended onto the component name.
//
// Examples:
//
// 'src/pages/HomePage/HomePage.js'         -> HomePage
// 'src/pages/Admin/BooksPage/BooksPage.js' -> AdminBooksPage

import { Set, Router, Route, Private } from '@redwoodjs/router'

import MainLayout from 'src/layouts/MainLayout'
import RegistrationLayout from 'src/layouts/RegistrationLayout/RegistrationLayout'

const Routes = () => {
  return (
    <Router>
      <Set wrap={MainLayout}>
        <Route path="/forbidden" page={ForbiddenPage} name="forbidden" />
      </Set>
      <Private unauthenticated="forbidden" roles={['admin', 'doorman']}>
        <Set wrap={MainLayout}>
          <Route path="/new" page={EnteringNewEnteringPage} name="newEntering" />
          <Route path="/{id:Int}/edit" page={EnteringEditEnteringPage} name="editEntering" />
          <Route path="/{id:Int}" page={EnteringEnteringPage} name="entering" />
          <Route path="/" page={EnteringEnteringsPage} name="enterings" />
        </Set>
      </Private>
      <Private unauthenticated="forbidden" roles={['admin', 'coordinator']}>
        <Set wrap={MainLayout}>
          <Route path="/trucks/new" page={TruckNewTruckPage} name="newTruck" />
          <Route path="/trucks/{id:Int}/edit" page={TruckEditTruckPage} name="editTruck" />
          <Route path="/trucks/{id:Int}" page={TruckTruckPage} name="truck" />
          <Route path="/trucks" page={TruckTrucksPage} name="trucks" />
        </Set>
      </Private>
      <Private unauthenticated="forbidden" roles={['admin', 'coordinator']}>
        <Set wrap={MainLayout}>
          <Route path="/drivers/new" page={DriverNewDriverPage} name="newDriver" />
          <Route path="/drivers/{id:Int}/edit" page={DriverEditDriverPage} name="editDriver" />
          <Route path="/drivers/{id:Int}" page={DriverDriverPage} name="driver" />
          <Route path="/drivers" page={DriverDriversPage} name="drivers" />
        </Set>
      </Private>
      <Private unauthenticated="forbidden" roles={['admin', 'coordinator']}>
        <Set wrap={MainLayout}>
          <Route path="/users/new" page={UserNewUserPage} name="newUser" />
          <Route path="/users/{id:Int}/edit" page={UserEditUserPage} name="editUser" />
          <Route path="/users/{id:Int}" page={UserUserPage} name="user" />
          <Route path="/users" page={UserUsersPage} name="users" />
        </Set>
      </Private>
      <Private unauthenticated="forbidden" roles={['admin', 'coordinator']}>
        <Set wrap={MainLayout} unauthenticated="login">
          <Route path="/organizations/new" page={OrganizationNewOrganizationPage} name="newOrganization" />
          <Route path="/organizations/{id:Int}/edit" page={OrganizationEditOrganizationPage} name="editOrganization" />
          <Route path="/organizations/{id:Int}" page={OrganizationOrganizationPage} name="organization" />
          <Route path="/organizations" page={OrganizationOrganizationsPage} name="organizations" />
        </Set>
      </Private>
      <Set wrap={RegistrationLayout}>
        <Route path="/login" page={LoginPage} name="login" />
        <Route path="/signup" page={SignupPage} name="signup" />
        <Route path="/forgot-password" page={ForgotPasswordPage} name="forgotPassword" />
        <Route path="/reset-password" page={ResetPasswordPage} name="resetPassword" />
      </Set>
      <Route notfound page={NotFoundPage} />
    </Router>
  )
}

export default Routes
