import { useParams } from '@redwoodjs/router'

import OrganizationsCell from 'src/components/Organization/OrganizationsCell'
import { config } from 'src/config/config'

const OrganizationsPage = () => {
  const params = useParams()
  const page = parseInt(params.page) || config.pagination.page
  const size = parseInt(params.size) || config.pagination.size

  const getEnabledValue = () => {
    if (params.enabled) return params.enabled === 'true'

    return true
  }

  return (
    <OrganizationsCell
      page={page}
      size={size}
      filter={params.filter}
      startDate={params.startDate}
      endDate={params.endDate}
      enabled={getEnabledValue()}
    />
  )
}

export default OrganizationsPage
