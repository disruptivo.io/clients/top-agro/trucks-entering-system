import { useEffect, useRef, useState } from 'react'

import { useTranslation } from 'react-i18next'

import { useAuth } from '@redwoodjs/auth'
import {
  Form,
  Label,
  PasswordField,
  Submit,
  FieldError,
} from '@redwoodjs/forms'
import { navigate, routes } from '@redwoodjs/router'
import { MetaTags } from '@redwoodjs/web'
import { toast } from '@redwoodjs/web/toast'

const ResetPasswordPage = ({ resetToken }: { resetToken: string }) => {
  const { t } = useTranslation()
  const { isAuthenticated, reauthenticate, validateResetToken, resetPassword } =
    useAuth()
  const [enabled, setEnabled] = useState(true)

  useEffect(() => {
    if (isAuthenticated) {
      navigate(routes.home())
    }
  }, [isAuthenticated])

  useEffect(() => {
    const validateToken = async () => {
      const response = await validateResetToken(resetToken)
      if (response.error) {
        setEnabled(false)
        toast.error(response.error)
      } else {
        setEnabled(true)
      }
    }
    validateToken()
  }, [])

  const passwordRef = useRef<HTMLInputElement>(null)
  useEffect(() => {
    passwordRef.current?.focus()
  }, [])

  const onSubmit = async (data: Record<string, string>) => {
    const response = await resetPassword({
      resetToken,
      password: data.password,
    })

    if (response.error) {
      toast.error(response.error)
    } else {
      toast.success(t('resetPasswordPage.txt1'))
      await reauthenticate()
      navigate(routes.login())
    }
  }

  return (
    <>
      <MetaTags title="Reset Password" />

      <div className="rw-segment w-2/6">
        <header className="rw-segment-header">
          <h2 className="rw-heading rw-heading-secondary">
            {t('resetPasswordPage.txt2')}
          </h2>
        </header>

        <div className="rw-segment-main">
          <div className="rw-form-wrapper">
            <Form onSubmit={onSubmit} className="rw-form-wrapper">
              <div className="text-left">
                <Label
                  name="password"
                  className="rw-label"
                  errorClassName="rw-label rw-label-error"
                >
                  {t('resetPasswordPage.txt3')}
                </Label>
                <PasswordField
                  name="password"
                  autoComplete="new-password"
                  className="rw-input"
                  errorClassName="rw-input rw-input-error"
                  disabled={!enabled}
                  ref={passwordRef}
                  validation={{
                    required: {
                      value: true,
                      message: t('resetPasswordPage.txt4'),
                    },
                  }}
                />

                <FieldError name="password" className="rw-field-error" />
              </div>

              <div className="rw-button-group">
                <Submit
                  className="rw-button rw-button-blue"
                  disabled={!enabled}
                >
                  {t('resetPasswordPage.txt5')}
                </Submit>
              </div>
            </Form>
          </div>
        </div>
      </div>
    </>
  )
}

export default ResetPasswordPage
