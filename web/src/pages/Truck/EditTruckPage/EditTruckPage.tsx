import EditTruckCell from 'src/components/Truck/EditTruckCell'

type TruckPageProps = {
  id: number
}

const EditTruckPage = ({ id }: TruckPageProps) => {
  return <EditTruckCell id={id} />
}

export default EditTruckPage
