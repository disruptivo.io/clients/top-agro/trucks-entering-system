import TruckCell from 'src/components/Truck/TruckCell'

type TruckPageProps = {
  id: number
}

const TruckPage = ({ id }: TruckPageProps) => {
  return <TruckCell id={id} />
}

export default TruckPage
