import { useParams } from '@redwoodjs/router'

import TrucksCell from 'src/components/Truck/TrucksCell'
import { config } from 'src/config/config'

const TrucksPage = () => {
  const params = useParams()
  const page = parseInt(params.page) || config.pagination.page
  const size = parseInt(params.size) || config.pagination.size

  const getEnabledValue = () => {
    if (params.enabled) return params.enabled === 'true'

    return true
  }

  return (
    <TrucksCell
      page={page}
      size={size}
      filter={params.filter}
      startDate={params.startDate}
      endDate={params.endDate}
      enabled={getEnabledValue()}
    />
  )
}

export default TrucksPage
