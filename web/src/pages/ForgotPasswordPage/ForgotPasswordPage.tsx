import { useEffect, useRef } from 'react'

import { useTranslation } from 'react-i18next'

import { useAuth } from '@redwoodjs/auth'
import { Form, Label, TextField, Submit, FieldError } from '@redwoodjs/forms'
import { navigate, routes } from '@redwoodjs/router'
import { MetaTags } from '@redwoodjs/web'
import { toast } from '@redwoodjs/web/toast'

const ForgotPasswordPage = () => {
  const { t } = useTranslation()
  const { isAuthenticated, forgotPassword } = useAuth()

  useEffect(() => {
    if (isAuthenticated) {
      navigate(routes.home())
    }
  }, [isAuthenticated])

  const usernameRef = useRef<HTMLInputElement>(null)
  useEffect(() => {
    usernameRef?.current?.focus()
  }, [])

  const onSubmit = async (data: { username: string }) => {
    const response = await forgotPassword(data.username)

    if (response.error) {
      toast.error(response.error)
    } else {
      // The function `forgotPassword.handler` in api/src/functions/auth.js has
      // been invoked, let the user know how to get the link to reset their
      // password (sent in email, perhaps?)
      toast.success(t('forgotPasswordPage.txt1', { email: data.username }))
      navigate(routes.login())
    }
  }

  return (
    <>
      <MetaTags title="Forgot Password" />

      <div className="rw-segment w-2/6">
        <header className="rw-segment-header">
          <h2 className="rw-heading rw-heading-secondary">
            {t('forgotPasswordPage.txt2')}
          </h2>
        </header>

        <div className="rw-segment-main">
          <div className="rw-form-wrapper">
            <Form onSubmit={onSubmit} className="rw-form-wrapper">
              <div className="text-left">
                <Label
                  name="username"
                  className="rw-label"
                  errorClassName="rw-label rw-label-error"
                >
                  {t('forgotPasswordPage.txt3')}
                </Label>
                <TextField
                  name="username"
                  className="rw-input"
                  errorClassName="rw-input rw-input-error"
                  ref={usernameRef}
                  validation={{
                    required: {
                      value: true,
                      message: t('forgotPasswordPage.txt4'),
                    },
                  }}
                />

                <FieldError name="username" className="rw-field-error" />
              </div>

              <div className="rw-button-group">
                <Submit className="rw-button rw-button-blue">
                  {t('forgotPasswordPage.txt5')}
                </Submit>
              </div>
            </Form>
          </div>
        </div>
      </div>
    </>
  )
}

export default ForgotPasswordPage
