import { useRef } from 'react'
import { useEffect } from 'react'

import { useTranslation } from 'react-i18next'

import { useAuth } from '@redwoodjs/auth'
import {
  Form,
  Label,
  TextField,
  PasswordField,
  FieldError,
  Submit,
} from '@redwoodjs/forms'
import { Link, navigate, routes } from '@redwoodjs/router'
import { MetaTags } from '@redwoodjs/web'
import { toast } from '@redwoodjs/web/toast'

const SignupPage = () => {
  const { t } = useTranslation()
  const { isAuthenticated, signUp } = useAuth()

  useEffect(() => {
    if (isAuthenticated) {
      navigate(routes.organizations())
    }
  }, [isAuthenticated])

  // focus on email box on page load
  const usernameRef = useRef<HTMLInputElement>(null)
  useEffect(() => {
    usernameRef.current?.focus()
  }, [])

  const onSubmit = async (data: Record<string, string>) => {
    const response = await signUp({ ...data })

    if (response.message) {
      toast(response.message)
    } else if (response.error) {
      toast.error(response.error)
    } else {
      // user is signed in automatically
      toast.success(t('signupPage.txt1'))
    }
  }

  return (
    <>
      <MetaTags title="Signup" />

      <div className="rw-segment w-2/6">
        <header className="rw-segment-header">
          <h2 className="rw-heading rw-heading-secondary">
            {t('signupPage.txt2')}
          </h2>
        </header>

        <div className="rw-segment-main">
          <div className="rw-form-wrapper">
            <Form onSubmit={onSubmit} className="rw-form-wrapper">
              <Label
                name="firstName"
                className="rw-label"
                errorClassName="rw-label rw-label-error"
              >
                {t('signupPage.txt3')}
              </Label>
              <TextField
                name="firstName"
                className="rw-input"
                errorClassName="rw-input rw-input-error"
                validation={{
                  required: {
                    value: true,
                    message: t('signupPage.txt4'),
                  },
                }}
              />
              <FieldError name="firstName" className="rw-field-error" />

              <Label
                name="lastName"
                className="rw-label"
                errorClassName="rw-label rw-label-error"
              >
                {t('signupPage.txt5')}
              </Label>
              <TextField
                name="lastName"
                className="rw-input"
                errorClassName="rw-input rw-input-error"
                validation={{
                  required: {
                    value: true,
                    message: t('signupPage.txt6'),
                  },
                }}
              />
              <FieldError name="lastName" className="rw-field-error" />

              <Label
                name="username"
                className="rw-label"
                errorClassName="rw-label rw-label-error"
              >
                {t('signupPage.txt7')}
              </Label>
              <TextField
                name="username"
                className="rw-input"
                errorClassName="rw-input rw-input-error"
                ref={usernameRef}
                validation={{
                  required: {
                    value: true,
                    message: t('signupPage.txt8'),
                  },
                }}
              />
              <FieldError name="username" className="rw-field-error" />

              <Label
                name="password"
                className="rw-label"
                errorClassName="rw-label rw-label-error"
              >
                {t('signupPage.txt9')}
              </Label>
              <PasswordField
                name="password"
                className="rw-input"
                errorClassName="rw-input rw-input-error"
                autoComplete="current-password"
                validation={{
                  required: {
                    value: true,
                    message: t('signupPage.txt10'),
                  },
                }}
              />
              <FieldError name="password" className="rw-field-error" />

              <div className="rw-button-group">
                <Submit className="rw-button rw-button-blue">
                  {t('signupPage.txt11')}
                </Submit>
              </div>
            </Form>
          </div>
        </div>
      </div>
      <div className="rw-login-link basis-0">
        <span>{t('signupPage.txt12')}</span>{' '}
        <Link to={routes.login()} className="rw-link">
          {t('signupPage.txt13')}
        </Link>
      </div>
    </>
  )
}

export default SignupPage
