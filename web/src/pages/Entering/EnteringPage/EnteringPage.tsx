import EnteringCell from 'src/components/Entering/EnteringCell'

type EnteringPageProps = {
  id: number
}

const EnteringPage = ({ id }: EnteringPageProps) => {
  return <EnteringCell id={id} />
}

export default EnteringPage
