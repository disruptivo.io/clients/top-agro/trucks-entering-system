import EditEnteringCell from 'src/components/Entering/EditEnteringCell'

type EnteringPageProps = {
  id: number
}

const EditEnteringPage = ({ id }: EnteringPageProps) => {
  return <EditEnteringCell id={id} />
}

export default EditEnteringPage
