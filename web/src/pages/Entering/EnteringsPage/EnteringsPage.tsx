import { useParams } from '@redwoodjs/router'

import EnteringsCell from 'src/components/Entering/EnteringsCell'
import { config } from 'src/config/config'

const EnteringsPage = () => {
  const params = useParams()
  const page = parseInt(params.page) || config.pagination.page
  const size = parseInt(params.size) || config.pagination.size

  return (
    <EnteringsCell
      page={page}
      size={size}
      filter={params.filter}
      startDate={params.startDate}
      endDate={params.endDate}
    />
  )
}

export default EnteringsPage
