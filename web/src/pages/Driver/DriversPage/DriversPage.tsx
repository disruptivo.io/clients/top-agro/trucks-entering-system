import { useParams } from '@redwoodjs/router'

import DriversCell from 'src/components/Driver/DriversCell'
import { config } from 'src/config/config'

const DriversPage = () => {
  const params = useParams()
  const page = parseInt(params.page) || config.pagination.page
  const size = parseInt(params.size) || config.pagination.size

  const getEnabledValue = () => {
    if (params.enabled) return params.enabled === 'true'

    return true
  }

  return (
    <DriversCell
      page={page}
      size={size}
      filter={params.filter}
      startDate={params.startDate}
      endDate={params.endDate}
      enabled={getEnabledValue()}
    />
  )
}

export default DriversPage
