import { useRef, useState, useEffect } from 'react'

import { useTranslation } from 'react-i18next'

import { useAuth } from '@redwoodjs/auth'
import {
  Form,
  Label,
  TextField,
  PasswordField,
  Submit,
  FieldError,
} from '@redwoodjs/forms'
import { Link, navigate, routes } from '@redwoodjs/router'
import { MetaTags } from '@redwoodjs/web'
import { toast } from '@redwoodjs/web/toast'

const WELCOME_MESSAGE = 'loginPage.txt1'

const LoginPage = ({ type }) => {
  const {
    isAuthenticated,
    client: webAuthn,
    loading,
    logIn,
    reauthenticate,
    hasRole,
  } = useAuth()
  const { t } = useTranslation()
  const [shouldShowWebAuthn, setShouldShowWebAuthn] = useState(false)
  const [showWebAuthn, setShowWebAuthn] = useState(
    webAuthn.isEnabled() && type !== 'password'
  )

  const redirect = () => {
    if (hasRole(['admin', 'doorman'])) return navigate(routes.enterings())

    navigate(routes.drivers())
  }

  // should redirect right after login or wait to show the webAuthn prompts?
  useEffect(() => {
    if (isAuthenticated && (!shouldShowWebAuthn || webAuthn.isEnabled())) {
      redirect()
    }
  }, [isAuthenticated, shouldShowWebAuthn])

  // if WebAuthn is enabled, show the prompt as soon as the page loads
  useEffect(() => {
    if (!loading && !isAuthenticated && showWebAuthn) {
      onAuthenticate()
    }
  }, [loading, isAuthenticated])

  // focus on the username field as soon as the page loads
  const usernameRef = useRef()
  useEffect(() => {
    usernameRef.current && usernameRef.current.focus()
  }, [])

  const onSubmit = async (data) => {
    const webAuthnSupported = await webAuthn.isSupported()

    if (webAuthnSupported) {
      setShouldShowWebAuthn(true)
    }
    const response = await logIn({ ...data })

    if (response.message) {
      // auth details good, but user not logged in
      toast(response.message)
    } else if (response.error) {
      // error while authenticating
      toast.error(response.error)
    } else {
      // user logged in
      if (webAuthnSupported && webAuthn.isEnabled()) {
        setShowWebAuthn(true)
      } else {
        redirect()
        toast.success(t(WELCOME_MESSAGE))
      }
    }
  }

  const onAuthenticate = async () => {
    try {
      await webAuthn.authenticate()
      await reauthenticate()
      toast.success(t(WELCOME_MESSAGE))
      redirect()
    } catch (e) {
      if (e.name === 'WebAuthnDeviceNotFoundError') {
        toast.error(
          'Device not found, log in with username/password to continue'
        )
        setShowWebAuthn(false)
      } else {
        toast.error(e.message)
      }
    }
  }

  const onRegister = async () => {
    try {
      await webAuthn.register()
      toast.success(t(WELCOME_MESSAGE))
      redirect()
    } catch (e) {
      toast.error(e.message)
    }
  }

  const onSkip = () => {
    toast.success(t(WELCOME_MESSAGE))
    setShouldShowWebAuthn(false)
  }

  const AuthWebAuthnPrompt = () => {
    return (
      <div className="rw-webauthn-wrapper">
        <h2>{t('loginPage.txt2')}</h2>
        <p>{t('loginPage.txt3')}</p>
        <div className="rw-button-group">
          <button className="rw-button rw-button-blue" onClick={onAuthenticate}>
            {t('loginPage.txt4')}
          </button>
        </div>
      </div>
    )
  }

  const RegisterWebAuthnPrompt = () => (
    <div className="rw-webauthn-wrapper">
      <h2>{t('loginPage.txt5')}</h2>
      <p>{t('loginPage.txt6')}</p>
      <div className="rw-button-group">
        <button className="rw-button rw-button-blue" onClick={onRegister}>
          {t('loginPage.txt7')}
        </button>
        <button className="rw-button" onClick={onSkip}>
          {t('loginPage.txt8')}
        </button>
      </div>
    </div>
  )

  const PasswordForm = () => (
    <Form onSubmit={onSubmit} className="rw-form-wrapper">
      <Label
        name="username"
        className="rw-label"
        errorClassName="rw-label rw-label-error"
      >
        {t('loginPage.txt9')}
      </Label>
      <TextField
        name="username"
        className="rw-input"
        errorClassName="rw-input rw-input-error"
        ref={usernameRef}
        validation={{
          required: {
            value: true,
            message: t('loginPage.txt10'),
          },
        }}
      />

      <FieldError name="username" className="rw-field-error" />

      <Label
        name="password"
        className="rw-label"
        errorClassName="rw-label rw-label-error"
      >
        {t('loginPage.txt11')}
      </Label>
      <PasswordField
        name="password"
        className="rw-input"
        errorClassName="rw-input rw-input-error"
        autoComplete="current-password"
        validation={{
          required: {
            value: true,
            message: t('loginPage.txt12'),
          },
        }}
      />

      <div className="rw-forgot-link">
        <Link to={routes.forgotPassword()} className="rw-forgot-link">
          {t('loginPage.txt13')}
        </Link>
      </div>

      <FieldError name="password" className="rw-field-error" />

      <div className="rw-button-group">
        <Submit className="rw-button rw-button-blue">
          {t('loginPage.txt14')}
        </Submit>
      </div>
    </Form>
  )

  const formToRender = () => {
    if (showWebAuthn) {
      if (webAuthn.isEnabled()) {
        return <AuthWebAuthnPrompt />
      } else {
        return <RegisterWebAuthnPrompt />
      }
    } else {
      return <PasswordForm />
    }
  }

  const linkToRender = () => {
    if (showWebAuthn) {
      if (webAuthn.isEnabled()) {
        return (
          <div className="rw-login-link basis-0">
            <span>{t('loginPage.txt15')} </span>{' '}
            <a href="?type=password" className="rw-link">
              {t('loginPage.txt16')}
            </a>
          </div>
        )
      }
    } else {
      return (
        <div className="rw-login-link basis-0">
          <span>{t('loginPage.txt17')}</span>{' '}
          <Link to={routes.signup()} className="rw-link">
            {t('loginPage.txt18')}
          </Link>
        </div>
      )
    }
  }

  if (loading) {
    return null
  }

  return (
    <>
      <MetaTags title="Login" />

      <div className="rw-segment w-2/6">
        <header className="rw-segment-header">
          <h2 className="rw-heading rw-heading-secondary">
            {t('loginPage.txt19')}
          </h2>
        </header>

        <div className="rw-segment-main">
          <div className="rw-form-wrapper">{formToRender()}</div>
        </div>
      </div>
      {linkToRender()}
    </>
  )
}

export default LoginPage
