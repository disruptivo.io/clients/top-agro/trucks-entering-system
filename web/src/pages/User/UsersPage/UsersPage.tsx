import { useParams } from '@redwoodjs/router'

import UsersCell from 'src/components/User/UsersCell'
import { config } from 'src/config/config'

const UsersPage = () => {
  const params = useParams()
  const page = parseInt(params.page) || config.pagination.page
  const size = parseInt(params.size) || config.pagination.size

  const getEnabledValue = () => {
    if (params.enabled) return params.enabled === 'true'

    return true
  }

  return (
    <UsersCell
      page={page}
      size={size}
      filter={params.filter}
      startDate={params.startDate}
      endDate={params.endDate}
      enabled={getEnabledValue()}
    />
  )
}

export default UsersPage
