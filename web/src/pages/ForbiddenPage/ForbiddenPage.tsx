import { useEffect } from 'react'

import { useTranslation } from 'react-i18next'

import { useAuth } from '@redwoodjs/auth'
import { navigate, routes } from '@redwoodjs/router'
import { MetaTags } from '@redwoodjs/web'

const ForbiddenPage = () => {
  const { isAuthenticated, loading } = useAuth()
  const { t } = useTranslation()

  useEffect(() => {
    if (!isAuthenticated && !loading) navigate(routes.login())
  }, [isAuthenticated])

  return (
    <div className="flex h-full w-full items-center justify-center">
      <MetaTags title="Forbidden" description="Forbidden page" />
      <h6 className="text-error">{t('forbiddenPage.txt1')}</h6>
    </div>
  )
}

export default ForbiddenPage
