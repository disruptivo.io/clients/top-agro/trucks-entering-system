// When you've added props to your component,
// pass Storybook's `args` through this story to control it from the addons panel:
//
// ```tsx
// import type { ComponentStory } from '@storybook/react'
//
// export const generated: ComponentStory<typeof ConfirmationModal> = (args) => {
//   return <ConfirmationModal {...args} />
// }
// ```
//
// See https://storybook.js.org/docs/react/writing-stories/args.

import type { ComponentMeta } from '@storybook/react'

import ConfirmationModal from './ConfirmationModal'

export const generated = () => {
  return <ConfirmationModal />
}

export default {
  title: 'Components/ConfirmationModal',
  component: ConfirmationModal,
} as ComponentMeta<typeof ConfirmationModal>
