import { MouseEventHandler } from 'react'

import { useTranslation } from 'react-i18next'

interface Props {
  title?: string
  message?: string
  onConfirm: MouseEventHandler<HTMLButtonElement>
  onCancel: MouseEventHandler<HTMLButtonElement>
  visible: boolean
}

const ConfirmationModal = ({
  title,
  message,
  onConfirm,
  onCancel,
  visible,
}: Props) => {
  const { t } = useTranslation()
  const defaultTitle = t('confirmationModal.txt1')
  const defaultMessage = t('confirmationModal.txt2')

  if (visible)
    return (
      <div className="absolute top-0 bottom-0 left-0 right-0 grid grid-cols-12 grid-rows-6 bg-light-100/50">
        <div className="col-start-6 col-end-10 row-start-3 row-end-4 grid grid-cols-3 grid-rows-5 rounded-md bg-primary-light">
          <h5 className="col-start-1 col-end-7 row-start-1 pl-2">
            {title || defaultTitle}
          </h5>
          <button
            className="col-start-6 row-start-1 flex w-10 items-center self-start"
            onClick={onCancel}
          >
            <div className="h-1 w-5 translate-x-2 translate-y-3 rotate-45 bg-accent-100"></div>
            <div className="h-1 w-5 -translate-x-3 translate-y-3 -rotate-45 bg-accent-100"></div>
          </button>
          <hr className=" col-start-1 col-end-7 row-start-2 my-8 h-px self-end border-0 bg-gray-200 dark:bg-gray-700" />
          <p className="col-start-1 col-end-7 row-start-3 self-center justify-self-center pl-2">
            {message || defaultMessage}
          </p>
          <button
            className="rw-button rw-button-small col-start-1 col-end-7 row-start-5 justify-self-center"
            onClick={onConfirm}
          >
            {t('confirmationModal.txt3')}
          </button>
        </div>
      </div>
    )

  return null
}

export default ConfirmationModal
