import { render } from '@redwoodjs/testing/web'

import ConfirmationModal from './ConfirmationModal'

//   Improve this test with help from the Redwood Testing Doc:
//    https://redwoodjs.com/docs/testing#testing-components

describe('ConfirmationModal', () => {
  it('renders successfully', () => {
    expect(() => {
      render(
        <ConfirmationModal onCancel={() => {}} onConfirm={() => {}} visible />
      )
    }).not.toThrow()
  })
})
