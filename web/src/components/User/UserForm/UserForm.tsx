import { useTranslation } from 'react-i18next'
import type { EditUserById, UpdateUserInput } from 'types/graphql'

import { useAuth } from '@redwoodjs/auth'
import {
  Form,
  FormError,
  FieldError,
  Label,
  TextField,
  CheckboxField,
  Submit,
  RWGqlError,
  PasswordField,
  NumberField,
  SelectField,
} from '@redwoodjs/forms'

type FormUser = NonNullable<EditUserById['user']>

interface UserFormProps {
  user?: EditUserById['user']
  onSave: (data: UpdateUserInput, id?: FormUser['id']) => void
  error: RWGqlError
  loading: boolean
}

const UserForm = (props: UserFormProps) => {
  const { currentUser } = useAuth()
  const { t } = useTranslation()

  const onSubmit = (data: FormUser) => {
    props.onSave(data, props?.user?.id)
  }

  return (
    <div className="rw-form-wrapper">
      <Form<FormUser> onSubmit={onSubmit} error={props.error}>
        <FormError
          error={props.error}
          wrapperClassName="rw-form-error-wrapper"
          titleClassName="rw-form-error-title"
          listClassName="rw-form-error-list"
        />

        <Label
          name="firstName"
          className="rw-label"
          errorClassName="rw-label rw-label-error"
        >
          {t('userForm.txt1')}
        </Label>

        <TextField
          name="firstName"
          defaultValue={props.user?.firstName}
          className="rw-input"
          errorClassName="rw-input rw-input-error"
          validation={{
            required: {
              value: true,
              message: t('userForm.txt2'),
            },
          }}
        />

        <FieldError name="firstName" className="rw-field-error" />

        <Label
          name="lastName"
          className="rw-label"
          errorClassName="rw-label rw-label-error"
        >
          {t('userForm.txt3')}
        </Label>

        <TextField
          name="lastName"
          defaultValue={props.user?.lastName}
          className="rw-input"
          errorClassName="rw-input rw-input-error"
          validation={{
            required: {
              value: true,
              message: t('userForm.txt4'),
            },
          }}
        />

        <FieldError name="lastName" className="rw-field-error" />

        <Label
          name="email"
          className="rw-label"
          errorClassName="rw-label rw-label-error"
        >
          {t('userForm.txt5')}
        </Label>

        <TextField
          name="username"
          defaultValue={props.user?.username}
          className="rw-input"
          errorClassName="rw-input rw-input-error"
          validation={{
            required: {
              value: true,
              message: t('userForm.txt6'),
            },
          }}
        />

        <FieldError name="username" className="rw-field-error" />

        {props.user && (
          <>
            <Label
              name="enabled"
              className="rw-label"
              errorClassName="rw-label rw-label-error"
            >
              {t('userForm.txt7')}
            </Label>

            <CheckboxField
              name="enabled"
              defaultChecked={props.user?.enabled}
              className="rw-input"
              errorClassName="rw-input rw-input-error"
            />

            <FieldError name="enabled" className="rw-field-error" />
          </>
        )}

        {!props.user && (
          <>
            <Label
              name="hashedPassword"
              className="rw-label"
              errorClassName="rw-label rw-label-error"
            >
              {t('userForm.txt8')}
            </Label>
            <PasswordField
              name="hashedPassword"
              className="rw-input"
              errorClassName="rw-input rw-input-error"
              autoComplete="current-hashedPassword"
              validation={{
                required: {
                  value: true,
                  message: t('userForm.txt9'),
                },
              }}
            />
            <FieldError name="hashedPassword" className="rw-field-error" />
            <NumberField hidden name="createdById" value={currentUser?.id} />
            <TextField hidden name="salt" value={'workaround'} />
          </>
        )}

        <Label
          name="roles"
          className="rw-label"
          errorClassName="rw-label rw-label-error"
        >
          {t('userForm.txt11')}
        </Label>
        <SelectField
          name="roles"
          className="rw-input"
          errorClassName="rw-input rw-input-error"
          autoComplete="current-role"
          defaultValue={props?.user?.roles}
          validation={{
            required: {
              value: true,
              message: t('userForm.txt12'),
            },
          }}
        >
          <option value="admin">Admin</option>
          <option value="doorman">Doorman</option>
          <option value="coordinator">Coordinator</option>
        </SelectField>
        <FieldError name="roles" className="rw-field-error" />

        <div className="rw-button-group">
          <Submit disabled={props.loading} className="rw-button rw-button-blue">
            {t('userForm.txt10')}
          </Submit>
        </div>
      </Form>
    </div>
  )
}

export default UserForm
