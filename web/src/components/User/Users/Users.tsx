import { useTranslation } from 'react-i18next'
import type { FindUsers } from 'types/graphql'

import { Link, routes, useParams } from '@redwoodjs/router'

import FilterForm from 'src/components/FilterForm/FilterForm'
import { formatDate } from 'src/components/helpers/date'
import Paginator from 'src/components/Paginator/Paginator'
import { config } from 'src/config/config'

const MAX_STRING_LENGTH = 150

const truncate = (value: string | number) => {
  const output = value?.toString()
  if (output?.length > MAX_STRING_LENGTH) {
    return output.substring(0, MAX_STRING_LENGTH) + '...'
  }
  return output ?? ''
}

const UsersList = ({ users, count }: FindUsers) => {
  const { t } = useTranslation()
  const { page = 1 } = useParams()

  return (
    <div className="rw-segment rw-table-wrapper-responsive h-full">
      <FilterForm className="col-start-3 col-end-13 row-start-1 mb-5 self-end" />
      <table className="rw-table h-3/4">
        <thead>
          <tr>
            <th>{t('users.txt1')}</th>
            <th>{t('users.txt2')}</th>
            <th>{t('users.txt3')}</th>
            <th>{t('users.txt4')}</th>
            <th className="hidden lg:table-cell">{t('users.txt5')}</th>
            <th className="hidden lg:table-cell">{t('users.txt6')}</th>
            <th className="hidden lg:table-cell">{t('users.txt7')}</th>
            <th className="hidden lg:table-cell">{t('users.txt8')}</th>
            <th>&nbsp;</th>
          </tr>
        </thead>
        <tbody>
          {users.map((user) => (
            <tr className="h-1" key={user.id}>
              <td>{truncate(user.id)}</td>
              <td>{truncate(user.firstName)}</td>
              <td>{truncate(user.lastName)}</td>
              <td>{truncate(user.username)}</td>
              <td className="hidden lg:table-cell">
                <time dateTime={user.createdAt} title={user.createdAt}>
                  {formatDate({ date: user.createdAt })}
                </time>
              </td>
              <td className="hidden lg:table-cell">
                <time dateTime={user.updatedAt} title={user.updatedAt}>
                  {formatDate({ date: user.updatedAt })}
                </time>
              </td>
              <td className="hidden lg:table-cell">
                {truncate(
                  `${user.createdBy?.firstName || ''} ${
                    user.createdBy?.lastName || ''
                  }`
                )}
              </td>
              <td className="hidden lg:table-cell">
                {truncate(
                  `${user.updatedBy?.firstName || ''} ${
                    user.updatedBy?.lastName || ''
                  }`
                )}
              </td>
              <td>
                <nav className="rw-table-actions">
                  <Link
                    to={routes.user({ id: user.id })}
                    title={t('users.txt9', { userId: user.id })}
                    className="rw-button rw-button-small"
                  >
                    {t('users.txt10')}
                  </Link>
                  <Link
                    to={routes.editUser({ id: user.id })}
                    title={t('users.txt11', { userId: user.id })}
                    className="rw-button rw-button-small"
                  >
                    {t('users.txt12')}
                  </Link>
                </nav>
              </td>
            </tr>
          ))}
          <tr />
        </tbody>
      </table>
      <Paginator
        disablePrevious={page < 2 || !page}
        disableNext={parseInt(page) * config.pagination.size >= count}
        className="mt-5"
        pageName="users"
      />
    </div>
  )
}

export default UsersList
