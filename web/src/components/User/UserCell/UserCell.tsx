import { useTranslation } from 'react-i18next'
import type { FindUserById } from 'types/graphql'

import type { CellSuccessProps, CellFailureProps } from '@redwoodjs/web'

import User from 'src/components/User/User'

export const QUERY = gql`
  query FindUserById($id: Int!) {
    user: user(id: $id) {
      id
      firstName
      lastName
      username
      enabled
      createdAt
      updatedAt
      createdBy {
        firstName
        lastName
      }
      updatedBy {
        firstName
        lastName
      }
      roles
    }
  }
`

export const Loading = () => {
  const { t } = useTranslation()

  return <div>{t('userCell.txt1')}</div>
}

export const Empty = () => {
  const { t } = useTranslation()

  return <div>{t('userCell.txt2')}</div>
}

export const Failure = ({ error }: CellFailureProps) => (
  <div className="rw-cell-error">{error?.message}</div>
)

export const Success = ({ user }: CellSuccessProps<FindUserById>) => {
  return <User user={user} />
}
