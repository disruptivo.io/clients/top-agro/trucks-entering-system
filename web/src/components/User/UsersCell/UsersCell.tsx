import { useTranslation } from 'react-i18next'
import type { FindUsers } from 'types/graphql'

import { Link, routes } from '@redwoodjs/router'
import type { CellSuccessProps, CellFailureProps } from '@redwoodjs/web'

import Users from 'src/components/User/Users'

export const QUERY = gql`
  query FindUsers(
    $page: Int!
    $size: Int!
    $filter: String
    $startDate: String
    $endDate: String
    $enabled: Boolean
  ) {
    users(
      page: $page
      size: $size
      filter: $filter
      startDate: $startDate
      endDate: $endDate
      enabled: $enabled
    ) {
      data {
        id
        firstName
        lastName
        username
        enabled
        createdAt
        updatedAt
        createdBy {
          firstName
          lastName
        }
        updatedBy {
          firstName
          lastName
        }
      }
      count
      isEmpty
    }
  }
`

export const beforeQuery = ({
  page,
  size,
  filter = '',
  startDate,
  endDate,
  enabled = true,
}) => {
  return { variables: { page, size, filter, startDate, endDate, enabled } }
}

export const Loading = () => {
  const { t } = useTranslation()

  return <div>{t('usersCell.txt1')}</div>
}

export const isEmpty = (data) => {
  return data.users.isEmpty
}

export const Empty = () => {
  const { t } = useTranslation()

  return (
    <div className="rw-text-center">
      {t('usersCell.txt2')}
      <Link to={routes.newUser()} className="rw-link">
        {t('usersCell.txt3')}
      </Link>
    </div>
  )
}

export const Failure = ({ error }: CellFailureProps) => (
  <div className="rw-cell-error">{error?.message}</div>
)

export const Success = ({ users }: CellSuccessProps<FindUsers>) => {
  return <Users users={users.data} count={users.count} />
}
