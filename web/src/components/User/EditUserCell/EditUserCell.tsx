import { useTranslation } from 'react-i18next'
import type { EditUserById, UpdateUserInput } from 'types/graphql'

import { useAuth } from '@redwoodjs/auth'
import { navigate, routes } from '@redwoodjs/router'
import type { CellSuccessProps, CellFailureProps } from '@redwoodjs/web'
import { useMutation } from '@redwoodjs/web'
import { toast } from '@redwoodjs/web/toast'

import UserForm from 'src/components/User/UserForm'

export const QUERY = gql`
  query EditUserById($id: Int!) {
    user: user(id: $id) {
      id
      firstName
      lastName
      username
      enabled
      createdAt
      updatedAt
      createdBy {
        firstName
        lastName
      }
      updatedBy {
        firstName
        lastName
      }
      roles
    }
  }
`
const UPDATE_USER_MUTATION = gql`
  mutation UpdateUserMutation($id: Int!, $input: UpdateUserInput!) {
    updateUser(id: $id, input: $input) {
      id
      firstName
      lastName
      username
      enabled
      createdAt
      updatedAt
      createdBy {
        firstName
        lastName
      }
      updatedBy {
        firstName
        lastName
      }
    }
  }
`

export const Loading = () => {
  const { t } = useTranslation()

  return <div>{t('editUserCell.txt1')}</div>
}

export const Failure = ({ error }: CellFailureProps) => (
  <div className="rw-cell-error">{error?.message}</div>
)

export const Success = ({ user }: CellSuccessProps<EditUserById>) => {
  const { t } = useTranslation()
  const { currentUser } = useAuth()
  const [updateUser, { loading, error }] = useMutation(UPDATE_USER_MUTATION, {
    onCompleted: () => {
      toast.success(t('editUserCell.txt2'))
      navigate(routes.users())
    },
    onError: (error) => {
      toast.error(error.message)
    },
  })

  const onSave = (input: UpdateUserInput, id: EditUserById['user']['id']) => {
    updateUser({
      variables: { id, input: { ...input, updatedById: currentUser.id } },
    })
  }

  return (
    <div className="rw-segment">
      <header className="rw-segment-header">
        <h2 className="rw-heading rw-heading-secondary">
          {t('editUserCell.txt3', { userId: user.id })}
        </h2>
      </header>
      <div className="rw-segment-main">
        <UserForm user={user} onSave={onSave} error={error} loading={loading} />
      </div>
    </div>
  )
}
