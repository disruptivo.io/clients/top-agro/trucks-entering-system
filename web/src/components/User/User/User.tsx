import { useTranslation } from 'react-i18next'
import type { FindUserById } from 'types/graphql'

import { Link, routes } from '@redwoodjs/router'

import { formatDate } from 'src/components/helpers/date'

const checkboxInputTag = (checked: boolean) => {
  return <input type="checkbox" checked={checked} disabled />
}

interface Props {
  user: NonNullable<FindUserById['user']>
}

const User = ({ user }: Props) => {
  const { t } = useTranslation()

  return (
    <>
      <div className="rw-segment">
        <header className="rw-segment-header">
          <h2 className="rw-heading rw-heading-secondary">
            {t('user.txt1', { userId: user.id })}
          </h2>
        </header>
        <table className="rw-table">
          <tbody>
            <tr>
              <th>{t('user.txt2')}</th>
              <td>{user.id}</td>
            </tr>
            <tr>
              <th>{t('user.txt3')}</th>
              <td>{user.firstName}</td>
            </tr>
            <tr>
              <th>{t('user.txt4')}</th>
              <td>{user.lastName}</td>
            </tr>
            <tr>
              <th>{t('user.txt5')}</th>
              <td>{user.username}</td>
            </tr>
            <tr>
              <th>{t('user.txt12')}</th>
              <td>{user.roles}</td>
            </tr>
            <tr>
              <th>{t('user.txt6')}</th>
              <td>{checkboxInputTag(user.enabled)}</td>
            </tr>
            <tr>
              <th>{t('user.txt7')}</th>
              <td>
                <time dateTime={user.createdAt} title={user.createdAt}>
                  {formatDate({ date: user.createdAt })}
                </time>
              </td>
            </tr>
            <tr>
              <th>{t('user.txt8')}</th>
              <td>
                <time dateTime={user.updatedAt} title={user.updatedAt}>
                  {formatDate({ date: user.updatedAt })}
                </time>
              </td>
            </tr>
            <tr>
              <th>{t('user.txt9')}</th>
              <td>{`${user.createdBy?.firstName || ''} ${
                user.createdBy?.lastName || ''
              }`}</td>
            </tr>
            <tr>
              <th>{t('user.txt10')}</th>
              <td>{`${user.updatedBy?.firstName || ''} ${
                user.updatedBy?.lastName || ''
              }`}</td>
            </tr>
          </tbody>
        </table>
      </div>
      <nav className="rw-button-group">
        <Link
          to={routes.editUser({ id: user.id })}
          className="rw-button rw-button-blue"
        >
          {t('user.txt11')}
        </Link>
      </nav>
    </>
  )
}

export default User
