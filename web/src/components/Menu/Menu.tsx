import { useState } from 'react'

import { useTranslation } from 'react-i18next'

import { useAuth } from '@redwoodjs/auth'
import { Form, Submit } from '@redwoodjs/forms'
import { Link, routes, useLocation } from '@redwoodjs/router'

const pathnameToHandlers = {
  default: {
    label: 'menu.txt6',
    roles: ['admin', 'doorman'],
    to: () => routes.enterings(),
  },
  drivers: {
    label: 'menu.txt2',
    roles: ['admin', 'coordinator'],
    to: () => routes.drivers(),
  },
  trucks: {
    label: 'menu.txt3',
    roles: ['admin', 'coordinator'],
    to: () => routes.trucks(),
  },
  organizations: {
    label: 'menu.txt4',
    roles: ['admin', 'coordinator'],
    to: () => routes.organizations(),
  },
  users: {
    label: 'menu.txt5',
    roles: ['admin', 'coordinator'],
    to: () => routes.users(),
  },
}

type SideBarProps = {
  className: string
}

const Menu = ({ className }: SideBarProps) => {
  const { t } = useTranslation()
  const { currentUser, logOut, hasRole } = useAuth()
  const { pathname } = useLocation()
  const formattedPathname = pathname.split('/')[1]
  const [hidden, setHidden] = useState(true)

  const onSubmit = () => {
    logOut()
  }

  const onClick = () => {
    setHidden((hidden) => !hidden)
  }

  return (
    <>
      <aside
        className={`absolute top-0 z-10 bg-primary transition duration-150 ease-out ${
          hidden ? 'bottom-full' : 'bottom-0'
        } left-0 right-0 grid grid-cols-1 grid-rows-3 overflow-hidden lg:relative lg:top-0 ${className}`}
      >
        <img
          className="row-start-1 hidden w-1/3 justify-self-center lg:block lg:pt-5"
          src={require('./logo.jpeg')}
          alt="logo"
        />
        <div className="row-start-2 flex flex-col items-center justify-center pl-0 lg:items-start lg:pl-5">
          {Object.keys(pathnameToHandlers).map((key) => {
            if (!hasRole(pathnameToHandlers[key].roles)) return null

            return (
              <Link
                key={key}
                className={`text-accent-200 hover:text-light-100 ${
                  key === formattedPathname ||
                  (key === 'default' && !formattedPathname)
                    ? 'text-light-100 underline decoration-light-100 decoration-1'
                    : ''
                }`}
                to={pathnameToHandlers[key].to()}
              >
                {t(pathnameToHandlers[key].label)}
              </Link>
            )
          })}
        </div>
        <Form
          className="row-start-3 flex flex-col items-center justify-end py-5"
          onSubmit={onSubmit}
        >
          <span>{`${currentUser?.firstName} ${currentUser?.lastName}`}</span>
          <Submit className="rw-button rw-button-green">
            {t('menu.txt1')}
          </Submit>
        </Form>
      </aside>
      <div className="absolute -top-3 z-20 flex h-10 w-full justify-center lg:hidden">
        <button
          className={`flex w-10 items-center ${
            hidden
              ? 'animate-bounce rounded-full bg-accent-200 hover:animate-none'
              : ''
          }`}
          onClick={onClick}
        >
          <div
            className={`h-1 w-5 rotate-45 bg-primary ${
              hidden
                ? 'translate-x-1 translate-y-1'
                : 'translate-x-2 translate-y-3'
            } `}
          ></div>
          <div
            className={`h-1 w-5 -rotate-45 bg-primary ${
              hidden
                ? '-translate-x-1 translate-y-1'
                : '-translate-x-3 translate-y-3'
            }`}
          ></div>
        </button>
      </div>
    </>
  )
}

export default Menu
