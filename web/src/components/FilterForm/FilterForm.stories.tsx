// When you've added props to your component,
// pass Storybook's `args` through this story to control it from the addons panel:
//
// ```tsx
// import type { ComponentStory } from '@storybook/react'
//
// export const generated: ComponentStory<typeof FilterForm> = (args) => {
//   return <FilterForm {...args} />
// }
// ```
//
// See https://storybook.js.org/docs/react/writing-stories/args.

import type { ComponentMeta } from '@storybook/react'

import FilterForm from './FilterForm'

export const generated = () => {
  return <FilterForm className="" />
}

export default {
  title: 'Components/FilterForm',
  component: FilterForm,
} as ComponentMeta<typeof FilterForm>
