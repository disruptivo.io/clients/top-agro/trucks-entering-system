import { render } from '@redwoodjs/testing/web'

import FilterForm from './FilterForm'

//   Improve this test with help from the Redwood Testing Doc:
//    https://redwoodjs.com/docs/testing#testing-components

describe('FilterForm', () => {
  it('renders successfully', () => {
    expect(() => {
      render(<FilterForm />)
    }).not.toThrow()
  })
})
