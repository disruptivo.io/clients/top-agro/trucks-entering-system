import { useEffect } from 'react'

import moment from 'moment'
import { useTranslation } from 'react-i18next'

import {
  TextField,
  Form,
  Label,
  Submit,
  CheckboxField,
  Controller,
  useForm,
} from '@redwoodjs/forms'
import { navigate, useLocation, useParams } from '@redwoodjs/router'
import { toast } from '@redwoodjs/web/dist/toast'

interface FilterFormProps {
  className: string
}

const FilterForm = ({ className, hideEnabled }: FilterFormProps) => {
  const { t } = useTranslation()
  const { pathname } = useLocation()
  const params = useParams()
  const formMethods = useForm()

  const getDefaultChecked = () => {
    if (params.enabled) return params.enabled === 'true'

    return true
  }

  const onSubmit = ({ filter, startDate, endDate, enabled }) => {
    if (
      [startDate, endDate].filter(Boolean).length &&
      [startDate, endDate].filter(Boolean).length !== 2
    )
      return toast.error(t('filterForm.txt7'))

    if (startDate > endDate) return toast.error(t('filterForm.txt8'))

    if (moment(endDate).diff(moment(startDate), 'days') > 59)
      return toast.error(t('filterForm.txt9', { days: 59 + 1 }))

    navigate(
      `${pathname}?${new URLSearchParams({
        ...params,
        page: '1',
        filter: filter || '',
        startDate: startDate ? moment(startDate).startOf('D').toString() : '',
        endDate: endDate ? moment(endDate).endOf('D').toString() : '',
        enabled,
      })}`
    )
  }

  const onReset = () => {
    formMethods.setValue('filter', '')
    formMethods.setValue('startDate', '')
    formMethods.setValue('endDate', '')
    formMethods.setValue('enabled', true)
    formMethods.handleSubmit(onSubmit)()
  }

  useEffect(() => {
    formMethods.setValue('filter', params.filter || '')
  }, [params.filter])

  useEffect(() => {
    formMethods.setValue(
      'startDate',
      params.startDate ? moment(params.startDate).format('YYYY-MM-DD') : ''
    )
  }, [params.startDate])

  useEffect(() => {
    formMethods.setValue(
      'endDate',
      params.endDate ? moment(params.endDate).format('YYYY-MM-DD') : ''
    )
  }, [params.endDate])

  useEffect(() => {
    formMethods.setValue('enabled', getDefaultChecked())
  }, [params.enabled])

  return (
    <Form
      formMethods={formMethods}
      className={`grid grid-cols-12 grid-rows-1 gap-x-5 ${className}`}
      onSubmit={formMethods.handleSubmit(onSubmit)}
    >
      <div className="col-start-1 col-end-3">
        <Label name="filter" className="rw-label m-0">
          {t('filterForm.txt1')}
        </Label>

        <Controller
          control={formMethods.control}
          name="filter"
          defaultValue={params.filter || ''}
          render={({ field }) => {
            const { name, value, onChange } = field
            return (
              <TextField
                name={name}
                value={value}
                onChange={onChange}
                className="rw-input"
              />
            )
          }}
        />
      </div>

      <div className="col-start-3 col-end-5">
        <Label name="startDate" className="rw-label m-0">
          {t('filterForm.txt2')}
        </Label>

        <Controller
          control={formMethods.control}
          name="startDate"
          defaultValue={
            params.startDate
              ? moment(params.startDate).format('YYYY-MM-DD')
              : ''
          }
          render={({ field }) => {
            const { name, value, onChange } = field
            return (
              <input
                type="date"
                name={name}
                value={value}
                onChange={onChange}
                className="rw-input"
                max={moment().format('YYYY-MM-DD')}
              />
            )
          }}
        />
      </div>

      <div className="col-start-5 col-end-7">
        <Label name="endDate" className="rw-label m-0">
          {t('filterForm.txt3')}
        </Label>

        <Controller
          control={formMethods.control}
          name="endDate"
          defaultValue={
            params.endDate ? moment(params.endDate).format('YYYY-MM-DD') : ''
          }
          render={({ field }) => {
            const { name, value, onChange } = field
            return (
              <input
                type="date"
                name={name}
                value={value}
                onChange={onChange}
                className="rw-input"
                max={moment().format('YYYY-MM-DD')}
              />
            )
          }}
        />
      </div>

      <div
        className={`col-start-7 col-end-8 flex flex-col items-center ${
          hideEnabled ? 'hidden' : ''
        }`}
      >
        <Label name="enabled" className="rw-label m-0">
          {t('filterForm.txt4')}
        </Label>

        <Controller
          control={formMethods.control}
          name="enabled"
          defaultValue={getDefaultChecked()}
          render={({ field }) => {
            const { name, value, onChange } = field
            return (
              <CheckboxField
                name={name}
                value={value}
                onChange={onChange}
                className="rw-input"
              />
            )
          }}
        />
      </div>

      <Submit className="rw-button rw-button-blue col-start-8 col-end-10 self-end lg:col-start-8 lg:col-end-9">
        {t('filterForm.txt5')}
      </Submit>

      <button
        type="reset"
        className="rw-button rw-button-green col-start-10 col-end-12 self-end lg:col-start-9 lg:col-end-10"
        onClick={onReset}
      >
        {t('filterForm.txt6')}
      </button>
    </Form>
  )
}

export default FilterForm
