import moment from 'moment'

const formatDate = ({
  date = '',
  withTime = true,
  dateInputFormat = false,
} = {}) => {
  if (!date) return '-'

  if (!withTime && dateInputFormat) return moment.utc(date).format('YYYY-MM-DD')

  if (!withTime) return moment.utc(date).format('DD/MM/YYYY')

  return moment(date).format('DD/MM/YYYY HH:mm')
}

export { formatDate }
