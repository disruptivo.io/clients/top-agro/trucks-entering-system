import { useTranslation } from 'react-i18next'
import type { FindDrivers } from 'types/graphql'

import { Link, routes, useParams } from '@redwoodjs/router'

import FilterForm from 'src/components/FilterForm/FilterForm'
import { formatDate } from 'src/components/helpers/date'
import Paginator from 'src/components/Paginator/Paginator'
import { config } from 'src/config/config'

const MAX_STRING_LENGTH = 150

const truncate = (value: string | number) => {
  const output = value?.toString()
  if (output?.length > MAX_STRING_LENGTH) {
    return output.substring(0, MAX_STRING_LENGTH) + '...'
  }
  return output ?? ''
}

const DriversList = ({ drivers, count }: FindDrivers) => {
  const { t } = useTranslation()
  const { page = 1 } = useParams()

  return (
    <div className="rw-segment rw-table-wrapper-responsive h-full">
      <FilterForm className="col-start-3 col-end-13 row-start-1 mb-5 self-end" />
      <table className="rw-table h-3/4">
        <thead>
          <tr>
            <th>{t('drivers.txt1')}</th>
            <th>{t('drivers.txt2')}</th>
            <th>{t('drivers.txt3')}</th>
            <th>{t('drivers.txt4')}</th>
            <th className="hidden lg:table-cell">{t('drivers.txt5')}</th>
            <th className="hidden lg:table-cell">{t('drivers.txt6')}</th>
            <th className="hidden lg:table-cell">{t('drivers.txt7')}</th>
            <th className="hidden lg:table-cell">{t('drivers.txt8')}</th>
            <th>&nbsp;</th>
          </tr>
        </thead>
        <tbody>
          {drivers.map((driver) => (
            <tr className="h-1" key={driver.id}>
              <td>{truncate(driver.id)}</td>
              <td>{truncate(driver.firstName)}</td>
              <td>{truncate(driver.lastName)}</td>
              <td>{truncate(driver.organization?.name)}</td>
              <td className="hidden lg:table-cell">
                <time dateTime={driver.createdAt} title={driver.createdAt}>
                  {formatDate({ date: driver.createdAt })}
                </time>
              </td>
              <td className="hidden lg:table-cell">
                <time dateTime={driver.updatedAt} title={driver.updatedAt}>
                  {formatDate({ date: driver.updatedAt })}
                </time>
              </td>
              <td className="hidden lg:table-cell">
                {truncate(
                  `${driver.createdBy?.firstName || ''} ${
                    driver.createdBy?.lastName || ''
                  }`
                )}
              </td>
              <td className="hidden lg:table-cell">
                {truncate(
                  `${driver.updatedBy?.firstName || ''} ${
                    driver.updatedBy?.lastName || ''
                  }`
                )}
              </td>
              <td>
                <nav className="rw-table-actions">
                  <Link
                    to={routes.driver({ id: driver.id })}
                    title={t('drivers.txt11', { driverId: driver.id })}
                    className="rw-button rw-button-small"
                  >
                    {t('drivers.txt9')}
                  </Link>
                  <Link
                    to={routes.editDriver({ id: driver.id })}
                    title={t('drivers.txt12', { driverId: driver.id })}
                    className="rw-button rw-button-small"
                  >
                    {t('drivers.txt10')}
                  </Link>
                </nav>
              </td>
            </tr>
          ))}
          <tr />
        </tbody>
      </table>
      <Paginator
        disablePrevious={page < 2 || !page}
        disableNext={parseInt(page) * config.pagination.size >= count}
        className="mt-5"
        pageName="drivers"
      />
    </div>
  )
}

export default DriversList
