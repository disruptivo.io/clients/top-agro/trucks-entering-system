import { useTranslation } from 'react-i18next'
import type { FindDrivers } from 'types/graphql'

import { Link, routes } from '@redwoodjs/router'
import type { CellSuccessProps, CellFailureProps } from '@redwoodjs/web'

import Drivers from 'src/components/Driver/Drivers'

export const QUERY = gql`
  query FindDrivers(
    $page: Int!
    $size: Int!
    $filter: String
    $startDate: String
    $endDate: String
    $enabled: Boolean
  ) {
    drivers(
      page: $page
      size: $size
      filter: $filter
      startDate: $startDate
      endDate: $endDate
      enabled: $enabled
    ) {
      data {
        id
        firstName
        lastName
        organization {
          id
          name
        }
        createdAt
        updatedAt
        createdBy {
          firstName
          lastName
        }
        updatedBy {
          firstName
          lastName
        }
      }
      count
      isEmpty
    }
  }
`

export const beforeQuery = ({
  page,
  size,
  filter = '',
  startDate,
  endDate,
  enabled = true,
}) => {
  return { variables: { page, size, filter, startDate, endDate, enabled } }
}

export const Loading = () => {
  const { t } = useTranslation()

  return <div>{t('driversCell.txt1')}</div>
}

export const isEmpty = (data) => {
  return data.drivers.isEmpty
}

export const Empty = () => {
  const { t } = useTranslation()

  return (
    <div className="rw-text-center">
      {t('driversCell.txt2')}
      <Link to={routes.newDriver()} className="rw-link">
        {t('driversCell.txt3')}
      </Link>
    </div>
  )
}

export const Failure = ({ error }: CellFailureProps) => (
  <div className="rw-cell-error">{error?.message}</div>
)

export const Success = ({ drivers }: CellSuccessProps<FindDrivers>) => {
  return <Drivers drivers={drivers.data} count={drivers.count} />
}
