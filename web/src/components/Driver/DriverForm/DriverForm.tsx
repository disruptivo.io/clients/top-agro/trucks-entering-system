import { useCallback, useEffect, useState } from 'react'

import * as filestack from 'filestack-js'
import { PickerOverlay } from 'filestack-react'
import { useTranslation } from 'react-i18next'
import Lightbox from 'react-image-lightbox'
import type { EditDriverById, UpdateDriverInput } from 'types/graphql'

import {
  Form,
  FormError,
  FieldError,
  Label,
  TextField,
  DateField,
  CheckboxField,
  Submit,
  SearchField,
  useForm,
  Controller,
} from '@redwoodjs/forms'
import type { RWGqlError } from '@redwoodjs/forms'
import { useQuery } from '@redwoodjs/web'
import { toast } from '@redwoodjs/web/dist/toast'

import { formatDate } from 'src/components/helpers/date'

const filestackClient = filestack.init(process.env.FILESTACK_API_KEY)
filestackClient.setSecurity({
  policy: process.env.FILESTACK_POLICY,
  signature: process.env.FILESTACK_SIGNATURE,
})

let timeout
export const ORGANIZATIONS_BY_NAME = gql`
  query organizationsByName($name: String!) {
    organizationsByName(name: $name) {
      id
      name
    }
  }
`
export const TRUCKS_BY_PLATE = gql`
  query trucksByPlate($plate: String!) {
    trucksByPlate(plate: $plate) {
      id
      plate
    }
  }
`

type FormDriver = NonNullable<EditDriverById['driver']>

interface DriverFormProps {
  driver?: EditDriverById['driver']
  onSave: (data: UpdateDriverInput, id?: FormDriver['id']) => void
  error: RWGqlError
  loading: boolean
}

const DriverForm = (props: DriverFormProps) => {
  const formMethods = useForm()
  const fallbackPictures = Array.from({ length: 2 })
    .fill(0)
    .map(() => ({ url: '' }))
  const mappedPictures = props?.driver?.pictures.map(({ id, url }) => ({
    id,
    url,
  }))
  const [pictures, setPictures] = useState(
    props?.driver?.pictures.length ? mappedPictures : fallbackPictures
  )
  const [showImagePicker, setShowImagePicker] = useState(false)
  const [removingPicture, setRemovingPicture] = useState(false)
  const [pictureToUpdate, setPictureToUpdate] = useState(null)
  const [imageUrl, setImageUrl] = useState('')
  const { t } = useTranslation()
  const { data: organizationsResponse, refetch: refetchOrganizations } =
    useQuery(ORGANIZATIONS_BY_NAME, {
      variables: {
        name: '',
      },
    })
  const { data: trucksResponse, refetch: refetchTrucks } = useQuery(
    TRUCKS_BY_PLATE,
    {
      variables: {
        plate: '',
      },
    }
  )
  const organizationsByName = organizationsResponse?.organizationsByName || []
  const trucksByPlate = trucksResponse?.trucksByPlate || []

  const onSubmit = (data: FormDriver) => {
    const { organization: organizationName, truck: truckPlate, ...rest } = data
    props.onSave(
      {
        ...rest,
        organizationId: organizationsByName
          .filter((organization) => organization.name === organizationName)
          .shift()?.id,
        truckId: trucksByPlate
          .filter((truck) => truck.plate === truckPlate)
          .shift()?.id,
      },
      props?.driver?.id
    )
  }

  const onChange = useCallback(({ target }) => {
    if (timeout) clearTimeout(timeout)
    timeout = setTimeout(() => {
      if (target.name === 'organization')
        return refetchOrganizations({ name: target.value })

      refetchTrucks({ plate: target.value })
    }, 1000)
  }, [])

  const removePicture =
    ({ url, index }) =>
    async () => {
      try {
        const FILE_ID = new URL(url).pathname.replace('/', '')
        setRemovingPicture(true)
        await filestackClient.remove(FILE_ID)

        setPictures((currentPictures) => {
          const pictures = [...currentPictures]
          pictures[index] = {
            ...pictures[index],
            url: '',
          }
          return pictures
        })

        setRemovingPicture(false)
      } catch (error) {
        setRemovingPicture(false)
      }
    }

  const toggleShowImagePicker = () => {
    setShowImagePicker((current) => !current)
  }

  const showImagePickerAndMarkImageForRemoval =
    ({ index }) =>
    () => {
      toggleShowImagePicker()
      setPictureToUpdate(index)
    }

  const onFileUploaded = async ({ filesFailed, filesUploaded }) => {
    try {
      if (filesFailed.length) return toast.error(t('driverForm.txt17'))

      await removePicture({
        url: pictures[pictureToUpdate].url,
        index: pictureToUpdate,
      })()

      setShowImagePicker(false)
      setPictures((currentPictures) => {
        const pictures = [...currentPictures]
        pictures[pictureToUpdate] = {
          ...pictures[pictureToUpdate],
          url: `${filesUploaded[0].url}?policy=${process.env.FILESTACK_POLICY}&signature=${process.env.FILESTACK_SIGNATURE}`,
        }
        return pictures
      })
    } catch (error) {
      console.log(error)
    }
  }

  const onClickPicture =
    ({ url }) =>
    () => {
      setImageUrl(url)
    }

  const onClosePicture = () => {
    setImageUrl('')
  }

  useEffect(() => {
    if (props?.driver?.pictures.length)
      setPictures(props?.driver?.pictures.map(({ id, url }) => ({ id, url })))
  }, [props.driver?.pictures])

  useEffect(() => {
    if (pictures.every((picture) => picture.url))
      formMethods.setValue('pictures', pictures)
    if (pictures.some((picture) => !picture.url))
      formMethods.setValue('pictures', null)
  }, [pictures])

  const renderImageButtons = () => {
    return (
      <div className="grid h-52 w-1/2 grid-cols-2 grid-rows-1 gap-2">
        {pictures.map(({ url }, index) => {
          return (
            <div
              key={index}
              className={`row-start-1 row-end-1 flex flex-col col-start-${
                index + 1
              }} col-end-${index + 2}`}
            >
              <div
                key={url}
                onClick={onClickPicture({ url })}
                role="button"
                aria-hidden="true"
              >
                <img
                  alt={url}
                  className="max-h-40 w-full object-contain p-1"
                  src={
                    url ||
                    'https://api.dicebear.com/6.x/bottts/svg?seed=Snowball&baseColor=9f0450'
                  }
                />
              </div>
              {url ? (
                <div className="flex flex-row self-center">
                  <button
                    disabled={removingPicture}
                    type="button"
                    className="rw-button rw-button-green disabled:cursor-progress disabled:border-dark-100 disabled:text-dark-100"
                    onClick={showImagePickerAndMarkImageForRemoval({ index })}
                  >
                    {t('driverForm.txt15')}
                  </button>
                </div>
              ) : (
                <button
                  disabled={removingPicture}
                  type="button"
                  className="rw-button rw-button-green disabled:cursor-progress disabled:border-dark-100 disabled:text-dark-100"
                  onClick={showImagePickerAndMarkImageForRemoval({ index })}
                >
                  {t('driverForm.txt16')}
                </button>
              )}
            </div>
          )
        })}
      </div>
    )
  }

  return (
    <div className="rw-form-wrapper">
      <Form
        onSubmit={formMethods.handleSubmit(onSubmit)}
        error={props.error}
        formMethods={formMethods}
        onChange={onChange}
      >
        <FormError
          error={props.error}
          wrapperClassName="rw-form-error-wrapper"
          titleClassName="rw-form-error-title"
          listClassName="rw-form-error-list"
        />

        <Label
          name="pictures"
          className="rw-label"
          errorClassName="rw-label rw-label-error"
        >
          {t('driverForm.txt7')}
        </Label>

        <Controller
          control={formMethods.control}
          name="pictures"
          rules={{
            required: {
              value: true,
              message: t('driverForm.txt14'),
            },
          }}
          render={({ field }) => {
            const { name } = field

            if (!showImagePicker) return renderImageButtons()

            return (
              <PickerOverlay
                apikey={process.env.FILESTACK_API_KEY}
                pickerOptions={{
                  lang: 'es',
                  imageDim: [800, 800],
                  fromSources: ['local_file_system', 'webcam'],
                  accept: 'image/*',
                  minFiles: 1,
                  maxFiles: 1,
                  onClose: toggleShowImagePicker,
                }}
                clientOptions={{
                  security: {
                    policy: process.env.FILESTACK_POLICY,
                    signature: process.env.FILESTACK_SIGNATURE,
                  },
                }}
                name={name}
                onUploadDone={onFileUploaded}
              />
            )
          }}
        />

        <FieldError name="pictures" className="rw-field-error" />

        <Label
          name="firstName"
          className="rw-label"
          errorClassName="rw-label rw-label-error"
        >
          {t('driverForm.txt1')}
        </Label>

        <TextField
          name="firstName"
          defaultValue={props.driver?.firstName}
          className="rw-input"
          errorClassName="rw-input rw-input-error"
          validation={{
            required: {
              value: true,
              message: t('driverForm.txt2'),
            },
          }}
        />

        <FieldError name="firstName" className="rw-field-error" />

        <Label
          name="lastName"
          className="rw-label"
          errorClassName="rw-label rw-label-error"
        >
          {t('driverForm.txt3')}
        </Label>

        <TextField
          name="lastName"
          defaultValue={props.driver?.lastName}
          className="rw-input"
          errorClassName="rw-input rw-input-error"
          validation={{
            required: {
              value: true,
              message: t('driverForm.txt4'),
            },
          }}
        />

        <FieldError name="lastName" className="rw-field-error" />

        <Label
          name="identification"
          className="rw-label"
          errorClassName="rw-label rw-label-error"
        >
          {t('driverForm.txt5')}
        </Label>

        <TextField
          name="identification"
          defaultValue={props.driver?.identification}
          className="rw-input"
          errorClassName="rw-input rw-input-error"
          validation={{
            required: {
              value: true,
              message: t('driverForm.txt6'),
            },
          }}
        />

        <FieldError name="identification" className="rw-field-error" />

        <Label
          name="dateOfBirth"
          className="rw-label"
          errorClassName="rw-label rw-label-error"
        >
          {t('driverForm.txt8')}
        </Label>

        <DateField
          name="dateOfBirth"
          defaultValue={formatDate({
            date: props.driver?.dateOfBirth,
            withTime: false,
            dateInputFormat: true,
          })}
          className="rw-input"
          errorClassName="rw-input rw-input-error"
        />

        <FieldError name="dateOfBirth" className="rw-field-error" />

        {props.driver && (
          <>
            <Label
              name="enabled"
              className="rw-label"
              errorClassName="rw-label rw-label-error"
            >
              {t('driverForm.txt9')}
            </Label>

            <CheckboxField
              name="enabled"
              defaultChecked={props.driver?.enabled}
              className="rw-input"
              errorClassName="rw-input rw-input-error"
            />

            <FieldError name="enabled" className="rw-field-error" />
          </>
        )}

        <Label
          name="organization"
          className="rw-label"
          errorClassName="rw-label rw-label-error"
        >
          {t('driverForm.txt10')}
        </Label>

        <SearchField
          name="organization"
          list="organizations"
          className="rw-input"
          defaultValue={props.driver?.organization?.name}
          errorClassName="rw-input rw-input-error"
          validation={{
            required: {
              value: true,
              message: t('driverForm.txt13'),
            },
          }}
        />

        <FieldError name="organization" className="rw-field-error" />

        <datalist id="organizations">
          {organizationsByName?.map((organization) => (
            <option key={organization.id} value={organization.name}>
              {organization.id}
            </option>
          ))}
        </datalist>

        <Label
          name="truck"
          className="rw-label"
          errorClassName="rw-label rw-label-error"
        >
          {t('driverForm.txt11')}
        </Label>

        <SearchField
          name="truck"
          list="trucks"
          className="rw-input"
          defaultValue={props.driver?.truck?.plate}
          errorClassName="rw-input rw-input-error"
        />

        <datalist id="trucks">
          {trucksByPlate?.map((truck) => (
            <option key={truck.id} value={truck.plate}>
              {truck.id}
            </option>
          ))}
        </datalist>

        <FieldError name="truck" className="rw-field-error" />

        <div className="rw-button-group">
          <Submit
            disabled={props.loading || removingPicture}
            className="rw-button rw-button-blue"
          >
            {t('driverForm.txt12')}
          </Submit>
        </div>
        {imageUrl && (
          <Lightbox
            enableZoom={true}
            mainSrc={imageUrl}
            onCloseRequest={onClosePicture}
            zoomInLabel="Zoom in"
            zoomOutLabel="Zoom out"
            closeLabel="Close"
          />
        )}
      </Form>
    </div>
  )
}

export default DriverForm
