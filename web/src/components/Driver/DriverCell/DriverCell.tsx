import { useTranslation } from 'react-i18next'
import type { FindDriverById } from 'types/graphql'

import type { CellSuccessProps, CellFailureProps } from '@redwoodjs/web'

import Driver from 'src/components/Driver/Driver'

export const QUERY = gql`
  query FindDriverById($id: Int!) {
    driver: driver(id: $id) {
      id
      firstName
      lastName
      identification
      pictures {
        url
      }
      dateOfBirth
      enabled
      createdAt
      updatedAt
      createdBy {
        firstName
        lastName
      }
      updatedBy {
        firstName
        lastName
      }
      organization {
        id
        name
      }
      truck {
        id
        plate
      }
    }
  }
`

export const Loading = () => {
  const { t } = useTranslation()

  return <div>{t('driverCell.txt1')}</div>
}

export const Empty = () => {
  const { t } = useTranslation()

  return <div>{t('driverCell.txt2')}</div>
}

export const Failure = ({ error }: CellFailureProps) => (
  <div className="rw-cell-error">{error?.message}</div>
)

export const Success = ({ driver }: CellSuccessProps<FindDriverById>) => {
  return <Driver driver={driver} />
}
