import { useState } from 'react'

import { useTranslation } from 'react-i18next'
import Lightbox from 'react-image-lightbox'
import type { FindDriverById } from 'types/graphql'

import { Link, routes } from '@redwoodjs/router'

import { formatDate } from 'src/components/helpers/date'

const checkboxInputTag = (checked: boolean) => {
  return <input type="checkbox" checked={checked} disabled />
}

interface Props {
  driver: NonNullable<FindDriverById['driver']>
}

const Driver = ({ driver }: Props) => {
  const { t } = useTranslation()
  const [imageUrl, setImageUrl] = useState('')

  const onClickPicture =
    ({ url }) =>
    () => {
      setImageUrl(url)
    }

  const onClosePicture = () => {
    setImageUrl('')
  }

  return (
    <>
      <div className="rw-segment">
        <header className="rw-segment-header">
          <h2 className="rw-heading rw-heading-secondary">
            {t('driver.txt1', { driverId: driver.id })}
          </h2>
        </header>
        <table className="rw-table">
          <tbody>
            <tr>
              <th>{t('driver.txt2')}</th>
              <td>{driver.id}</td>
            </tr>
            <tr>
              <th>{t('driver.txt3')}</th>
              <td>{driver.firstName}</td>
            </tr>
            <tr>
              <th>{t('driver.txt4')}</th>
              <td>{driver.lastName}</td>
            </tr>
            <tr>
              <th>{t('driver.txt5')}</th>
              <td>{driver.identification}</td>
            </tr>
            <tr className="bg-primary">
              <th>{t('driver.txt6')}</th>
              <td className="grid h-52 w-1/2 grid-cols-2 grid-rows-1 gap-2">
                {driver?.pictures?.map(({ url }, index) => {
                  return (
                    <div
                      key={url}
                      onClick={onClickPicture({ url })}
                      role="button"
                      aria-hidden="true"
                    >
                      <img
                        alt={url}
                        className={`row-start-1 row-end-1 flex flex-col col-start-${
                          index + 1
                        }} col-end-${
                          index + 2
                        } max-h-40 w-full cursor-pointer object-contain p-1`}
                        src={url}
                      />
                    </div>
                  )
                })}
              </td>
            </tr>
            <tr>
              <th>{t('driver.txt7')}</th>
              <td>
                <time dateTime={driver.dateOfBirth} title={driver.dateOfBirth}>
                  {formatDate({ date: driver.dateOfBirth, withTime: false })}
                </time>
              </td>
            </tr>
            <tr>
              <th>{t('driver.txt8')}</th>
              <td>{checkboxInputTag(driver.enabled)}</td>
            </tr>
            <tr>
              <th>{t('driver.txt9')}</th>
              <td>
                <time dateTime={driver.createdAt} title={driver.createdAt}>
                  {formatDate({ date: driver.createdAt })}
                </time>
              </td>
            </tr>
            <tr>
              <th>{t('driver.txt10')}</th>
              <td>
                <time dateTime={driver.updatedAt} title={driver.updatedAt}>
                  {formatDate({ date: driver.updatedAt })}
                </time>
              </td>
            </tr>
            <tr>
              <th>{t('driver.txt11')}</th>
              <td>{`${driver.createdBy?.firstName || ''} ${
                driver.createdBy?.lastName || ''
              }`}</td>
            </tr>
            <tr>
              <th>{t('driver.txt12')}</th>
              <td>{`${driver.updatedBy?.firstName || ''} ${
                driver.updatedBy?.lastName || ''
              }`}</td>
            </tr>
            <tr>
              <th>{t('driver.txt13')}</th>
              <td>{driver.organization?.name}</td>
            </tr>
            <tr>
              <th>{t('driver.txt14')}</th>
              <td>{driver.truck?.plate}</td>
            </tr>
          </tbody>
        </table>
        {imageUrl && (
          <Lightbox
            enableZoom={true}
            mainSrc={imageUrl}
            onCloseRequest={onClosePicture}
            zoomInLabel="Zoom in"
            zoomOutLabel="Zoom out"
            closeLabel="Close"
          />
        )}
      </div>
      <nav className="rw-button-group">
        <Link
          to={routes.editDriver({ id: driver.id })}
          className="rw-button rw-button-blue"
        >
          {t('driver.txt15')}
        </Link>
      </nav>
    </>
  )
}

export default Driver
