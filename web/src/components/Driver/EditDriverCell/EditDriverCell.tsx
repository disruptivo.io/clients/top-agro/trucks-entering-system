import { useTranslation } from 'react-i18next'
import type { EditDriverById, UpdateDriverInput } from 'types/graphql'

import { useAuth } from '@redwoodjs/auth'
import { navigate, routes } from '@redwoodjs/router'
import type { CellSuccessProps, CellFailureProps } from '@redwoodjs/web'
import { useMutation } from '@redwoodjs/web'
import { toast } from '@redwoodjs/web/toast'

import DriverForm from 'src/components/Driver/DriverForm'

export const QUERY = gql`
  query EditDriverById($id: Int!) {
    driver: driver(id: $id) {
      id
      firstName
      lastName
      identification
      pictures {
        id
        url
      }
      dateOfBirth
      enabled
      createdAt
      updatedAt
      createdBy {
        firstName
        lastName
      }
      updatedBy {
        firstName
        lastName
      }
      organization {
        id
        name
      }
      truck {
        id
        plate
      }
    }
  }
`
const UPDATE_DRIVER_MUTATION = gql`
  mutation UpdateDriverMutation($id: Int!, $input: UpdateDriverInput!) {
    updateDriver(id: $id, input: $input) {
      id
      firstName
      lastName
      identification
      pictures {
        url
      }
      dateOfBirth
      enabled
      createdAt
      updatedAt
      createdBy {
        firstName
        lastName
      }
      updatedBy {
        firstName
        lastName
      }
      organization {
        id
        name
      }
      truck {
        id
        plate
      }
    }
  }
`

export const Loading = () => {
  const { t } = useTranslation()

  return <div>{t('editDriverCell.txt1')}</div>
}

export const Failure = ({ error }: CellFailureProps) => (
  <div className="rw-cell-error">{error?.message}</div>
)

export const Success = ({ driver }: CellSuccessProps<EditDriverById>) => {
  const { t } = useTranslation()
  const { currentUser } = useAuth()
  const [updateDriver, { loading, error }] = useMutation(
    UPDATE_DRIVER_MUTATION,
    {
      onCompleted: () => {
        toast.success(t('editDriverCell.txt2'))
        navigate(routes.drivers())
      },
      onError: (error) => {
        toast.error(error.message)
      },
    }
  )

  const onSave = (
    input: UpdateDriverInput,
    id: EditDriverById['driver']['id']
  ) => {
    updateDriver({
      variables: { id, input: { ...input, updatedById: currentUser.id } },
    })
  }

  return (
    <div className="rw-segment">
      <header className="rw-segment-header">
        <h2 className="rw-heading rw-heading-secondary">
          {t('editDriverCell.txt3', { driverId: driver.id })}
        </h2>
      </header>
      <div className="rw-segment-main">
        <DriverForm
          driver={driver}
          onSave={onSave}
          error={error}
          loading={loading}
        />
      </div>
    </div>
  )
}
