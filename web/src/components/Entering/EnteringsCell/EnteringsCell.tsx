import { useTranslation } from 'react-i18next'
import type { FindEnterings } from 'types/graphql'

import { Link, routes } from '@redwoodjs/router'
import type { CellSuccessProps, CellFailureProps } from '@redwoodjs/web'

import Enterings from 'src/components/Entering/Enterings'

export const QUERY = gql`
  query FindEnterings(
    $page: Int!
    $size: Int!
    $filter: String
    $startDate: String
    $endDate: String
  ) {
    enterings(
      page: $page
      size: $size
      filter: $filter
      startDate: $startDate
      endDate: $endDate
    ) {
      data {
        id
        enteringAt
        exitAt
        driver {
          firstName
          lastName
        }
        truck {
          plate
        }
        createdBy {
          firstName
          lastName
        }
        updatedBy {
          firstName
          lastName
        }
      }
      count
      isEmpty
    }
  }
`

export const beforeQuery = ({
  page,
  size,
  filter = '',
  startDate,
  endDate,
}) => {
  return { variables: { page, size, filter, startDate, endDate } }
}

export const Loading = () => {
  const { t } = useTranslation()

  return <div>{t('enteringsCell.txt1')}</div>
}

export const isEmpty = (data) => {
  return data.enterings.isEmpty
}

export const Empty = () => {
  const { t } = useTranslation()

  return (
    <div className="rw-text-center">
      {t('enteringsCell.txt2')}
      <Link to={routes.newEntering()} className="rw-link">
        {t('enteringsCell.txt3')}
      </Link>
    </div>
  )
}

export const Failure = ({ error }: CellFailureProps) => (
  <div className="rw-cell-error">{error?.message}</div>
)

export const Success = ({ enterings }: CellSuccessProps<FindEnterings>) => {
  return <Enterings enterings={enterings.data} count={enterings.count} />
}
