import { useTranslation } from 'react-i18next'
import type { CreateEnteringInput } from 'types/graphql'

import { useAuth } from '@redwoodjs/auth'
import { navigate, routes } from '@redwoodjs/router'
import { useMutation } from '@redwoodjs/web'
import { toast } from '@redwoodjs/web/toast'

import EnteringForm from 'src/components/Entering/EnteringForm'

const CREATE_ENTERING_MUTATION = gql`
  mutation CreateEnteringMutation($input: CreateEnteringInput!) {
    createEntering(input: $input) {
      id
    }
  }
`

const NewEntering = () => {
  const { t } = useTranslation()
  const { currentUser } = useAuth()
  const [createEntering, { loading, error }] = useMutation(
    CREATE_ENTERING_MUTATION,
    {
      onCompleted: () => {
        toast.success(t('newEntering.txt1'))
        navigate(routes.enterings())
      },
      onError: (error) => {
        toast.error(error.message)
      },
    }
  )

  const onSave = (input: CreateEnteringInput) => {
    createEntering({
      variables: {
        input: {
          ...input,
          driverId: Number(input.driverId),
          enteringAt: new Date(),
          createdById: currentUser.id,
        },
      },
    })
  }

  return (
    <div className="rw-segment">
      <header className="rw-segment-header">
        <h2 className="rw-heading rw-heading-secondary">
          {t('newEntering.txt2')}
        </h2>
      </header>
      <div className="rw-segment-main">
        <EnteringForm onSave={onSave} loading={loading} error={error} />
      </div>
    </div>
  )
}

export default NewEntering
