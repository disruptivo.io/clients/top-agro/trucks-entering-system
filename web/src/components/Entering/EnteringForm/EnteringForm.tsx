import { useEffect, useState } from 'react'

import QrReader from 'modern-react-qr-reader'
import { useTranslation } from 'react-i18next'
import type { EditEnteringById, UpdateEnteringInput } from 'types/graphql'

import {
  Form,
  FormError,
  FieldError,
  Label,
  TextField,
  Submit,
  SelectField,
  useForm,
  Controller,
} from '@redwoodjs/forms'
import type { RWGqlError } from '@redwoodjs/forms'
import { useQuery } from '@redwoodjs/web'
import { toast } from '@redwoodjs/web/dist/toast'

type FormEntering = NonNullable<EditEnteringById['entering']>

interface EnteringFormProps {
  entering?: EditEnteringById['entering']
  onSave: (data: UpdateEnteringInput, id?: FormEntering['id']) => void
  error: RWGqlError
  loading: boolean
}

const DRIVERS_BY_TRUCK = gql`
  query driversByTruck($plate: String!) {
    driversByTruck(plate: $plate) {
      id
      firstName
      lastName
    }
  }
`

const EnteringForm = (props: EnteringFormProps) => {
  const formMethods = useForm()
  const [plate, setPlate] = useState('')
  const [truckId, setTruckId] = useState(props?.entering?.truck?.id || null)
  const { t } = useTranslation()
  const { data, refetch: refetchDriversByTruck } = useQuery(DRIVERS_BY_TRUCK, {
    variables: {
      plate: props?.entering?.truck?.plate || '',
    },
  })

  const onSubmit = (data: FormEntering) => {
    props.onSave(
      { ...data, truckId: Number(truckId), driverId: Number(data.driverId) },
      props?.entering?.id
    )
  }

  const onQRScanError = (error) => {
    toast.error(error.message)
  }

  const onQRScan = (data) => {
    if (data) {
      const [truckId, plate] = data.split(',')
      setTruckId(truckId)
      setPlate(plate)
    }
  }

  useEffect(() => {
    if (plate) {
      formMethods.setValue('truckId', plate)
      refetchDriversByTruck({
        plate,
      })
    }
  }, [plate])

  return (
    <div className="rw-form-wrapper flex flex-col">
      {!props?.entering?.truck?.plate ? (
        <QrReader
          onError={onQRScanError}
          onScan={onQRScan}
          className="h-96 w-96 self-center"
        />
      ) : null}

      <Form
        onSubmit={formMethods.handleSubmit(onSubmit)}
        error={props.error}
        formMethods={formMethods}
      >
        <FormError
          error={props.error}
          wrapperClassName="rw-form-error-wrapper"
          titleClassName="rw-form-error-title"
          listClassName="rw-form-error-list"
        />

        <Label
          name="driverId"
          className="rw-label"
          errorClassName="rw-label rw-label-error"
        >
          {t('enteringForm.txt1')}
        </Label>

        <Controller
          control={formMethods.control}
          name="driverId"
          defaultValue={props?.entering?.driver?.id || -1}
          render={({ field }) => {
            const { name, value, onChange } = field
            return (
              <SelectField
                className="rw-input"
                errorClassName="rw-input rw-input-error"
                name={name}
                value={value}
                onChange={onChange}
                validation={{
                  required: {
                    value: true,
                    message: t('enteringForm.txt2'),
                  },
                  validate: {
                    matchesInitialValue: (value) =>
                      value != -1 || t('enteringForm.txt2'),
                  },
                }}
              >
                <option value={-1}>{t('enteringForm.txt3')}</option>
                {data?.driversByTruck?.map((driver, index) => (
                  <option key={index} value={driver.id}>
                    {`${driver.firstName} ${driver.lastName}`}
                  </option>
                ))}
              </SelectField>
            )
          }}
        />

        <FieldError name="driverId" className="rw-field-error" />

        <Label
          name="truckId"
          className="rw-label"
          errorClassName="rw-label rw-label-error"
        >
          {t('enteringForm.txt4')}
        </Label>

        <Controller
          control={formMethods.control}
          name="truckId"
          defaultValue={props?.entering?.truck?.plate || plate}
          render={({ field }) => {
            const { name, value } = field
            return (
              <TextField
                name={name}
                value={value}
                className="rw-input"
                errorClassName="rw-input rw-input-error"
                validation={{
                  required: {
                    value: true,
                    message: t('enteringForm.txt5'),
                  },
                }}
              />
            )
          }}
        />

        <FieldError name="truckId" className="rw-field-error" />

        <div className="rw-button-group">
          <Submit disabled={props.loading} className="rw-button rw-button-blue">
            {t('enteringForm.txt6')}
          </Submit>
        </div>
      </Form>
    </div>
  )
}

export default EnteringForm
