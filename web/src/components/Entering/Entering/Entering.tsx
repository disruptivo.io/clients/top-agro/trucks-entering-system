import { useTranslation } from 'react-i18next'
import type { FindEnteringById } from 'types/graphql'

import { Link, routes } from '@redwoodjs/router'

import { formatDate } from 'src/components/helpers/date'

interface Props {
  entering: NonNullable<FindEnteringById['entering']>
}

const Entering = ({ entering }: Props) => {
  const { t } = useTranslation()

  return (
    <>
      <div className="rw-segment">
        <header className="rw-segment-header">
          <h2 className="rw-heading rw-heading-secondary">
            {t('entering.txt1', { enteringId: entering.id })}
          </h2>
        </header>
        <table className="rw-table">
          <tbody>
            <tr>
              <th>{t('entering.txt2')}</th>
              <td>{entering.id}</td>
            </tr>
            <tr>
              <th>{t('entering.txt3')}</th>
              <td>
                <time
                  dateTime={entering.enteringAt}
                  title={entering.enteringAt}
                >
                  {formatDate({ date: entering.enteringAt })}
                </time>
              </td>
            </tr>
            <tr>
              <th>{t('entering.txt4')}</th>
              <td>
                <time dateTime={entering.exitAt} title={entering.exitAt}>
                  {formatDate({ date: entering.exitAt })}
                </time>
              </td>
            </tr>
            <tr>
              <th>{t('entering.txt5')}</th>
              <td>{`${entering.driver.firstName} ${entering.driver.lastName}`}</td>
            </tr>
            <tr>
              <th>{t('entering.txt6')}</th>
              <td>{entering.truck.plate}</td>
            </tr>
            <tr>
              <th>{t('entering.txt7')}</th>
              <td>{`${entering.createdBy.firstName} ${entering.createdBy.lastName}`}</td>
            </tr>
            <tr>
              <th>{t('entering.txt8')}</th>
              <td>{`${entering.updatedBy?.firstName || ''} ${
                entering.updatedBy?.lastName || ''
              }`}</td>
            </tr>
          </tbody>
        </table>
      </div>
      <nav className="rw-button-group">
        <Link
          to={routes.editEntering({ id: entering.id })}
          className="rw-button rw-button-blue"
        >
          {t('entering.txt9')}
        </Link>
      </nav>
    </>
  )
}

export default Entering
