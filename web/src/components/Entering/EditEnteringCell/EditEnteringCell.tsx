import { useTranslation } from 'react-i18next'
import type { EditEnteringById, UpdateEnteringInput } from 'types/graphql'

import { useAuth } from '@redwoodjs/auth'
import { navigate, routes } from '@redwoodjs/router'
import type { CellSuccessProps, CellFailureProps } from '@redwoodjs/web'
import { useMutation } from '@redwoodjs/web'
import { toast } from '@redwoodjs/web/toast'

import EnteringForm from 'src/components/Entering/EnteringForm'

export const QUERY = gql`
  query EditEnteringById($id: Int!) {
    entering: entering(id: $id) {
      id
      driver {
        id
      }
      truck {
        id
        plate
      }
    }
  }
`
const UPDATE_ENTERING_MUTATION = gql`
  mutation UpdateEnteringMutation($id: Int!, $input: UpdateEnteringInput!) {
    updateEntering(id: $id, input: $input) {
      id
      enteringAt
      exitAt
      driverId
      truckId
      createdById
      updatedById
    }
  }
`

export const Loading = () => {
  const { t } = useTranslation()

  return <div>{t('editEnteringCell.txt1')}</div>
}

export const Failure = ({ error }: CellFailureProps) => (
  <div className="rw-cell-error">{error?.message}</div>
)

export const Success = ({ entering }: CellSuccessProps<EditEnteringById>) => {
  const { t } = useTranslation()
  const { currentUser } = useAuth()
  const [updateEntering, { loading, error }] = useMutation(
    UPDATE_ENTERING_MUTATION,
    {
      onCompleted: () => {
        toast.success(t('editEnteringCell.txt2'))
        navigate(routes.enterings())
      },
      onError: (error) => {
        toast.error(error.message)
      },
    }
  )

  const onSave = (
    input: UpdateEnteringInput,
    id: EditEnteringById['entering']['id']
  ) => {
    updateEntering({
      variables: { id, input: { ...input, updatedById: currentUser.id } },
    })
  }

  return (
    <div className="rw-segment">
      <header className="rw-segment-header">
        <h2 className="rw-heading rw-heading-secondary">
          {t('editEnteringCell.txt3', { enteringId: entering.id })}
        </h2>
      </header>
      <div className="rw-segment-main">
        <EnteringForm
          entering={entering}
          onSave={onSave}
          error={error}
          loading={loading}
        />
      </div>
    </div>
  )
}
