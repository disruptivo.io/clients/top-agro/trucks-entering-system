import { useTranslation } from 'react-i18next'
import type { FindEnteringById } from 'types/graphql'

import type { CellSuccessProps, CellFailureProps } from '@redwoodjs/web'

import Entering from 'src/components/Entering/Entering'

export const QUERY = gql`
  query FindEnteringById($id: Int!) {
    entering: entering(id: $id) {
      id
      enteringAt
      exitAt
      driver {
        firstName
        lastName
      }
      truck {
        plate
      }
      createdBy {
        firstName
        lastName
      }
      updatedBy {
        firstName
        lastName
      }
    }
  }
`

export const Loading = () => {
  const { t } = useTranslation()

  return <div>{t('enteringCell.txt1')}</div>
}

export const Empty = () => {
  const { t } = useTranslation()

  return <div>{t('enteringCell.txt2')}</div>
}

export const Failure = ({ error }: CellFailureProps) => (
  <div className="rw-cell-error">{error?.message}</div>
)

export const Success = ({ entering }: CellSuccessProps<FindEnteringById>) => {
  return <Entering entering={entering} />
}
