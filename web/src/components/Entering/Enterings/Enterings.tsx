import { useState } from 'react'

import { useMutation } from '@apollo/client'
import { useTranslation } from 'react-i18next'
import type { FindEnterings } from 'types/graphql'

import { Link, routes, useParams } from '@redwoodjs/router'
import { toast } from '@redwoodjs/web/dist/toast'

import ConfirmationModal from 'src/components/ConfirmationModal/ConfirmationModal'
import FilterForm from 'src/components/FilterForm/FilterForm'
import { formatDate } from 'src/components/helpers/date'
import Paginator from 'src/components/Paginator/Paginator'
import { config } from 'src/config/config'

const MAX_STRING_LENGTH = 150
const UPDATE_ENTERING_MUTATION = gql`
  mutation UpdateEnteringMutation($id: Int!, $input: UpdateEnteringInput!) {
    updateEntering(id: $id, input: $input) {
      id
      exitAt
      updatedBy {
        firstName
        lastName
      }
    }
  }
`

const truncate = (value: string | number) => {
  const output = value?.toString()
  if (output?.length > MAX_STRING_LENGTH) {
    return output.substring(0, MAX_STRING_LENGTH) + '...'
  }
  return output ?? ''
}

const EnteringsList = ({ enterings, count }: FindEnterings) => {
  const { t } = useTranslation()
  const { page = 1 } = useParams()
  const [enteringId, setEnteringId] = useState(null)
  const [updateEntering, { loading }] = useMutation(UPDATE_ENTERING_MUTATION, {
    onCompleted: () => {
      setEnteringId(null)
      toast.success(t('enterings.txt10'))
    },
    onError: (error) => {
      setEnteringId(null)
      toast.error(error.message)
    },
  })

  const onClick = (enteringId) => () => {
    setEnteringId(enteringId)
  }

  const onConfirm = () => {
    updateEntering({
      variables: {
        id: enteringId,
        input: {
          exitAt: new Date(),
        },
      },
    })
  }

  const onCancel = () => {
    setEnteringId(null)
  }

  return (
    <div className="rw-segment rw-table-wrapper-responsive h-full">
      <ConfirmationModal
        onConfirm={onConfirm}
        onCancel={onCancel}
        visible={!!enteringId}
        message={t('enterings.txt11', { enteringId })}
      />
      <FilterForm
        className="col-start-3 col-end-13 row-start-1 mb-5 self-end"
        hideEnabled
      />
      <table className="rw-table h-3/4">
        <thead>
          <tr>
            <th>{t('enterings.txt1')}</th>
            <th>{t('enterings.txt2')}</th>
            <th>{t('enterings.txt3')}</th>
            <th>{t('enterings.txt4')}</th>
            <th>{t('enterings.txt5')}</th>
            <th className="hidden lg:table-cell">{t('enterings.txt6')}</th>
            <th className="hidden lg:table-cell">{t('enterings.txt7')}</th>
            <th>&nbsp;</th>
          </tr>
        </thead>
        <tbody>
          {enterings.map((entering) => (
            <tr className="h-1" key={entering.id}>
              <td>{truncate(entering.id)}</td>
              <td>
                <time
                  dateTime={entering.enteringAt}
                  title={entering.enteringAt}
                >
                  {formatDate({ date: entering.enteringAt })}
                </time>
              </td>
              <td>
                <time dateTime={entering.exitAt} title={entering.exitAt}>
                  {formatDate({ date: entering.exitAt })}
                </time>
              </td>
              <td>
                {' '}
                {truncate(
                  `${entering.driver?.firstName || ''} ${
                    entering.driver?.lastName || ''
                  }`
                )}
              </td>
              <td>{truncate(entering?.truck?.plate)}</td>
              <td className="hidden lg:table-cell">
                {truncate(
                  `${entering.createdBy.firstName} ${entering.createdBy.lastName}`
                )}
              </td>
              <td className="hidden lg:table-cell">
                {truncate(
                  `${entering.updatedBy?.firstName || ''} ${
                    entering.updatedBy?.lastName || ''
                  }`
                )}
              </td>
              <td>
                <nav className="rw-table-actions">
                  <Link
                    to={routes.entering({ id: entering.id })}
                    title={t('enterings.txt12', { enteringId: entering.id })}
                    className="rw-button rw-button-small"
                  >
                    {t('enterings.txt8')}
                  </Link>
                  <Link
                    to={routes.editEntering({ id: entering.id })}
                    title={t('enterings.txt13', { enteringId: entering.id })}
                    className="rw-button rw-button-small"
                  >
                    {t('enterings.txt14')}
                  </Link>
                  <button
                    disabled={!!entering.exitAt || loading}
                    className="rw-button rw-button-small disabled:cursor-not-allowed disabled:text-dark-300 disabled:hover:text-dark-300"
                    onClick={onClick(entering.id)}
                  >
                    {t('enterings.txt9')}
                  </button>
                </nav>
              </td>
            </tr>
          ))}
          <tr />
        </tbody>
      </table>
      <Paginator
        disablePrevious={page < 2 || !page}
        disableNext={parseInt(page) * config.pagination.size >= count}
        className="mt-5"
        pageName="enterings"
      />
    </div>
  )
}

export default EnteringsList
