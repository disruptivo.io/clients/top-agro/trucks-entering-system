import { useTranslation } from 'react-i18next'
import type {
  EditOrganizationById,
  UpdateOrganizationInput,
} from 'types/graphql'

import { useAuth } from '@redwoodjs/auth'
import { navigate, routes } from '@redwoodjs/router'
import type { CellSuccessProps, CellFailureProps } from '@redwoodjs/web'
import { useMutation } from '@redwoodjs/web'
import { toast } from '@redwoodjs/web/toast'

import OrganizationForm from 'src/components/Organization/OrganizationForm'

export const QUERY = gql`
  query EditOrganizationById($id: Int!) {
    organization: organization(id: $id) {
      id
      name
      enabled
      createdAt
      updatedAt
      createdById
      updatedById
    }
  }
`
const UPDATE_ORGANIZATION_MUTATION = gql`
  mutation UpdateOrganizationMutation(
    $id: Int!
    $input: UpdateOrganizationInput!
  ) {
    updateOrganization(id: $id, input: $input) {
      id
      name
      enabled
      createdAt
      updatedAt
      createdById
      updatedById
    }
  }
`

export const Loading = () => {
  const { t } = useTranslation()

  return <div>{t('editOrganizationCell.txt1')}</div>
}

export const Failure = ({ error }: CellFailureProps) => (
  <div className="rw-cell-error">{error?.message}</div>
)

export const Success = ({
  organization,
}: CellSuccessProps<EditOrganizationById>) => {
  const { t } = useTranslation()
  const { currentUser } = useAuth()
  const [updateOrganization, { loading, error }] = useMutation(
    UPDATE_ORGANIZATION_MUTATION,
    {
      onCompleted: () => {
        toast.success(t('editOrganizationCell.txt2'))
        navigate(routes.organizations())
      },
      onError: (error) => {
        toast.error(error.message)
      },
    }
  )

  const onSave = (
    input: UpdateOrganizationInput,
    id: EditOrganizationById['organization']['id']
  ) => {
    updateOrganization({
      variables: { id, input: { ...input, updatedById: currentUser.id } },
    })
  }

  return (
    <div className="rw-segment">
      <header className="rw-segment-header">
        <h2 className="rw-heading rw-heading-secondary">
          {t('editOrganizationCell.txt3', { organizationId: organization.id })}
        </h2>
      </header>
      <div className="rw-segment-main">
        <OrganizationForm
          organization={organization}
          onSave={onSave}
          error={error}
          loading={loading}
        />
      </div>
    </div>
  )
}
