import { useTranslation } from 'react-i18next'
import type { FindOrganizationById } from 'types/graphql'

import { Link, routes } from '@redwoodjs/router'

import { formatDate } from 'src/components/helpers/date'

const checkboxInputTag = (checked: boolean) => {
  return <input type="checkbox" checked={checked} disabled />
}

interface Props {
  organization: NonNullable<FindOrganizationById['organization']>
}

const Organization = ({ organization }: Props) => {
  const { t } = useTranslation()

  return (
    <>
      <div className="rw-segment">
        <header className="rw-segment-header">
          <h2 className="rw-heading rw-heading-secondary">
            {t('organization.txt1', { organizationId: organization.id })}
          </h2>
        </header>
        <table className="rw-table">
          <tbody>
            <tr>
              <th>{t('organization.txt2')}</th>
              <td>{organization.id}</td>
            </tr>
            <tr>
              <th>{t('organization.txt3')}</th>
              <td>{organization.name}</td>
            </tr>
            <tr>
              <th>{t('organization.txt4')}</th>
              <td>{checkboxInputTag(organization.enabled)}</td>
            </tr>
            <tr>
              <th>{t('organization.txt5')}</th>
              <td>
                <time
                  dateTime={organization.createdAt}
                  title={organization.createdAt}
                >
                  {formatDate({ date: organization.createdAt })}
                </time>
              </td>
            </tr>
            <tr>
              <th>{t('organization.txt6')}</th>
              <td>
                <time
                  dateTime={organization.updatedAt}
                  title={organization.updatedAt}
                >
                  {formatDate({ date: organization.updatedAt })}
                </time>
              </td>
            </tr>
            <tr>
              <th>{t('organization.txt7')}</th>
              <td>{`${organization.createdBy.firstName} ${organization.createdBy.lastName}`}</td>
            </tr>
            <tr>
              <th>{t('organization.txt8')}</th>
              <td>{`${organization.updatedBy?.firstName || ''} ${
                organization.updatedBy?.lastName || ''
              }`}</td>
            </tr>
          </tbody>
        </table>
      </div>
      <nav className="rw-button-group">
        <Link
          to={routes.editOrganization({ id: organization.id })}
          className="rw-button rw-button-blue"
        >
          {t('organization.txt9')}
        </Link>
      </nav>
    </>
  )
}

export default Organization
