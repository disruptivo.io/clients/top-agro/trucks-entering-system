import { useTranslation } from 'react-i18next'
import type {
  EditOrganizationById,
  UpdateOrganizationInput,
} from 'types/graphql'

import {
  Form,
  FormError,
  FieldError,
  Label,
  TextField,
  Submit,
  CheckboxField,
} from '@redwoodjs/forms'
import type { RWGqlError } from '@redwoodjs/forms'

type FormOrganization = NonNullable<EditOrganizationById['organization']>

interface OrganizationFormProps {
  organization?: EditOrganizationById['organization']
  onSave: (data: UpdateOrganizationInput, id?: FormOrganization['id']) => void
  error: RWGqlError
  loading: boolean
}

const OrganizationForm = (props: OrganizationFormProps) => {
  const { t } = useTranslation()
  const onSubmit = (data: FormOrganization) => {
    props.onSave(data, props?.organization?.id)
  }

  return (
    <div className="rw-form-wrapper">
      <Form<FormOrganization> onSubmit={onSubmit} error={props.error}>
        <FormError
          error={props.error}
          wrapperClassName="rw-form-error-wrapper"
          titleClassName="rw-form-error-title"
          listClassName="rw-form-error-list"
        />

        <Label
          name="name"
          className="rw-label"
          errorClassName="rw-label rw-label-error"
        >
          {t('organizationForm.txt1')}
        </Label>

        <TextField
          name="name"
          defaultValue={props.organization?.name}
          className="rw-input"
          errorClassName="rw-input rw-input-error"
          validation={{
            required: {
              value: true,
              message: t('organizationForm.txt2'),
            },
          }}
        />

        <FieldError name="name" className="rw-field-error" />

        {props.organization && (
          <>
            <Label
              name="enabled"
              className="rw-label"
              errorClassName="rw-label rw-label-error"
            >
              {t('organizationForm.txt3')}
            </Label>

            <CheckboxField
              name="enabled"
              defaultChecked={props.organization?.enabled}
              className="rw-input"
              errorClassName="rw-input rw-input-error"
            />

            <FieldError name="enabled" className="rw-field-error" />
          </>
        )}

        <div className="rw-button-group">
          <Submit disabled={props.loading} className="rw-button rw-button-blue">
            {t('organizationForm.txt4')}
          </Submit>
        </div>
      </Form>
    </div>
  )
}

export default OrganizationForm
