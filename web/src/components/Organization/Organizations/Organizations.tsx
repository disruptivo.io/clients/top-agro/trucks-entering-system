import { useTranslation } from 'react-i18next'
import type { FindOrganizations } from 'types/graphql'

import { Link, routes, useParams } from '@redwoodjs/router'

import FilterForm from 'src/components/FilterForm/FilterForm'
import { formatDate } from 'src/components/helpers/date'
import Paginator from 'src/components/Paginator/Paginator'
import { config } from 'src/config/config'

const MAX_STRING_LENGTH = 150

const truncate = (value: string | number) => {
  const output = value?.toString()
  if (output?.length > MAX_STRING_LENGTH) {
    return output.substring(0, MAX_STRING_LENGTH) + '...'
  }
  return output ?? ''
}

const OrganizationsList = ({ organizations, count }: FindOrganizations) => {
  const { t } = useTranslation()
  const { page = 1 } = useParams()

  return (
    <div className="rw-segment rw-table-wrapper-responsive h-full">
      <FilterForm className="col-start-3 col-end-13 row-start-1 mb-5 self-end" />
      <table className="rw-table h-3/4">
        <thead>
          <tr>
            <th>{t('organizations.txt1')}</th>
            <th>{t('organizations.txt2')}</th>
            <th className="hidden lg:table-cell">{t('organizations.txt3')}</th>
            <th className="hidden lg:table-cell">{t('organizations.txt4')}</th>
            <th className="hidden lg:table-cell">{t('organizations.txt5')}</th>
            <th className="hidden lg:table-cell">{t('organizations.txt6')}</th>
            <th>&nbsp;</th>
          </tr>
        </thead>
        <tbody>
          {organizations.map((organization) => (
            <tr className="h-1" key={organization.id}>
              <td>{truncate(organization.id)}</td>
              <td>{truncate(organization.name)}</td>
              <td className="hidden lg:table-cell">
                <time
                  dateTime={organization.createdAt}
                  title={organization.createdAt}
                >
                  {formatDate({ date: organization.createdAt })}
                </time>
              </td>
              <td className="hidden lg:table-cell">
                <time
                  dateTime={organization.updatedAt}
                  title={organization.updatedAt}
                >
                  {formatDate({ date: organization.updatedAt })}
                </time>
              </td>
              <td className="hidden lg:table-cell">
                {truncate(
                  `${organization.createdBy.firstName} ${organization.createdBy.lastName}`
                )}
              </td>
              <td className="hidden lg:table-cell">
                {truncate(
                  `${organization.updatedBy?.firstName || ''} ${
                    organization.updatedBy?.lastName || ''
                  }`
                )}
              </td>
              <td>
                <nav className="rw-table-actions">
                  <Link
                    to={routes.organization({ id: organization.id })}
                    title={t('organizations.txt9', {
                      organizationId: organization.id,
                    })}
                    className="rw-button rw-button-small"
                  >
                    {t('organizations.txt7')}
                  </Link>
                  <Link
                    to={routes.editOrganization({ id: organization.id })}
                    title={t('organizations.txt10', {
                      organizationId: organization.id,
                    })}
                    className="rw-button rw-button-small"
                  >
                    {t('organizations.txt8')}
                  </Link>
                </nav>
              </td>
            </tr>
          ))}
          <tr />
        </tbody>
      </table>
      <Paginator
        disablePrevious={page < 2 || !page}
        disableNext={parseInt(page) * config.pagination.size >= count}
        className="mt-5"
        pageName="organizations"
      />
    </div>
  )
}

export default OrganizationsList
