import { useTranslation } from 'react-i18next'
import type { FindOrganizationById } from 'types/graphql'

import type { CellSuccessProps, CellFailureProps } from '@redwoodjs/web'

import Organization from 'src/components/Organization/Organization'

export const QUERY = gql`
  query FindOrganizationById($id: Int!) {
    organization: organization(id: $id) {
      id
      name
      enabled
      createdAt
      updatedAt
      createdBy {
        firstName
        lastName
      }
      updatedBy {
        firstName
        lastName
      }
    }
  }
`

export const Loading = () => {
  const { t } = useTranslation()

  return <div>{t('organizationCell.txt1')}</div>
}

export const Empty = () => {
  const { t } = useTranslation()

  return <div>{t('organizationCell.txt2')}</div>
}

export const Failure = ({ error }: CellFailureProps) => (
  <div className="rw-cell-error">{error?.message}</div>
)

export const Success = ({
  organization,
}: CellSuccessProps<FindOrganizationById>) => {
  return <Organization organization={organization} />
}
