import { useState } from 'react'

import { useTranslation } from 'react-i18next'
import Lightbox from 'react-image-lightbox'
import type { FindTruckById } from 'types/graphql'

import { Link, routes } from '@redwoodjs/router'

import { formatDate } from 'src/components/helpers/date'

const checkboxInputTag = (checked: boolean) => {
  return <input type="checkbox" checked={checked} disabled />
}

interface Props {
  truck: NonNullable<FindTruckById['truck']>
}

const Truck = ({ truck }: Props) => {
  const { t } = useTranslation()
  const [imageUrl, setImageUrl] = useState('')
  const onClickPicture =
    ({ url }) =>
    () => {
      setImageUrl(url)
    }

  const onClosePicture = () => {
    setImageUrl('')
  }

  return (
    <>
      <div className="rw-segment">
        <header className="rw-segment-header">
          <h2 className="rw-heading rw-heading-secondary">
            {t('truck.txt1', { truckId: truck.id })}
          </h2>
        </header>
        <table className="rw-table">
          <tbody>
            <tr>
              <th>{t('truck.txt2')}</th>
              <td>{truck.id}</td>
            </tr>
            <tr className="bg-primary">
              <th>{t('truck.txt3')}</th>
              <td className="grid h-52 w-1/2 grid-cols-2 grid-rows-1 gap-2">
                {truck?.pictures?.map(({ url }, index) => {
                  return (
                    <div
                      key={url}
                      onClick={onClickPicture({ url })}
                      role="button"
                      aria-hidden="true"
                    >
                      <img
                        alt={url}
                        className={`row-start-1 row-end-1 flex flex-col col-start-${
                          index + 1
                        }} col-end-${
                          index + 2
                        } max-h-40 w-full cursor-pointer object-contain p-1`}
                        src={url}
                      />
                    </div>
                  )
                })}
              </td>
            </tr>
            <tr>
              <th>{t('truck.txt4')}</th>
              <td>{truck.plate}</td>
            </tr>
            <tr>
              <th>{t('truck.txt5')}</th>
              <td>{truck.color}</td>
            </tr>
            <tr>
              <th>{t('truck.txt6')}</th>
              <td>{truck.brand}</td>
            </tr>
            <tr>
              <th>{t('truck.txt7')}</th>
              <td>{truck.model}</td>
            </tr>
            <tr>
              <th>{t('truck.txt8')}</th>
              <td>{checkboxInputTag(truck.enabled)}</td>
            </tr>
            <tr>
              <th>{t('truck.txt9')}</th>
              <td>
                <time dateTime={truck.createdAt} title={truck.createdAt}>
                  {formatDate({ date: truck.createdAt })}
                </time>
              </td>
            </tr>
            <tr>
              <th>{t('truck.txt10')}</th>
              <td>
                <time dateTime={truck.updatedAt} title={truck.updatedAt}>
                  {formatDate({ date: truck.updatedAt })}
                </time>
              </td>
            </tr>
            <tr>
              <th>{t('truck.txt11')}</th>
              <td>{`${truck.createdBy?.firstName || ''} ${
                truck.createdBy?.lastName || ''
              }`}</td>
            </tr>
            <tr>
              <th>{t('truck.txt12')}</th>
              <td>{`${truck.updatedBy?.firstName || ''} ${
                truck.updatedBy?.lastName || ''
              }`}</td>
            </tr>
          </tbody>
        </table>
        {imageUrl && (
          <Lightbox
            enableZoom={true}
            mainSrc={imageUrl}
            onCloseRequest={onClosePicture}
            zoomInLabel="Zoom in"
            zoomOutLabel="Zoom out"
            closeLabel="Close"
          />
        )}
      </div>
      <nav className="rw-button-group">
        <Link
          to={routes.editTruck({ id: truck.id })}
          className="rw-button rw-button-blue"
        >
          {t('truck.txt13')}
        </Link>
      </nav>
    </>
  )
}

export default Truck
