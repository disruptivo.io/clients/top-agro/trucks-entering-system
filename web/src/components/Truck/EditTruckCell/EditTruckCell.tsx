import { useTranslation } from 'react-i18next'
import type { EditTruckById, UpdateTruckInput } from 'types/graphql'

import { useAuth } from '@redwoodjs/auth'
import { navigate, routes } from '@redwoodjs/router'
import type { CellSuccessProps, CellFailureProps } from '@redwoodjs/web'
import { useMutation } from '@redwoodjs/web'
import { toast } from '@redwoodjs/web/toast'

import TruckForm from 'src/components/Truck/TruckForm'

export const QUERY = gql`
  query EditTruckById($id: Int!) {
    truck: truck(id: $id) {
      id
      pictures {
        id
        url
      }
      plate
      color
      brand
      model
      enabled
      createdAt
      updatedAt
      createdBy {
        firstName
        lastName
      }
      updatedBy {
        firstName
        lastName
      }
    }
  }
`
const UPDATE_TRUCK_MUTATION = gql`
  mutation UpdateTruckMutation($id: Int!, $input: UpdateTruckInput!) {
    updateTruck(id: $id, input: $input) {
      id
      pictures {
        id
        url
      }
      plate
      color
      brand
      model
      enabled
      createdAt
      updatedAt
      createdBy {
        firstName
        lastName
      }
      updatedBy {
        firstName
        lastName
      }
    }
  }
`

export const Loading = () => {
  const { t } = useTranslation()

  return <div>{t('editTruckCell.txt1')}</div>
}

export const Failure = ({ error }: CellFailureProps) => (
  <div className="rw-cell-error">{error?.message}</div>
)

export const Success = ({ truck }: CellSuccessProps<EditTruckById>) => {
  const { t } = useTranslation()
  const { currentUser } = useAuth()
  const [updateTruck, { loading, error }] = useMutation(UPDATE_TRUCK_MUTATION, {
    onCompleted: () => {
      toast.success(t('editTruckCell.txt2'))
      navigate(routes.trucks())
    },
    onError: (error) => {
      toast.error(error.message)
    },
  })

  const onSave = (
    input: UpdateTruckInput,
    id: EditTruckById['truck']['id']
  ) => {
    updateTruck({
      variables: { id, input: { ...input, updatedById: currentUser.id } },
    })
  }

  return (
    <div className="rw-segment">
      <header className="rw-segment-header">
        <h2 className="rw-heading rw-heading-secondary">
          {t('editTruckCell.txt3', { truckId: truck.id })}
        </h2>
      </header>
      <div className="rw-segment-main">
        <TruckForm
          truck={truck}
          onSave={onSave}
          error={error}
          loading={loading}
        />
      </div>
    </div>
  )
}
