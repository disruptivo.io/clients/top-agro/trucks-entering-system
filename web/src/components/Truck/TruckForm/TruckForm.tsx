import { useEffect, useState } from 'react'

import * as filestack from 'filestack-js'
import { PickerOverlay } from 'filestack-react'
import { useTranslation } from 'react-i18next'
import Lightbox from 'react-image-lightbox'
import type { EditTruckById, UpdateTruckInput } from 'types/graphql'

import {
  Form,
  FormError,
  FieldError,
  Label,
  TextField,
  CheckboxField,
  Submit,
  useForm,
  Controller,
} from '@redwoodjs/forms'
import type { RWGqlError } from '@redwoodjs/forms'
import { toast } from '@redwoodjs/web/dist/toast'

const filestackClient = filestack.init(process.env.FILESTACK_API_KEY)
filestackClient.setSecurity({
  policy: process.env.FILESTACK_POLICY,
  signature: process.env.FILESTACK_SIGNATURE,
})

type FormTruck = NonNullable<EditTruckById['truck']>

interface TruckFormProps {
  truck?: EditTruckById['truck']
  onSave: (data: UpdateTruckInput, id?: FormTruck['id']) => void
  error: RWGqlError
  loading: boolean
}

const TruckForm = (props: TruckFormProps) => {
  const formMethods = useForm()
  const fallbackPictures = Array.from({ length: 2 })
    .fill(0)
    .map(() => ({ url: '' }))
  const mappedPictures = props?.truck?.pictures.map(({ id, url }) => ({
    id,
    url,
  }))
  const [pictures, setPictures] = useState(
    props?.truck?.pictures.length ? mappedPictures : fallbackPictures
  )
  const [showImagePicker, setShowImagePicker] = useState(false)
  const [removingPicture, setRemovingPicture] = useState(false)
  const [pictureToUpdate, setPictureToUpdate] = useState(null)
  const [imageUrl, setImageUrl] = useState('')
  const { t } = useTranslation()

  const onSubmit = (data: FormTruck) => {
    props.onSave(data, props?.truck?.id)
  }

  const removePicture =
    ({ url, index }) =>
    async () => {
      try {
        const FILE_ID = new URL(url).pathname.replace('/', '')
        setRemovingPicture(true)
        await filestackClient.remove(FILE_ID)

        setPictures((currentPictures) => {
          const pictures = [...currentPictures]
          pictures[index] = {
            ...pictures[index],
            url: '',
          }
          return pictures
        })

        setRemovingPicture(false)
      } catch (error) {
        setRemovingPicture(false)
      }
    }

  const toggleShowImagePicker = () => {
    setShowImagePicker((current) => !current)
  }

  const showImagePickerAndMarkImageForRemoval =
    ({ index }) =>
    () => {
      toggleShowImagePicker()
      setPictureToUpdate(index)
    }

  const onFileUploaded = async ({ filesFailed, filesUploaded }) => {
    try {
      if (filesFailed.length) return toast.error(t('truckForm.txt15'))

      await removePicture({
        url: pictures[pictureToUpdate].url,
        index: pictureToUpdate,
      })()

      setShowImagePicker(false)
      setPictures((currentPictures) => {
        const pictures = [...currentPictures]
        pictures[pictureToUpdate] = {
          ...pictures[pictureToUpdate],
          url: `${filesUploaded[0].url}?policy=${process.env.FILESTACK_POLICY}&signature=${process.env.FILESTACK_SIGNATURE}`,
        }
        return pictures
      })
    } catch (error) {
      console.log(error)
    }
  }

  const onClickPicture =
    ({ url }) =>
    () => {
      setImageUrl(url)
    }

  const onClosePicture = () => {
    setImageUrl('')
  }

  useEffect(() => {
    if (props?.truck?.pictures.length)
      setPictures(props?.truck?.pictures.map(({ id, url }) => ({ id, url })))
  }, [props.truck?.pictures])

  useEffect(() => {
    if (pictures.every((picture) => picture.url))
      formMethods.setValue('pictures', pictures)
    if (pictures.some((picture) => !picture.url))
      formMethods.setValue('pictures', null)
  }, [pictures])

  const renderImageButtons = () => {
    return (
      <div className="grid h-52 w-1/2 grid-cols-2 grid-rows-1 gap-2">
        {pictures.map(({ url }, index) => {
          return (
            <div
              key={index}
              className={`row-start-1 row-end-1 flex flex-col col-start-${
                index + 1
              }} col-end-${index + 2}`}
            >
              <div
                key={url}
                onClick={onClickPicture({ url })}
                role="button"
                aria-hidden="true"
              >
                <img
                  alt={url}
                  className="max-h-40 w-full object-contain p-1"
                  src={
                    url ||
                    'https://api.dicebear.com/6.x/bottts/svg?seed=Snowball&baseColor=9f0450'
                  }
                />
              </div>
              {url ? (
                <div className="flex flex-row self-center">
                  <button
                    disabled={removingPicture}
                    type="button"
                    className="rw-button rw-button-green disabled:cursor-progress disabled:border-dark-100 disabled:text-dark-100"
                    onClick={showImagePickerAndMarkImageForRemoval({ index })}
                  >
                    {t('truckForm.txt13')}
                  </button>
                </div>
              ) : (
                <button
                  disabled={removingPicture}
                  type="button"
                  className="rw-button rw-button-green disabled:cursor-progress disabled:border-dark-100 disabled:text-dark-100"
                  onClick={showImagePickerAndMarkImageForRemoval({ index })}
                >
                  {t('truckForm.txt14')}
                </button>
              )}
            </div>
          )
        })}
      </div>
    )
  }

  return (
    <div className="rw-form-wrapper">
      <Form
        onSubmit={formMethods.handleSubmit(onSubmit)}
        error={props.error}
        formMethods={formMethods}
      >
        <FormError
          error={props.error}
          wrapperClassName="rw-form-error-wrapper"
          titleClassName="rw-form-error-title"
          listClassName="rw-form-error-list"
        />

        <Label
          name="pictures"
          className="rw-label"
          errorClassName="rw-label rw-label-error"
        >
          {t('truckForm.txt1')}
        </Label>

        <Controller
          control={formMethods.control}
          name="pictures"
          rules={{
            required: {
              value: true,
              message: t('truckForm.txt2'),
            },
          }}
          render={({ field }) => {
            const { name } = field

            if (!showImagePicker) return renderImageButtons()

            return (
              <PickerOverlay
                apikey={process.env.FILESTACK_API_KEY}
                pickerOptions={{
                  lang: 'es',
                  imageDim: [800, 800],
                  fromSources: ['local_file_system', 'webcam'],
                  accept: 'image/*',
                  minFiles: 1,
                  maxFiles: 1,
                  onClose: toggleShowImagePicker,
                }}
                clientOptions={{
                  security: {
                    policy: process.env.FILESTACK_POLICY,
                    signature: process.env.FILESTACK_SIGNATURE,
                  },
                }}
                name={name}
                onUploadDone={onFileUploaded}
              />
            )
          }}
        />

        <FieldError name="pictures" className="rw-field-error" />

        <Label
          name="plate"
          className="rw-label"
          errorClassName="rw-label rw-label-error"
        >
          {t('truckForm.txt3')}
        </Label>

        <TextField
          name="plate"
          defaultValue={props.truck?.plate}
          className="rw-input"
          errorClassName="rw-input rw-input-error"
          validation={{
            required: {
              value: true,
              message: t('truckForm.txt4'),
            },
          }}
        />

        <FieldError name="plate" className="rw-field-error" />

        <Label
          name="color"
          className="rw-label"
          errorClassName="rw-label rw-label-error"
        >
          {t('truckForm.txt5')}
        </Label>

        <TextField
          name="color"
          defaultValue={props.truck?.color}
          className="rw-input"
          errorClassName="rw-input rw-input-error"
          validation={{
            required: {
              value: true,
              message: t('truckForm.txt6'),
            },
          }}
        />

        <FieldError name="color" className="rw-field-error" />

        <Label
          name="brand"
          className="rw-label"
          errorClassName="rw-label rw-label-error"
        >
          {t('truckForm.txt7')}
        </Label>

        <TextField
          name="brand"
          defaultValue={props.truck?.brand}
          className="rw-input"
          errorClassName="rw-input rw-input-error"
          validation={{
            required: {
              value: true,
              message: t('truckForm.txt8'),
            },
          }}
        />

        <FieldError name="brand" className="rw-field-error" />

        <Label
          name="model"
          className="rw-label"
          errorClassName="rw-label rw-label-error"
        >
          {t('truckForm.txt9')}
        </Label>

        <TextField
          name="model"
          defaultValue={props.truck?.model}
          className="rw-input"
          errorClassName="rw-input rw-input-error"
          validation={{
            required: {
              value: true,
              message: t('truckForm.txt10'),
            },
          }}
        />

        <FieldError name="model" className="rw-field-error" />

        {props.truck && (
          <>
            <Label
              name="enabled"
              className="rw-label"
              errorClassName="rw-label rw-label-error"
            >
              {t('truckForm.txt11')}
            </Label>

            <CheckboxField
              name="enabled"
              defaultChecked={props.truck?.enabled}
              className="rw-input"
              errorClassName="rw-input rw-input-error"
            />

            <FieldError name="enabled" className="rw-field-error" />
          </>
        )}

        <div className="rw-button-group">
          <Submit disabled={props.loading} className="rw-button rw-button-blue">
            {t('truckForm.txt12')}
          </Submit>
        </div>
        {imageUrl && (
          <Lightbox
            enableZoom={true}
            mainSrc={imageUrl}
            onCloseRequest={onClosePicture}
            zoomInLabel="Zoom in"
            zoomOutLabel="Zoom out"
            closeLabel="Close"
          />
        )}
      </Form>
    </div>
  )
}

export default TruckForm
