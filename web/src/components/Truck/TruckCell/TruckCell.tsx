import { useTranslation } from 'react-i18next'
import type { FindTruckById } from 'types/graphql'

import type { CellSuccessProps, CellFailureProps } from '@redwoodjs/web'

import Truck from 'src/components/Truck/Truck'

export const QUERY = gql`
  query FindTruckById($id: Int!) {
    truck: truck(id: $id) {
      id
      pictures {
        url
      }
      plate
      color
      brand
      model
      enabled
      createdAt
      updatedAt
      createdBy {
        firstName
        lastName
      }
      updatedBy {
        firstName
        lastName
      }
    }
  }
`

export const Loading = () => {
  const { t } = useTranslation()

  return <div>{t('truckCell.txt1')}</div>
}

export const Empty = () => {
  const { t } = useTranslation()

  return <div>{t('truckCell.txt2')}</div>
}

export const Failure = ({ error }: CellFailureProps) => (
  <div className="rw-cell-error">{error?.message}</div>
)

export const Success = ({ truck }: CellSuccessProps<FindTruckById>) => {
  return <Truck truck={truck} />
}
