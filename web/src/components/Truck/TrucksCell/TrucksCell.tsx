import { useTranslation } from 'react-i18next'
import type { FindTrucks } from 'types/graphql'

import { Link, routes } from '@redwoodjs/router'
import type { CellSuccessProps, CellFailureProps } from '@redwoodjs/web'

import Trucks from 'src/components/Truck/Trucks'

export const QUERY = gql`
  query FindTrucks(
    $page: Int!
    $size: Int!
    $filter: String
    $startDate: String
    $endDate: String
    $enabled: Boolean
  ) {
    trucks(
      page: $page
      size: $size
      filter: $filter
      startDate: $startDate
      endDate: $endDate
      enabled: $enabled
    ) {
      data {
        id
        plate
        color
        brand
        model
        createdAt
        updatedAt
        createdBy {
          firstName
          lastName
        }
        updatedBy {
          firstName
          lastName
        }
      }
      count
      isEmpty
    }
  }
`

export const beforeQuery = ({
  page,
  size,
  filter = '',
  startDate,
  endDate,
  enabled = true,
}) => {
  return { variables: { page, size, filter, startDate, endDate, enabled } }
}

export const Loading = () => {
  const { t } = useTranslation()

  return <div>{t('trucksCell.txt1')}</div>
}

export const isEmpty = (data) => {
  return data.trucks.isEmpty
}

export const Empty = () => {
  const { t } = useTranslation()

  return (
    <div className="rw-text-center">
      {t('trucksCell.txt2')}
      <Link to={routes.newTruck()} className="rw-link">
        {t('trucksCell.txt3')}
      </Link>
    </div>
  )
}

export const Failure = ({ error }: CellFailureProps) => (
  <div className="rw-cell-error">{error?.message}</div>
)

export const Success = ({ trucks }: CellSuccessProps<FindTrucks>) => {
  return <Trucks trucks={trucks.data} count={trucks.count} />
}
