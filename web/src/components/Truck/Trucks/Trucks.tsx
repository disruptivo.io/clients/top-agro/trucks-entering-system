import { useTranslation } from 'react-i18next'
import type { FindTrucks } from 'types/graphql'

import { Link, routes, useParams } from '@redwoodjs/router'

import FilterForm from 'src/components/FilterForm/FilterForm'
import { formatDate } from 'src/components/helpers/date'
import Paginator from 'src/components/Paginator/Paginator'
import { config } from 'src/config/config'

const MAX_STRING_LENGTH = 150

const truncate = (value: string | number) => {
  const output = value?.toString()
  if (output?.length > MAX_STRING_LENGTH) {
    return output.substring(0, MAX_STRING_LENGTH) + '...'
  }
  return output ?? ''
}

const TrucksList = ({ trucks, count }: FindTrucks) => {
  const { t } = useTranslation()
  const { page = 1 } = useParams()

  return (
    <div className="rw-segment rw-table-wrapper-responsive h-full">
      <FilterForm className="col-start-3 col-end-13 row-start-1 mb-5 self-end" />
      <table className="rw-table h-3/4">
        <thead>
          <tr>
            <th>{t('trucks.txt1')}</th>
            <th>{t('trucks.txt2')}</th>
            <th>{t('trucks.txt3')}</th>
            <th>{t('trucks.txt4')}</th>
            <th>{t('trucks.txt5')}</th>
            <th className="hidden lg:table-cell">{t('trucks.txt6')}</th>
            <th className="hidden lg:table-cell">{t('trucks.txt7')}</th>
            <th className="hidden lg:table-cell">{t('trucks.txt8')}</th>
            <th className="hidden lg:table-cell">{t('trucks.txt9')}</th>
            <th>&nbsp;</th>
          </tr>
        </thead>
        <tbody>
          {trucks.map((truck) => (
            <tr className="h-1" key={truck.id}>
              <td>{truncate(truck.id)}</td>
              <td>{truncate(truck.plate)}</td>
              <td>{truncate(truck.color)}</td>
              <td>{truncate(truck.brand)}</td>
              <td>{truncate(truck.model)}</td>
              <td className="hidden lg:table-cell">
                <time dateTime={truck.createdAt} title={truck.createdAt}>
                  {formatDate({ date: truck.createdAt })}
                </time>
              </td>
              <td className="hidden lg:table-cell">
                <time dateTime={truck.updatedAt} title={truck.updatedAt}>
                  {formatDate({ date: truck.updatedAt })}
                </time>
              </td>
              <td className="hidden lg:table-cell">
                {truncate(
                  `${truck.createdBy?.firstName || ''} ${
                    truck.createdBy?.lastName || ''
                  }`
                )}
              </td>
              <td className="hidden lg:table-cell">
                {truncate(
                  `${truck.updatedBy?.firstName || ''} ${
                    truck.updatedBy?.lastName || ''
                  }`
                )}
              </td>
              <td>
                <nav className="rw-table-actions">
                  <Link
                    to={routes.truck({ id: truck.id })}
                    title={t('trucks.txt10', { truckId: truck.id })}
                    className="rw-button rw-button-small"
                  >
                    {t('trucks.txt11')}
                  </Link>
                  <Link
                    to={routes.editTruck({ id: truck.id })}
                    title={t('trucks.txt12', { truckId: truck.id })}
                    className="rw-button rw-button-small"
                  >
                    {t('trucks.txt13')}
                  </Link>
                </nav>
              </td>
            </tr>
          ))}
          <tr />
        </tbody>
      </table>
      <Paginator
        disablePrevious={page < 2 || !page}
        disableNext={parseInt(page) * config.pagination.size >= count}
        className="mt-5"
        pageName="trucks"
      />
    </div>
  )
}

export default TrucksList
