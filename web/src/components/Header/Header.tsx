import { useTranslation } from 'react-i18next'

import { useAuth } from '@redwoodjs/auth'
import { Link, routes, useLocation } from '@redwoodjs/router'

const pathnameToHandlers = {
  default: {
    table: () => routes.enterings(),
    new: () => routes.newEntering(),
    title: 'header.txt9',
    label: 'header.txt10',
    roles: ['admin', 'doorman'],
  },
  organizations: {
    table: () => routes.organizations(),
    new: () => routes.newOrganization(),
    title: 'header.txt1',
    label: 'header.txt2',
    roles: ['admin', 'coordinator'],
  },
  users: {
    table: () => routes.users(),
    new: () => routes.newUser(),
    title: 'header.txt3',
    label: 'header.txt4',
    roles: ['admin', 'coordinator'],
  },
  drivers: {
    table: () => routes.drivers(),
    new: () => routes.newDriver(),
    title: 'header.txt5',
    label: 'header.txt6',
    roles: ['admin', 'coordinator'],
  },
  trucks: {
    table: () => routes.trucks(),
    new: () => routes.newTruck(),
    title: 'header.txt7',
    label: 'header.txt8',
    roles: ['admin', 'coordinator'],
  },
}
type HeaderProps = {
  className: string
}

const Header = ({ className }: HeaderProps) => {
  const { hasRole } = useAuth()
  const { t, i18n } = useTranslation()
  const { pathname } = useLocation()
  const formattedPathname = pathname.split('/')[1]
  const handler =
    pathnameToHandlers[formattedPathname] || pathnameToHandlers.default

  if (formattedPathname === 'forbidden') return null

  if (!hasRole(handler.roles)) return null

  const onChangeLanguage = ({ target: { value } }) => {
    i18n.changeLanguage(value)
  }

  return (
    <header className={`rw-header flex p-5 ${className}`}>
      <h1 className="rw-heading rw-heading-primary">
        <Link to={handler.table()} className="rw-link">
          {t(handler.title)}
        </Link>
      </h1>
      <div className="flex">
        <select
          name="languages"
          className="rw-input m-0 mr-5 h-full self-center"
          onChange={onChangeLanguage}
          defaultValue={i18n.language}
        >
          <option value="es">{t('header.txt12')}</option>
          <option value="en">{t('header.txt11')}</option>
        </select>
        <Link
          to={handler.new()}
          className="rw-button rw-button-blue min-w-max items-center"
        >
          <div className="rw-button-icon">+</div> {t(handler.label)}
        </Link>
      </div>
    </header>
  )
}

export default Header
