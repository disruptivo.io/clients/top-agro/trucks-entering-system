import React from 'react'

import { useTranslation } from 'react-i18next'

import { Link, useParams, routes } from '@redwoodjs/router'

type PaginatorProps = {
  disablePrevious: boolean
  disableNext: boolean
  className?: string
  pageName: string
}

const Paginator = ({
  disablePrevious,
  disableNext,
  className,
  pageName,
}: PaginatorProps) => {
  const { t } = useTranslation()
  const { page = 1, ...rest } = useParams()

  const renderDisabledButton = ({ label }) => {
    return (
      <button
        disabled={true}
        className="rw-button rw-button-blue ml-5 cursor-not-allowed border-2 border-solid border-dark-100 disabled:bg-dark-100 disabled:hover:bg-dark-100 disabled:hover:text-primary-light"
      >
        {label}
      </button>
    )
  }

  const renderButton = ({ label, to }) => {
    return (
      <Link className="rw-button rw-button-green ml-5 mr-1" to={to}>
        {label}
      </Link>
    )
  }

  return (
    <div className={`flex flex-row justify-end ${className}`}>
      {disablePrevious
        ? renderDisabledButton({ label: t('paginator.txt1') })
        : renderButton({
            label: t('paginator.txt1'),
            to: routes[pageName]({ page: parseInt(page) - 1, ...rest }),
          })}
      {disableNext
        ? renderDisabledButton({ label: t('paginator.txt2') })
        : renderButton({
            label: t('paginator.txt2'),
            to: routes[pageName]({ page: parseInt(page) + 1, ...rest }),
          })}
    </div>
  )
}

export default Paginator
