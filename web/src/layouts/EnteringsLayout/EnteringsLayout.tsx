import { Link, routes } from '@redwoodjs/router'
import { Toaster } from '@redwoodjs/web/toast'

type EnteringLayoutProps = {
  children: React.ReactNode
}

const EnteringsLayout = ({ children }: EnteringLayoutProps) => {
  return (
    <div className="rw-scaffold">
      <Toaster toastOptions={{ className: 'rw-toast', duration: 6000 }} />
      <header className="rw-header">
        <h1 className="rw-heading rw-heading-primary">
          <Link to={routes.enterings()} className="rw-link">
            Enterings
          </Link>
        </h1>
        <Link to={routes.newEntering()} className="rw-button rw-button-green">
          <div className="rw-button-icon">+</div> New Entering
        </Link>
      </header>
      <main className="rw-main">{children}</main>
    </div>
  )
}

export default EnteringsLayout
