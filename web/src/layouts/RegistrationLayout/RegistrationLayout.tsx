import { Toaster } from '@redwoodjs/web/toast'

type RegistrationLayoutProps = {
  children: React.ReactNode
}

const RegistrationLayout = ({ children }: RegistrationLayoutProps) => {
  return (
    <main className="rw-scaffold h-full bg-primary-light">
      <Toaster toastOptions={{ className: 'rw-toast', duration: 6000 }} />
      <div className="flex h-full flex-col items-center justify-center">
        {children}
      </div>
    </main>
  )
}

export default RegistrationLayout
