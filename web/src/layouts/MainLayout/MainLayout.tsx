import { Toaster } from '@redwoodjs/web/toast'

import Header from 'src/components/Header'
import Menu from 'src/components/Menu'

type MainLayoutProps = {
  children: React.ReactNode
}

const MainLayout = ({ children }: MainLayoutProps) => {
  return (
    <main className="rw-scaffold grid h-full grid-cols-12 grid-rows-6 bg-primary-light">
      <Toaster toastOptions={{ className: 'rw-toast', duration: 6000 }} />
      <Menu className="col-start-1 col-end-3 row-start-1 row-end-7" />
      <Header className="col-start-1 col-end-13 row-start-1 self-start lg:col-start-3" />
      <div className="rw-main col-start-1 col-end-13 row-start-2 row-end-7 overflow-scroll lg:col-start-3">
        {children}
      </div>
    </main>
  )
}

export default MainLayout
