/** @type {import('tailwindcss').Config} */
const colors = require('tailwindcss/colors');

module.exports = {
  content: ['src/**/*.{js,jsx,ts,tsx}'],
  theme: {
    colors: {
      ...colors,
      'primary': '#ffffff',
      'primary-light': '#fafafa',
      'accent-200': '#9f0450',
      'accent-100': '#8eb820',
      'light-100': '#5a5b5e',
      'dark-100': '#b2b2b2',
      'dark-200': '#3F3F46',
      'dark-300': '#CACACA',
      'dark-400': '#FAFAFA',
      'error': '#F8A5A5',
      'success': '#86EFAC',
    },
    extend: {},
  },
  plugins: [],
}
