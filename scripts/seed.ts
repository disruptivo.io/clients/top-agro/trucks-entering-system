const { PrismaClient } = require('@prisma/client')

const prisma = new PrismaClient()
const { hashPassword } = require('@redwoodjs/api')

const [hashedPassword, salt] = hashPassword(process.env.PASSWORD)

prisma.user
  .create({
    data: {
      firstName: 'Admin',
      lastName: 'Admin',
      username: process.env.USERNAME,
      hashedPassword,
      salt,
      roles: 'admin',
    },
  })
  .catch((error) => console.log(error.message))
