#### 0.9.15 (2023-06-02)

##### Code Style Changes

* **web:**  add styles for disabled button (ba99f1d7)

#### 0.9.14 (2023-06-02)

##### Bug Fixes

* **web:**  fix error message when picture upload fails (b55b55b0)

#### 0.9.13 (2023-06-01)

##### Chores

* **web/api:**  move Filestack credentials to env file (e1c8c4e5)

#### 0.9.12 (2023-05-31)

##### Chores

* **web/api:**  update deploy command (652d9e22)

#### 0.9.11 (2023-05-31)

##### Chores

* **web/api:**  enable debug mode Gitlab pipeline (61f4f686)

#### 0.9.10 (2023-05-31)

##### Chores

* **web/api:**  update deploy command (9799f84f)

#### 0.9.9 (2023-05-31)

##### New Features

* **web/api:**  add build env vars to deploy command (93300ab9)

#### 0.9.8 (2023-05-30)

##### Chores

* **web/api:**  remove build envs from deploy command (1eee8654)

#### 0.9.7 (2023-05-30)

#### 0.9.6 (2023-05-30)

##### Chores

* **web/api:**  disable debug for Gitlab pipeline (1dfa598a)

#### 0.9.5 (2023-05-30)

##### Chores

* **web:**  add build env vars for deploy command (6adee8a8)

#### 0.9.4 (2023-05-30)

##### Chores

* **web/api:**  enable debug on Github pipeline (d78fe567)

#### 0.9.3 (2023-05-30)

##### Bug Fixes

* **api:**  re throw error on createOrganization method (ee422a9c)

#### 0.9.2 (2023-05-30)

##### Tests

* **api:**  fix broken tests (bd8edac3)

#### 0.9.1 (2023-05-29)

##### Chores

* **web/api:**  add NX_BRANCH env var to Gitlab pipeline (b7829b95)

### 0.9.0 (2023-05-29)

##### Chores

* **web:**
  *  install and configure react-image-lightbox (71854b7d)
  *  add Filestack env vars (ccdd43d5)
  *  install filestack libraries (ba7d417c)
*  add env vars needed for seeding data (18bdaa91)

##### Documentation Changes

* **api:**  update command for creating a branch in PlanetScale (d5a29cd3)

##### New Features

* **web/api:**
  *  add pictures to truck view (4a747d81)
  *  add pictures on creating and updating a driver (534bac2e)
  *  validate organization on Driver mutation (56c185a6)
* **web:**
  *  add texts for driver pictures (2b36dbc5)
  *  show pictures on Driver view (1c2e53c1)
  *  add options to formatDate util (257671c8)
  *  add logo (7c396b6e)
  *  add dropdown to Header for changing languages (4360d684)
* **api:**
  *  add driver pictures when fetching a driver (39b60c9e)
  *  add picture service (f9ddd547)
  *  create sdl for Picture model (26410a5a)
*  add picture model (6c699626)

##### Bug Fixes

* **api:**
  *  update pictures and drives sdls (33fae9f5)
  *  increase length of picture url (6a77e4fa)
* **web:**
  *  fix formatDate util (d517ffc8)
  *  change DateTime by Date input on Driver view (05c01d80)

##### Other Changes

* **api:**  make organization required for Driver object (a23930d9)

##### Refactors

* **api:**  use relation resolver instead of include (31a7502b)
* **web:**  remove unused fields from DriversCell (51665bba)

##### Code Style Changes

* **web:**
  *  fix styles of Paginator and Enterings (d75c3533)
  *  change colors (f48bc579)

### 0.8.0 (2022-11-25)

##### New Features

* **web:**  add roles property to User module (6ee2102c)

##### Bug Fixes

* **web:**
  *  redirect from login based on the user role (84609352)
  *  change plate field to text instead of number (3f23ee54)

### 0.7.0 (2022-11-25)

##### Documentation Changes

* **api:**  update steps for deploying changes to Planet Scale (718b5ecc)

##### New Features

* **api:**
  *  add roles to script for seeding data (ee8e31ac)
  *  return roles on CurrentUser object (9d20eea2)
  *  add role property to user model (2aea807f)
* **web:**
  *  hide components by checking roles (3206e34c)
  *  add ForbiddenPange component (707b4871)

##### Bug Fixes

* **api:**
  *  change role by roles on user model (050855bc)
  *  change roles by role on user model (bcbfaf4c)

### 0.6.0 (2022-11-23)

##### New Features

* **web:**
  *  add edit method for Enterings (ab878fe8)
  *  add ConfirmationModal to Enterings in order to set exit time (784ef676)

##### Bug Fixes

* **web:**  add missing texts to Organizations, Drivers and Enterings (83ed20bf)

### 0.5.0 (2022-11-22)

##### Chores

* **web:**  install QR reader (b4bb8d27)

##### Documentation Changes

*  update Changelog.md (78db2fc5)
* **web/api:**  update doc for deploying schema changes to PlanetScale (727be4e7)

##### New Features

* **web:**
  *  create ConfirmationModal component (58b1e3da)
  *  add QR reader to EnteringForm (93a520eb)
* **api:**  add endpoint for fetching Drivers by Truck plate (da1f9b75)

##### Bug Fixes

* **web:**
  *  add missing texts for ComfirmationModal component (d94f207c)
  *  update data to show on Enterings component (090cbabc)
  *  get plate and truck id from QR (dadb12ce)
  *  update Entering component (b7d9637b)
  *  update formatDate helper in order to show a dash when date is falsy (34faa047)
  *  fix highlight on Entering menu item (fa52f3ed)
  *  fix text of the Driver component (347fe3d7)

##### Code Style Changes

* **web:**  add z-index class to Menu in order to avoid QReader overlap it (3cdc55a2)

### 0.4.0 (2022-11-15)

##### Chores

* **web/api:**
  *  add command for seeding data on project.json file (79a2c6c9)
  *  add method for deploying databases changes to PlanetScale (9ff1061e)

##### Documentation Changes

* **web/api:**  add infor for seeding data (dc7daafb)

##### New Features

* **web/api:**  add isEmpty for using on isEmpty method of Cells (adcc0a47)
* **web:**  add Entering module (072e2ff1)
* **api:**
  *  add Entering model (111e8220)
  *  add script for seeding data (c6141e87)

##### Bug Fixes

* **web:**
  *  update redirection from Login page to Enterings page (1a54db4d)
  *  fix message when there is not organizations (7632818e)
  *  update isEmpty hook for plural cells (1c395c07)

#### 0.3.2 (2022-11-11)

##### Chores

* **web/api:**  remove vercel pull command from deploy nx command (142971c9)

##### Code Style Changes

* **web:**  fix eslint issues (b651f7a7)

#### 0.3.1 (2022-11-11)

##### Bug Fixes

* **web/api:**  execute vercel pull command before dploying (94c90d32)

### 0.3.0 (2022-11-11)

##### Chores

* **web/api:**  update deploy command for building and deploying (479db606)
* **web:**  update description of style type of our standard commit message (b21354fd)

##### Continuous Integration

* **web/api:**  remove build command from Gitlab pipeline (8aa88ae2)

##### Documentation Changes

* **web/api:**
  *  recover Readme file (065d51df)
  *  add info for updating the build command on vercel (cdbb368c)
  *  add todo for removing prebuild option of deploy command (81050bfa)
  *  add how to deploy a schema to PlanetScale (fb08f79b)

##### New Features

* **api:**
  *  add support for PlanetScale (5c8d4d5b)
  *  add truck model (6d00719e)
  *  make organization name unique (350fb066)
  *  add query for getting organizations by name (4cb005ca)
  *  add organization relation to driver model (eea48efa)
  *  add driver model (0367a2f5)
  *  return first name and last name on getCurrentUser method (8c27650c)
* **web:**
  *  avoid fetching data for more than 60 days on FilterForm (c181c044)
  *  link Driver Filter Form to API (8f942542)
  *  link Organization Filter Form to API (efcc9ae5)
  *  link Truck Filter Form to the API (b4e641b5)
  *  add FilterForm to Organizations, Drivers and Trucks (6ec92699)
  *  add additional filters to FilterForm (fca3756e)
  *  add FilterForm to MainLayout and configure for UsersPage component (18fe2d7b)
  *  attach other params to Paginator (b8fa4d87)
  *  create FilterForm component (4ae7cbc2)
  *  create Paginator component (bb168c9a)
  *  add internationalization to SignupPage component (8aa40d10)
  *  add internationalization to ResetPasswordPage component (ed57c7ad)
  *  add internationalization to LoginPage component (7a50f63a)
  *  add internationalization module to ForgotPasswordPage (c8f77997)
  *  add internationalization to Driver module (3cd1c6f5)
  *  add internationalization to Header module (8ac2d1ed)
  *  add internationalization to Organization module (e027d128)
  *  add internationalization to Truck module (e631d851)
  *  add internationalization to SideBar component (131856b1)
  *  add internationalization to User module (0a841535)
  *  configure internationalization (43e53920)
  *  add RegistrationLayout to ResetPasswordPage (9d4c063f)
  *  add RegistrationLayout to ForgotPasswordPage (3e06ca1c)
  *  add RegistrationLayout to SignupPage (400790e1)
  *  add RegistrationLayout to LoginPage (b1cf2aaa)
  *  create RegistrationLayout component (1dcb4114)
  *  add font and configure typography (41f55869)
  *  add base styles (77fa145a)
  *  override tailwind colors (352cc0b8)
  *  install sass (c4bce380)
  *  add date formatter to Driver and Drivers components (bc8b37ac)
  *  add date formatter to Truck and Trucks components (4b817162)
  *  add date formatter to User and Users components (3fae7ef3)
  *  add date formatter to Organization and Organizations component (7c305410)
  *  create date formatter by using moment (e8eac094)
  *  add className prop to Header component (96dd31fe)
  *  remove layouts and replace them by using MainLayout (05bf4f30)
  *  create MainLayout component (7116a095)
  *  create Header component (0afc8804)
  *  create SideBar component (2d418de5)
  *  install and configure tailwindcss (0950b578)
* **web/api:**
  *  add two additional filters to users module (f7a34386)
  *  add pagination to Driver module (e9061c4c)
  *  add pagination to Truck module (9a82c337)
  *  add pagination to User module (eabbd4ec)
  *  add pagination to Organizations module (3b30ceaa)
  *  link truck to driver module (ffae76c3)
  *  create crud for trucks (e3c0dfa9)
  *  add crud for drivers (73dff544)
  *  add user crud (a8b3dd54)

##### Bug Fixes

* **web:**
  *  fix typo on Drivers component (68da31ad)
  *  attach new useForm instance to FilterForm (d7932098)
  *  change DateField by html input (93a8b325)
  *  transform FilterForm inputs into controlled inputs (3b6081f7)
  *  clean filter form after pressing same page on the side menu (e3157f1f)
  *  add default value to filter param for fetching users (8d898c05)
  *  fix label of Paginator component (22d130dd)
  *  disable sign up flow (1334384c)
  *  disable webAuthn in the frontend (53c9788d)
  *  internationalize missing message of LoginPage component (40f0da23)
  *  internationalize missing message of SignupPage component (4f0c8e2a)
  *  fix message of LoginPage component (69ef7f8b)
  *  internationalize missing message of ForgotPasswordPage component (458de779)
  *  pass email to i18n on ForgotPasswordPage (8d4ab44c)
  *  internationalize missing texts on OrganizationForm component (134c72d3)
  *  add contidion chaining for avoiding error getting the current user (36de548d)
  *  add conditional chaining to edit driver method (27a75e2a)
* **api:**  translate prisma error (1e49800a)
* **web/api:**
  *  fix user flow creation (1b8c2db6)
  *  change email by username in order to reuse signUp method for creating an user (65ef7604)

##### Refactors

* **web:**
  *  change name of SideBar component (896b1177)
  *  remove irrelevant columns of Drivers component (1355d83f)
  *  remove irrelevant columns of Trucks component (74e535aa)
  *  remove irrelevant columns of Organizations component (1634687a)
  *  remove irrelevant user columns of User component (32dbfcd6)

##### Code Style Changes

* **web:**
  *  add animation to button for showing Menu component (5738feeb)
  *  make Menu component responsive (97bd91ab)
  *  make responsive the FilterForm component (debc036c)
  *  add transparent border to rw-button-blue (cded99bc)
  *  hide SideBar when screen size is lower than lg (b3fc8213)
  *  add col-start-1 for screen size lower than large (0929afc8)
  *  hide columns when screen size is lower than lg (e7497f90)
  *  update styles of SideBar component (ab29a35d)
  *  update Paginator styles (cc1b1234)
  *  override styles of rw-button-green (822b22e1)
  *  update style Paginator (d7736a8d)
  *  reduce padding to Header component (c102b0fe)
  *  update style of redwood form error title (590fb2f0)
  *  change font properties for div tags (a5f968d6)
  *  change font properties for button tags (5ac8bae1)
  *  update style of FatalErrorPage component (2dd72c16)
  *  update style of NotFoundPage component (7dc349b0)
  *  add rw-scaffold to MainLayou component (5cec6a4e)
  *  overwrite Redwood styles (947edfab)
  *  update styles of SideBar component (51d649e6)
  *  change background color of MainLayout component (41ae1a20)
  *  remove class from edit button of tables (0ae0b302)
  *  update typography styles (53592f4f)
  *  update style of MainLayout component (8ca1da31)
  *  add full heigth to main containers (9795becb)

##### Tests

* **api:**
  *  update tests for fetching drivers, users and trucks (cfa720b3)
  *  update test for getting organizations (4bd2a9cb)
* **web:**  fix test for creating user (e3940e1e)

#### 0.2.4 (2022-10-05)

##### Continuous Integration

* **web/api:**  add DATABASE_URL variable to ci/cd file (019ae759)

#### 0.2.3 (2022-10-05)

##### Continuous Integration

* **web/api:**  change localhost by mysql on ci/cd file (e7cd0a70)

#### 0.2.2 (2022-10-05)

##### Continuous Integration

* **web/api:**  remove connect option from ci/cd (a2e72c11)

#### 0.2.1 (2022-10-05)

##### Continuous Integration

* **web/api:**  add mysql to our ci/cd in order to run api test (3b96e239)

##### Other Changes

* **api:**  change database to mysql (88d1d85d)

### 0.2.0 (2022-10-05)

##### New Features

* **web:**  add name to SignupPage (cf1a2f21)
* **web/api:**  add crud for organization (a5a1f9e8)
* **api:**
  *  change sqlite by postgres (c712a8dc)
  *  add organization model and migration (4998d3a5)

##### Bug Fixes

* **web:**  redirect to organization view after login (4fa3e953)
* **web/api:**  make optional enabled and dates properties (5f57e7ed)
* **api:**  disable webAuthn (2b89ca19)

#### 0.1.24 (2022-10-01)

##### Chores

* **web/api:**  add deploy task to cacheable tasks (279690d4)

#### 0.1.23 (2022-10-01)

##### Continuous Integration

* **web/api:**  download vercel config on before_script (4f7accf0)

#### 0.1.22 (2022-10-01)

##### Continuous Integration

* **web/api:**  decrease machines for running the build and deploy tasks (745837d4)

#### 0.1.21 (2022-10-01)

##### Continuous Integration

* **web/api:**  use nx command for building and deploy in place of vercel commands (ddc292b5)

#### 0.1.20 (2022-10-01)

##### Continuous Integration

* **web/api:**  move vercel command to pipeline file (3fa28787)

#### 0.1.19 (2022-10-01)

##### Continuous Integration

* **web/api:**  surround VERCEL_TOKEN into curly braces (28140afd)

#### 0.1.18 (2022-10-01)

##### Continuous Integration

* **web/api:**  enable debug mode on ci/cd (44c2f16c)

#### 0.1.17 (2022-09-30)

##### Chores

* **web/api:**  add token parameter to build and deploy command (990603a8)

#### 0.1.16 (2022-09-30)

##### Chores

* **web/api:**
  *  add installation of kusky to postinstall hook (4c0b5497)
  *  add git add to lint-staged (d189480b)

##### Tests

* **api:**  comment requireAuth test (31dc184e)

#### 0.1.15 (2022-09-30)

##### Code Style Changes

* **web/api:**  remove linting errors (d414a49f)

#### 0.1.14 (2022-09-30)

##### Continuous Integration

* **web/api:**
  *  disable debug mode (cfe9b43d)
  *  remove cache block (eb3146bc)

#### 0.1.13 (2022-09-30)

##### Continuous Integration

* **web/api:**  update key and path of the cache (7d531afc)

#### 0.1.12 (2022-09-30)

##### Continuous Integration

* **web/api:**  add key to cache in order to share the cache across jobs (a3d96ed9)

#### 0.1.11 (2022-09-29)

##### Continuous Integration

* **web/api:**  update base commit value (30df150c)

#### 0.1.10 (2022-09-29)

##### Continuous Integration

* **web/api:**  change base value of affected command (cba7c7e6)

#### 0.1.9 (2022-09-29)

##### Continuous Integration

* **web/api:**  remove base from affected command and restore head (653136f2)

#### 0.1.8 (2022-09-29)

##### Continuous Integration

* **web/api:**  remove head param fron affected command (5961781c)

#### 0.1.7 (2022-09-29)

##### Continuous Integration

* **web/api:**  add variables in order to see some commit shas (b891609b)

#### 0.1.6 (2022-09-29)

##### Continuous Integration

* **web/api:**  remove quotes from base commit sha (14b9dec0)

#### 0.1.5 (2022-09-29)

##### Continuous Integration

* **web/api:**  add commit sha to a variable (e573c087)

#### 0.1.4 (2022-09-29)

##### Continuous Integration

* **web/api:**  update NX_BASE env var of the ci/cd file (b2677ab4)

#### 0.1.3 (2022-09-29)

##### Continuous Integration

* **web/api:**  add complete commit hash to base param (b42166f4)

#### 0.1.2 (2022-09-29)

##### Continuous Integration

* **web/api:**  hardcode base commit in order to deploy to Vercel (e28379cb)

#### 0.1.1 (2022-09-29)

##### Continuous Integration

* **web/api:**  enable debug for gitlab pipeline (8a11c86e)

#### 0.1.0 2022-09-29

##### Chores

* **web/api:**
  *  add changelog generator (650d6b99)
  *  update yarn.lock (f03673c5)
  *  add lint and test to cacheable commands (1508929b)
  *  add test command to project.json file (fece91e3)
*  add lint commands to project.json file (26f836c0)
*  add build to cacheable scritps (53b5f864)
*  update nx and nxcloud versions on yarn.lock (5b57d7d3)
*  add webpack-stats and web-performance commands to project.json (042ad05f)
*  add project.json file in order to configure project for Nx (db778526)
*  add distribute cache (0cf5cbd9)
*  install and initialize nx (465ae5f4)
*  install dependencies (97e754ab)
*  ping Nodejs version by using Volta (72dff733)

##### Continuous Integration

* **web/api:**
  *  change trigger for ci/cd (ca820f0b)
  *  separate build and deploy into two commands (8f90f67d)
  *  create a deploy command on project.json file (bfcea196)
  *  initialize dte agents (93a9d473)
  *  gather all tasks in one job (a9a22675)
  *  remove teardown stage (6a2a6b80)
  *  add more stages and jobs to gitlab ci/cd (edc295e0)
  *  change yarn.lock in place of package-lock.json file (ed457ea3)
  *  use yarn in place of npm (3231da3a)
  *  add gitlab ci/cd pipeline (275491cd)

##### Documentation Changes

* **web/api:**
  *  add how to deploy information to Readme.md file (ad29d57a)
  *  add how to install and how to commit to Readme.md file (083840c6)

##### New Features

* **web/api:**
  *  initialize authentication module (e88da372)
  *  add commitizen and customize it (3617a515)
*  add husky and lint-staged (b1c63f7a)

