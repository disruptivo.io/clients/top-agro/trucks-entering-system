FROM node:16.17.0-alpine3.16
WORKDIR /app
COPY . .

ADD https://github.com/ufoscout/docker-compose-wait/releases/download/2.5.0/wait /wait
RUN chmod +x /wait

EXPOSE 8910 8911
CMD /wait && yarn install && yarn rw prisma migrate dev && yarn rw prisma db seed && yarn rw dev