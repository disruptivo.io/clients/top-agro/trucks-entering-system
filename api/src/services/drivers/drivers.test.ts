import type { Driver, Organization, Picture } from '@prisma/client'

import { organizations } from '../organizations/organizations'
import { pictures } from '../pictures/pictures'

import {
  drivers,
  driver,
  createDriver,
  updateDriver,
  deleteDriver,
} from './drivers'
import type { StandardScenario } from './drivers.scenarios'

// Generated boilerplate tests do not account for all circumstances
// and can fail without adjustments, e.g. Float and DateTime types.
//           Please refer to the RedwoodJS Testing Docs:
//       https://redwoodjs.com/docs/testing#testing-services
// https://redwoodjs.com/docs/testing#jest-expect-type-considerations

describe('drivers', () => {
  scenario('returns all drivers', async (scenario: StandardScenario) => {
    const result = await drivers({ page: 1, size: 2 })

    expect(result.data.length).toEqual(Object.keys(scenario.driver).length)
  })

  scenario('returns a single driver', async (scenario: StandardScenario) => {
    const result = await driver({ id: scenario.driver.one.id })

    expect(result).toEqual(scenario.driver.one)
  })

  scenario('creates a driver', async () => {
    const organization = (
      await organizations({ page: 1, size: 2 })
    ).data.pop() as Organization
    const result = await createDriver({
      input: {
        firstName: 'String9864152',
        lastName: 'String2692266',
        identification: 'String482301',
        organizationId: organization.id,
        pictures: [
          {
            url: 'String',
          },
        ],
      },
    })

    expect(result.firstName).toEqual('String9864152')
    expect(result.lastName).toEqual('String2692266')
    expect(result.identification).toEqual('String482301')
  })

  scenario('updates a driver', async (scenario: StandardScenario) => {
    const original = (await driver({ id: scenario.driver.one.id })) as Driver
    const originalPictures = (await pictures()) as Picture[]
    const picturesUpdated = [
      {
        id: originalPictures[0].id,
        url: 'Updated image',
      },
    ]
    const result = await updateDriver({
      id: original.id,
      input: {
        firstName: 'String1187132',
        pictures: picturesUpdated,
      },
    })

    expect(result.firstName).toEqual('String1187132')
  })

  scenario('deletes a driver', async (scenario: StandardScenario) => {
    const original = (await deleteDriver({
      id: scenario.driver.one.id,
    })) as Driver
    const result = await driver({ id: original.id })

    expect(result).toEqual(null)
  })
})
