import type {
  QueryResolvers,
  MutationResolvers,
  DriverRelationResolvers,
} from 'types/graphql'

import { RedwoodError } from '@redwoodjs/api'

import { db } from 'src/lib/db'

const filterToWhereCondition = {
  startDate: (date) => ({
    createdAt: {
      gte: new Date(date),
    },
  }),
  endDate: (date) => ({
    createdAt: {
      lte: new Date(date),
    },
  }),
  filter: (filter) => ({
    OR: [
      {
        firstName: {
          contains: filter,
        },
      },
      {
        lastName: {
          contains: filter,
        },
      },
    ],
  }),
}

export const drivers: QueryResolvers['drivers'] = async ({
  page,
  size,
  enabled,
  ...filters
}) => {
  const offset = (page - 1) * size
  const nonNullFilterKeys = Object.keys(filters).filter((key) => filters[key])
  const AND = nonNullFilterKeys.map((key) =>
    filterToWhereCondition[key](filters[key])
  )
  const where = {
    AND: [
      ...AND,
      {
        enabled: {
          equals: enabled,
        },
      },
    ],
  }

  return {
    data: await db.driver.findMany({
      take: size,
      skip: offset,
      orderBy: { createdAt: 'desc' },
      where,
    }),
    count: await db.driver.count({
      where,
    }),
    isEmpty: (await db.driver.findFirst()) ? false : true,
  }
}

export const driversByTruck: QueryResolvers['driversByTruck'] = ({ plate }) => {
  return db.driver.findMany({
    where: {
      truck: {
        plate: {
          equals: plate,
        },
      },
    },
  })
}

export const driver: QueryResolvers['driver'] = ({ id }) => {
  return db.driver.findUnique({
    where: { id },
  })
}

export const createDriver: MutationResolvers['createDriver'] = async ({
  input,
}) => {
  try {
    if (!input.organizationId)
      throw new RedwoodError('Debes seleccionar una organización de la lista')

    const { pictures, ...rest } = input

    return await db.driver.create({
      data: {
        ...rest,
        pictures: {
          createMany: {
            data: pictures,
          },
        },
      },
    })
  } catch (error) {
    if (error.code === 'P2002')
      throw new RedwoodError('Ya existe conductor con esa identificación')

    throw error
  }
}

export const updateDriver: MutationResolvers['updateDriver'] = async ({
  id,
  input,
}) => {
  try {
    const { pictures, ...rest } = input

    for (const picture of pictures)
      await db.picture.update({
        where: { id: picture.id },
        data: {
          ...picture,
        },
      })

    return await db.driver.update({
      data: {
        ...rest,
      },
      where: { id },
    })
  } catch (error) {
    if (error.code === 'P2002')
      throw new RedwoodError('Ya existe conductor con esa identificación')

    throw error
  }
}

export const deleteDriver: MutationResolvers['deleteDriver'] = ({ id }) => {
  return db.driver.delete({
    where: { id },
  })
}

export const Driver: DriverRelationResolvers = {
  createdBy: (_obj, { root }) => {
    return db.driver.findUnique({ where: { id: root?.id } }).createdBy()
  },
  updatedBy: (_obj, { root }) => {
    return db.driver.findUnique({ where: { id: root?.id } }).updatedBy()
  },
  organization: (_obj, { root }) => {
    return db.driver.findUnique({ where: { id: root?.id } }).organization()
  },
  truck: (_obj, { root }) => {
    return db.driver.findUnique({ where: { id: root?.id } }).truck()
  },
  pictures: async (_obj, { root }) => {
    return db.driver.findUnique({ where: { id: root?.id } }).pictures()
  },
}
