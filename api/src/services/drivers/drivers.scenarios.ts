import type { Prisma, Driver } from '@prisma/client'

import type { ScenarioData } from '@redwoodjs/testing/api'

export const standard = defineScenario<Prisma.DriverCreateArgs>({
  driver: {
    one: {
      data: {
        firstName: 'String595151',
        lastName: 'String4998182',
        identification: 'String3015287',
        pictures: {
          create: {
            url: 'String123',
          },
        },
        organization: {
          create: {
            name: 'String43243',
          },
        },
      },
    },
    two: {
      data: {
        firstName: 'String3463761',
        lastName: 'String7701478',
        identification: 'String7138713',
        pictures: {
          create: {
            url: 'String12344',
          },
        },
        organization: {
          create: {
            name: 'String43',
          },
        },
      },
    },
  },
})

export type StandardScenario = ScenarioData<Driver, 'driver'>
