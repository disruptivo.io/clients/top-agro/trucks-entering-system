import type { Picture } from '@prisma/client'

import {
  pictures,
  picture,
  createPicture,
  updatePicture,
  deletePicture,
} from './pictures'
import type { StandardScenario } from './pictures.scenarios'

// Generated boilerplate tests do not account for all circumstances
// and can fail without adjustments, e.g. Float and DateTime types.
//           Please refer to the RedwoodJS Testing Docs:
//       https://redwoodjs.com/docs/testing#testing-services
// https://redwoodjs.com/docs/testing#jest-expect-type-considerations

describe('pictures', () => {
  scenario('returns all pictures', async (scenario: StandardScenario) => {
    const result = await pictures()

    expect(result.length).toEqual(Object.keys(scenario.picture).length)
  })

  scenario('returns a single picture', async (scenario: StandardScenario) => {
    const result = await picture({ id: scenario.picture.one.id })

    expect(result).toEqual(scenario.picture.one)
  })

  scenario('creates a picture', async () => {
    const result = await createPicture({
      input: { url: 'String' },
    })

    expect(result.url).toEqual('String')
  })

  scenario('updates a picture', async (scenario: StandardScenario) => {
    const original = (await picture({ id: scenario.picture.one.id })) as Picture
    const result = await updatePicture({
      id: original.id,
      input: { url: 'String2' },
    })

    expect(result.url).toEqual('String2')
  })

  scenario('deletes a picture', async (scenario: StandardScenario) => {
    const original = (await deletePicture({
      id: scenario.picture.one.id,
    })) as Picture
    const result = await picture({ id: original.id })

    expect(result).toEqual(null)
  })
})
