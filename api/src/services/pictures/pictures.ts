import type {
  QueryResolvers,
  MutationResolvers,
  PictureRelationResolvers,
} from 'types/graphql'

import { db } from 'src/lib/db'

export const pictures: QueryResolvers['pictures'] = () => {
  return db.picture.findMany()
}

export const picture: QueryResolvers['picture'] = ({ id }) => {
  return db.picture.findUnique({
    where: { id },
  })
}

export const createPicture: MutationResolvers['createPicture'] = ({
  input,
}) => {
  return db.picture.create({
    data: input,
  })
}

export const updatePicture: MutationResolvers['updatePicture'] = ({
  id,
  input,
}) => {
  return db.picture.update({
    data: input,
    where: { id },
  })
}

export const deletePicture: MutationResolvers['deletePicture'] = ({ id }) => {
  return db.picture.delete({
    where: { id },
  })
}

export const Picture: PictureRelationResolvers = {
  createdBy: (_obj, { root }) => {
    return db.picture.findUnique({ where: { id: root?.id } }).createdBy()
  },
  updatedBy: (_obj, { root }) => {
    return db.picture.findUnique({ where: { id: root?.id } }).updatedBy()
  },
  Driver: (_obj, { root }) => {
    return db.picture.findUnique({ where: { id: root?.id } }).Driver()
  },
}
