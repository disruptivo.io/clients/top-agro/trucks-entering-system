import type { Prisma, Picture } from '@prisma/client'

import type { ScenarioData } from '@redwoodjs/testing/api'

export const standard = defineScenario<Prisma.PictureCreateArgs>({
  picture: {
    one: { data: { url: 'String' } },
    two: { data: { url: 'String' } },
  },
})

export type StandardScenario = ScenarioData<Picture, 'picture'>
