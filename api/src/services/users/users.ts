import type {
  QueryResolvers,
  MutationResolvers,
  UserRelationResolvers,
} from 'types/graphql'

import { hashPassword, RedwoodError } from '@redwoodjs/api'

import { db } from 'src/lib/db'

const filterToWhereCondition = {
  startDate: (date) => ({
    createdAt: {
      gte: new Date(date),
    },
  }),
  endDate: (date) => ({
    createdAt: {
      lte: new Date(date),
    },
  }),
  filter: (filter) => ({
    OR: [
      {
        firstName: {
          contains: filter,
        },
      },
      {
        lastName: {
          contains: filter,
        },
      },
      {
        username: {
          contains: filter,
        },
      },
    ],
  }),
}

export const users: QueryResolvers['users'] = async ({
  page,
  size,
  enabled,
  ...filters
}) => {
  const offset = (page - 1) * size
  const nonNullFilterKeys = Object.keys(filters).filter((key) => filters[key])
  const AND = nonNullFilterKeys.map((key) =>
    filterToWhereCondition[key](filters[key])
  )
  const where = {
    AND: [
      ...AND,
      {
        enabled: {
          equals: enabled,
        },
      },
    ],
  }

  return {
    data: await db.user.findMany({
      take: size,
      skip: offset,
      orderBy: { createdAt: 'desc' },
      where,
    }),
    count: await db.user.count({
      where,
    }),
    isEmpty: (await db.user.findFirst()) ? false : true,
  }
}

export const user: QueryResolvers['user'] = ({ id }) => {
  return db.user.findUnique({
    where: { id },
  })
}

export const createUser: MutationResolvers['createUser'] = async ({
  input,
}) => {
  try {
    const [hashedPassword, salt] = hashPassword(input.hashedPassword)
    return await db.user.create({
      data: {
        ...input,
        hashedPassword,
        salt,
        createdById: input.createdById,
      },
    })
  } catch (error) {
    if (error.code === 'P2002')
      throw new RedwoodError('Ya existe usuario con ese correo')
  }
}

export const updateUser: MutationResolvers['updateUser'] = async ({
  id,
  input,
}) => {
  try {
    return await db.user.update({
      data: input,
      where: { id },
    })
  } catch (error) {
    if (error.code === 'P2002')
      throw new RedwoodError('Ya existe usuario con ese correo')
  }
}

export const deleteUser: MutationResolvers['deleteUser'] = ({ id }) => {
  return db.user.delete({
    where: { id },
  })
}

export const User: UserRelationResolvers = {
  createdBy: (_obj, { root }) => {
    return db.user.findUnique({ where: { id: root?.id } }).createdBy()
  },
  updatedBy: (_obj, { root }) => {
    return db.user.findUnique({ where: { id: root?.id } }).updatedBy()
  },
  userCreatedBy: (_obj, { root }) => {
    return db.user.findUnique({ where: { id: root?.id } }).userCreatedBy()
  },
  userUpdatedBy: (_obj, { root }) => {
    return db.user.findUnique({ where: { id: root?.id } }).userUpdatedBy()
  },
  organizationCreatedBy: (_obj, { root }) => {
    return db.user
      .findUnique({ where: { id: root?.id } })
      .organizationCreatedBy()
  },
  organizationUpdatedBy: (_obj, { root }) => {
    return db.user
      .findUnique({ where: { id: root?.id } })
      .organizationUpdatedBy()
  },
}
