import type { Prisma, User } from '@prisma/client'

import type { ScenarioData } from '@redwoodjs/testing/api'

export const standard = defineScenario<Prisma.UserCreateArgs>({
  user: {
    one: {
      data: {
        firstName: 'String',
        lastName: 'String',
        username: 'String2696936',
        hashedPassword: 'String',
        salt: 'String',
      },
    },
    two: {
      data: {
        firstName: 'String',
        lastName: 'String',
        username: 'String3475874',
        hashedPassword: 'String',
        salt: 'String',
      },
    },
  },
})

export type StandardScenario = ScenarioData<User, 'user'>
