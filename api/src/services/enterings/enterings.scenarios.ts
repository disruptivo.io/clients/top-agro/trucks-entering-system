import type { Prisma, Entering } from '@prisma/client'

import type { ScenarioData } from '@redwoodjs/testing/api'

export const standard = defineScenario<Prisma.EnteringCreateArgs>({
  entering: {
    one: {
      data: {
        driver: {
          create: {
            firstName: 'String',
            lastName: 'String',
            identification: 'String8452753',
            pictures: {
              create: {
                url: 'String12321',
              },
            },
            organization: {
              create: {
                name: 'String33244',
              },
            },
          },
        },
        truck: {
          create: {
            plate: 'String4711596',
            color: 'String',
            brand: 'String',
            model: 'String',
          },
        },
      },
    },
    two: {
      data: {
        driver: {
          create: {
            firstName: 'String',
            lastName: 'String',
            identification: 'String4023406',
            pictures: {
              create: {
                url: 'String2234',
              },
            },
            organization: {
              create: {
                name: 'String',
              },
            },
          },
        },
        truck: {
          create: {
            plate: 'String97150',
            color: 'String',
            brand: 'String',
            model: 'String',
          },
        },
      },
    },
  },
})

export type StandardScenario = ScenarioData<Entering, 'entering'>
