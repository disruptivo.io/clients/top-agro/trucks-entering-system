import type { Entering } from '@prisma/client'

import {
  enterings,
  entering,
  createEntering,
  updateEntering,
  deleteEntering,
} from './enterings'
import type { StandardScenario } from './enterings.scenarios'

// Generated boilerplate tests do not account for all circumstances
// and can fail without adjustments, e.g. Float and DateTime types.
//           Please refer to the RedwoodJS Testing Docs:
//       https://redwoodjs.com/docs/testing#testing-services
// https://redwoodjs.com/docs/testing#jest-expect-type-considerations

describe('enterings', () => {
  scenario('returns all enterings', async (scenario: StandardScenario) => {
    const result = await enterings({ page: 1, size: 2 })

    expect(result.data.length).toEqual(Object.keys(scenario.entering).length)
  })

  scenario('returns a single entering', async (scenario: StandardScenario) => {
    const result = await entering({ id: scenario.entering.one.id })

    expect(result).toEqual(scenario.entering.one)
  })

  scenario('creates a entering', async (scenario: StandardScenario) => {
    const result = await createEntering({
      input: {
        driverId: scenario.entering.two.driverId,
        truckId: scenario.entering.two.truckId,
      },
    })

    expect(result.driverId).toEqual(scenario.entering.two.driverId)
    expect(result.truckId).toEqual(scenario.entering.two.truckId)
  })

  scenario('updates a entering', async (scenario: StandardScenario) => {
    const original = (await entering({
      id: scenario.entering.one.id,
    })) as Entering
    const result = await updateEntering({
      id: original.id,
      input: { driverId: scenario.entering.two.driverId },
    })

    expect(result.driverId).toEqual(scenario.entering.two.driverId)
  })

  scenario('deletes a entering', async (scenario: StandardScenario) => {
    const original = (await deleteEntering({
      id: scenario.entering.one.id,
    })) as Entering
    const result = await entering({ id: original.id })

    expect(result).toEqual(null)
  })
})
