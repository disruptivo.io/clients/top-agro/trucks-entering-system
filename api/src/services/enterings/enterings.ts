import type {
  QueryResolvers,
  MutationResolvers,
  EnteringRelationResolvers,
} from 'types/graphql'

import { db } from 'src/lib/db'

const filterToWhereCondition = {
  startDate: (date) => ({
    createdAt: {
      gte: new Date(date),
    },
  }),
  endDate: (date) => ({
    createdAt: {
      lte: new Date(date),
    },
  }),
  filter: (filter) => ({
    OR: [
      {
        driver: {
          firstName: {
            contains: filter,
          },
        },
      },
      {
        driver: {
          lastName: {
            contains: filter,
          },
        },
      },
      {
        truck: {
          plate: {
            contains: filter,
          },
        },
      },
    ],
  }),
}

export const enterings: QueryResolvers['enterings'] = async ({
  page,
  size,
  ...filters
}) => {
  const offset = (page - 1) * size
  const nonNullFilterKeys = Object.keys(filters).filter((key) => filters[key])
  const AND = nonNullFilterKeys.map((key) =>
    filterToWhereCondition[key](filters[key])
  )
  const where = {
    AND,
  }

  return {
    data: await db.entering.findMany({
      take: size,
      skip: offset,
      orderBy: { enteringAt: 'desc' },
      where,
    }),
    count: await db.entering.count({
      where,
    }),
    isEmpty: (await db.entering.findFirst()) ? false : true,
  }
}
export const entering: QueryResolvers['entering'] = ({ id }) => {
  return db.entering.findUnique({
    where: { id },
  })
}

export const createEntering: MutationResolvers['createEntering'] = ({
  input,
}) => {
  return db.entering.create({
    data: input,
  })
}

export const updateEntering: MutationResolvers['updateEntering'] = ({
  id,
  input,
}) => {
  return db.entering.update({
    data: input,
    where: { id },
  })
}

export const deleteEntering: MutationResolvers['deleteEntering'] = ({ id }) => {
  return db.entering.delete({
    where: { id },
  })
}

export const Entering: EnteringRelationResolvers = {
  driver: (_obj, { root }) => {
    return db.entering.findUnique({ where: { id: root?.id } }).driver()
  },
  truck: (_obj, { root }) => {
    return db.entering.findUnique({ where: { id: root?.id } }).truck()
  },
  createdBy: (_obj, { root }) => {
    return db.entering.findUnique({ where: { id: root?.id } }).createdBy()
  },
  updatedBy: (_obj, { root }) => {
    return db.entering.findUnique({ where: { id: root?.id } }).updatedBy()
  },
}
