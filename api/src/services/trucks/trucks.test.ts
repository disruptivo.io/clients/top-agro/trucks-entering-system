import type { Truck, Picture } from '@prisma/client'

import { pictures } from '../pictures/pictures'

import { trucks, truck, createTruck, updateTruck, deleteTruck } from './trucks'
import type { StandardScenario } from './trucks.scenarios'

// Generated boilerplate tests do not account for all circumstances
// and can fail without adjustments, e.g. Float and DateTime types.
//           Please refer to the RedwoodJS Testing Docs:
//       https://redwoodjs.com/docs/testing#testing-services
// https://redwoodjs.com/docs/testing#jest-expect-type-considerations

describe('trucks', () => {
  scenario('returns all trucks', async (scenario: StandardScenario) => {
    const result = await trucks({ page: 1, size: 2 })

    expect(result.data.length).toEqual(Object.keys(scenario.truck).length)
  })

  scenario('returns a single truck', async (scenario: StandardScenario) => {
    const result = await truck({ id: scenario.truck.one.id })

    expect(result).toEqual(scenario.truck.one)
  })

  scenario('creates a truck', async () => {
    const result = await createTruck({
      input: {
        plate: 'String4548249',
        color: 'String',
        brand: 'String',
        model: 'String',
        pictures: [
          {
            url: 'String',
          },
        ],
      },
    })

    expect(result.plate).toEqual('String4548249')
    expect(result.color).toEqual('String')
    expect(result.brand).toEqual('String')
    expect(result.model).toEqual('String')
  })

  scenario('updates a truck', async (scenario: StandardScenario) => {
    const original = (await truck({ id: scenario.truck.one.id })) as Truck
    const originalPictures = (await pictures()) as Picture[]
    const picturesUpdated = [
      {
        id: originalPictures[0].id,
        url: 'Updated image',
      },
    ]

    const result = await updateTruck({
      id: original.id,
      input: {
        pictures: picturesUpdated,
        plate: 'String62823072',
      },
    })

    expect(result.plate).toEqual('String62823072')
  })

  scenario('deletes a truck', async (scenario: StandardScenario) => {
    const original = (await deleteTruck({ id: scenario.truck.one.id })) as Truck
    const result = await truck({ id: original.id })

    expect(result).toEqual(null)
  })
})
