import type { Prisma, Truck } from '@prisma/client'

import type { ScenarioData } from '@redwoodjs/testing/api'

export const standard = defineScenario<Prisma.TruckCreateArgs>({
  truck: {
    one: {
      data: {
        plate: 'String5237887',
        color: 'String',
        brand: 'String',
        model: 'String',
        pictures: {
          create: {
            id: 1,
            url: 'String35',
          },
        },
      },
    },
    two: {
      data: {
        plate: 'String4596173',
        color: 'String',
        brand: 'String',
        model: 'String',
        pictures: {
          create: {
            id: 2,
            url: 'String35434',
          },
        },
      },
    },
  },
})

export type StandardScenario = ScenarioData<Truck, 'truck'>
