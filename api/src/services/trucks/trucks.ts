import type {
  QueryResolvers,
  MutationResolvers,
  TruckRelationResolvers,
} from 'types/graphql'

import { RedwoodError } from '@redwoodjs/api'

import { db } from 'src/lib/db'

const filterToWhereCondition = {
  startDate: (date) => ({
    createdAt: {
      gte: new Date(date),
    },
  }),
  endDate: (date) => ({
    createdAt: {
      lte: new Date(date),
    },
  }),
  filter: (filter) => ({
    OR: [
      {
        plate: {
          contains: filter,
        },
      },
      {
        brand: {
          contains: filter,
        },
      },
      {
        color: {
          contains: filter,
        },
      },
      {
        model: {
          contains: filter,
        },
      },
    ],
  }),
}

export const trucks: QueryResolvers['trucks'] = async ({
  page,
  size,
  enabled,
  ...filters
}) => {
  const offset = (page - 1) * size
  const nonNullFilterKeys = Object.keys(filters).filter((key) => filters[key])
  const AND = nonNullFilterKeys.map((key) =>
    filterToWhereCondition[key](filters[key])
  )
  const where = {
    AND: [
      ...AND,
      {
        enabled: {
          equals: enabled,
        },
      },
    ],
  }

  return {
    data: await db.truck.findMany({
      take: size,
      skip: offset,
      orderBy: { createdAt: 'desc' },
      where,
    }),
    count: await db.truck.count({
      where,
    }),
    isEmpty: (await db.truck.findFirst()) ? false : true,
  }
}

export const trucksByPlate: QueryResolvers['trucks'] = ({ plate }) => {
  return db.truck.findMany({
    where: {
      plate: {
        contains: plate,
      },
    },
  })
}

export const truck: QueryResolvers['truck'] = ({ id }) => {
  return db.truck.findUnique({
    where: { id },
  })
}

export const createTruck: MutationResolvers['createTruck'] = async ({
  input,
}) => {
  try {
    const { pictures, ...rest } = input

    return await db.truck.create({
      data: {
        ...rest,
        pictures: {
          createMany: {
            data: pictures,
          },
        },
      },
    })
  } catch (error) {
    if (error.code === 'P2002')
      throw new RedwoodError('Ya existe camión con esa placa')
  }
}

export const updateTruck: MutationResolvers['updateTruck'] = async ({
  id,
  input,
}) => {
  try {
    const { pictures, ...rest } = input

    for (const picture of pictures)
      await db.picture.update({
        where: { id: picture.id },
        data: {
          ...picture,
        },
      })

    return await db.truck.update({
      data: rest,
      where: { id },
    })
  } catch (error) {
    if (error.code === 'P2002')
      throw new RedwoodError('Ya existe camión con esa placa')

    throw error
  }
}

export const deleteTruck: MutationResolvers['deleteTruck'] = ({ id }) => {
  return db.truck.delete({
    where: { id },
  })
}

export const Truck: TruckRelationResolvers = {
  createdBy: (_obj, { root }) => {
    return db.truck.findUnique({ where: { id: root?.id } }).createdBy()
  },
  updatedBy: (_obj, { root }) => {
    return db.truck.findUnique({ where: { id: root?.id } }).updatedBy()
  },
  driver: (_obj, { root }) => {
    return db.truck.findUnique({ where: { id: root?.id } }).driver()
  },
  pictures: async (_obj, { root }) => {
    return db.truck.findUnique({ where: { id: root?.id } }).pictures()
  },
}
