import type { Prisma, Organization } from '@prisma/client'

import type { ScenarioData } from '@redwoodjs/testing/api'

export const standard = defineScenario<Prisma.OrganizationCreateArgs>({
  organization: {
    one: { data: { name: 'Org1', updatedAt: '2022-10-04T01:50:51Z' } },
    two: { data: { name: 'Org2', updatedAt: '2022-10-04T01:50:51Z' } },
  },
})

export type StandardScenario = ScenarioData<Organization, 'organization'>
