import type {
  QueryResolvers,
  MutationResolvers,
  OrganizationRelationResolvers,
} from 'types/graphql'

import { RedwoodError } from '@redwoodjs/api'

import { db } from 'src/lib/db'

const filterToWhereCondition = {
  startDate: (date) => ({
    createdAt: {
      gte: new Date(date),
    },
  }),
  endDate: (date) => ({
    createdAt: {
      lte: new Date(date),
    },
  }),
  filter: (filter) => ({
    OR: [
      {
        name: {
          contains: filter,
        },
      },
    ],
  }),
}

export const organizations: QueryResolvers['organizations'] = async ({
  page,
  size,
  enabled,
  ...filters
}) => {
  const offset = (page - 1) * size
  const nonNullFilterKeys = Object.keys(filters).filter((key) => filters[key])
  const AND = nonNullFilterKeys.map((key) =>
    filterToWhereCondition[key](filters[key])
  )
  const where = {
    AND: [
      ...AND,
      {
        enabled: {
          equals: enabled,
        },
      },
    ],
  }

  return {
    data: await db.organization.findMany({
      take: size,
      skip: offset,
      orderBy: { createdAt: 'desc' },
      where,
    }),
    count: await db.organization.count({
      where,
    }),
    isEmpty: (await db.organization.findFirst()) ? false : true,
  }
}

export const organizationsByName: QueryResolvers['organizations'] = ({
  name,
}) => {
  return db.organization.findMany({
    where: {
      name: {
        contains: name,
      },
    },
  })
}

export const organization: QueryResolvers['organization'] = ({ id }) => {
  return db.organization.findUnique({
    where: { id },
  })
}

export const createOrganization: MutationResolvers['createOrganization'] =
  async ({ input }) => {
    try {
      return await db.organization.create({
        data: input,
      })
    } catch (error) {
      if (error.code === 'P2002')
        throw new RedwoodError('Ya existe empresa con ese nombre')

      throw error
    }
  }

export const updateOrganization: MutationResolvers['updateOrganization'] =
  async ({ id, input }) => {
    try {
      return await db.organization.update({
        data: input,
        where: { id },
      })
    } catch (error) {
      if (error.code === 'P2002')
        throw new RedwoodError('Ya existe empresa con ese nombre')
    }
  }

export const Organization: OrganizationRelationResolvers = {
  createdBy: (_obj, { root }) => {
    return db.organization.findUnique({ where: { id: root?.id } }).createdBy()
  },
  updatedBy: (_obj, { root }) => {
    return db.organization.findUnique({ where: { id: root?.id } }).updatedBy()
  },
}
