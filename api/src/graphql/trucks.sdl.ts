export const schema = gql`
  type Truck {
    id: Int!
    pictures: [Picture]
    plate: String!
    color: String!
    brand: String!
    model: String!
    enabled: Boolean
    createdAt: DateTime
    updatedAt: DateTime
    createdBy: User
    createdById: Int
    updatedBy: User
    updatedById: Int
    driver: [Driver]!
  }

  type Trucks {
    data: [Truck!]!
    count: Int!
    isEmpty: Boolean
  }

  type Query {
    trucks(
      page: Int!
      size: Int!
      filter: String
      startDate: String
      endDate: String
      enabled: Boolean
    ): Trucks! @requireAuth
    truck(id: Int!): Truck @requireAuth
    trucksByPlate(plate: String!): [Truck!]! @requireAuth
  }

  input CreateTruckInput {
    pictures: [CreatePictureInput]
    plate: String!
    color: String!
    brand: String!
    model: String!
    enabled: Boolean
    createdById: Int
    updatedById: Int
  }

  input UpdateTruckInput {
    pictures: [UpdatePictureInput]
    plate: String
    color: String
    brand: String
    model: String
    enabled: Boolean
    createdById: Int
    updatedById: Int
  }

  type Mutation {
    createTruck(input: CreateTruckInput!): Truck! @requireAuth
    updateTruck(id: Int!, input: UpdateTruckInput!): Truck! @requireAuth
    deleteTruck(id: Int!): Truck! @requireAuth
  }
`
