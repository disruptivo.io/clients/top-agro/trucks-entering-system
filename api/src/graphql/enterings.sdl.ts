export const schema = gql`
  type Entering {
    id: Int!
    enteringAt: DateTime!
    exitAt: DateTime
    driver: Driver!
    driverId: Int!
    truck: Truck!
    truckId: Int!
    createdBy: User
    createdById: Int
    updatedBy: User
    updatedById: Int
  }

  type Enterings {
    data: [Entering!]!
    count: Int!
    isEmpty: Boolean
  }

  type Query {
    enterings(
      page: Int!
      size: Int!
      filter: String
      startDate: String
      endDate: String
    ): Enterings! @requireAuth
    entering(id: Int!): Entering @requireAuth
  }

  input CreateEnteringInput {
    enteringAt: DateTime!
    exitAt: DateTime
    driverId: Int!
    truckId: Int!
    createdById: Int
    updatedById: Int
  }

  input UpdateEnteringInput {
    enteringAt: DateTime
    exitAt: DateTime
    driverId: Int
    truckId: Int
    createdById: Int
    updatedById: Int
  }

  type Mutation {
    createEntering(input: CreateEnteringInput!): Entering! @requireAuth
    updateEntering(id: Int!, input: UpdateEnteringInput!): Entering!
      @requireAuth
    deleteEntering(id: Int!): Entering! @requireAuth
  }
`
