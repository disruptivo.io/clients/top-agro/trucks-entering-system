export const schema = gql`
  type Driver {
    id: Int!
    firstName: String!
    lastName: String!
    identification: String!
    pictures: [Picture]
    dateOfBirth: DateTime
    enabled: Boolean
    createdAt: DateTime
    updatedAt: DateTime
    createdBy: User
    createdById: Int
    updatedBy: User
    updatedById: Int
    organization: Organization
    organizationId: Int
    truck: Truck
    truckId: Int
  }

  type Drivers {
    data: [Driver!]!
    count: Int!
    isEmpty: Boolean
  }

  type Query {
    drivers(
      page: Int!
      size: Int!
      filter: String
      startDate: String
      endDate: String
      enabled: Boolean
    ): Drivers! @requireAuth
    driver(id: Int!): Driver @requireAuth
    driversByTruck(plate: String!): [Driver!]! @requireAuth
  }

  input CreateDriverInput {
    firstName: String!
    lastName: String!
    identification: String!
    pictures: [CreatePictureInput]
    dateOfBirth: DateTime
    enabled: Boolean
    createdById: Int
    updatedById: Int
    organizationId: Int
    truckId: Int
  }

  input UpdateDriverInput {
    firstName: String
    lastName: String
    identification: String
    pictures: [UpdatePictureInput]
    dateOfBirth: DateTime
    enabled: Boolean
    createdById: Int
    updatedById: Int
    organizationId: Int
    truckId: Int
  }

  type Mutation {
    createDriver(input: CreateDriverInput!): Driver! @requireAuth
    updateDriver(id: Int!, input: UpdateDriverInput!): Driver! @requireAuth
    deleteDriver(id: Int!): Driver! @requireAuth
  }
`
