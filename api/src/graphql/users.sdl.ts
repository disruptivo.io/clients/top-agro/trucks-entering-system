export const schema = gql`
  enum Role {
    doorman
    coordinator
    admin
  }

  type User {
    id: Int!
    firstName: String!
    lastName: String!
    username: String!
    hashedPassword: String!
    salt: String!
    resetToken: String
    resetTokenExpiresAt: DateTime
    webAuthnChallenge: String
    enabled: Boolean
    createdAt: DateTime
    updatedAt: DateTime
    createdBy: User
    createdById: Int
    updatedBy: User
    updatedById: Int
    userCreatedBy: [User]!
    userUpdatedBy: [User]!
    organizationCreatedBy: [Organization]!
    organizationUpdatedBy: [Organization]!
    roles: Role
  }

  type Users {
    data: [User!]!
    count: Int!
    isEmpty: Boolean
  }

  type Query {
    users(
      page: Int!
      size: Int!
      filter: String
      startDate: String
      endDate: String
      enabled: Boolean
    ): Users! @requireAuth
    user(id: Int!): User @requireAuth
  }

  input CreateUserInput {
    firstName: String!
    lastName: String!
    username: String!
    hashedPassword: String!
    salt: String!
    resetToken: String
    resetTokenExpiresAt: DateTime
    webAuthnChallenge: String
    enabled: Boolean
    createdById: Int
    updatedById: Int
    roles: Role
  }

  input UpdateUserInput {
    firstName: String
    lastName: String
    username: String
    hashedPassword: String
    salt: String
    resetToken: String
    resetTokenExpiresAt: DateTime
    webAuthnChallenge: String
    enabled: Boolean
    createdById: Int
    updatedById: Int
    roles: Role
  }

  type Mutation {
    createUser(input: CreateUserInput!): User! @requireAuth
    updateUser(id: Int!, input: UpdateUserInput!): User! @requireAuth
    deleteUser(id: Int!): User! @requireAuth
  }
`
