export const schema = gql`
  type Organization {
    id: Int!
    name: String!
    enabled: Boolean!
    createdAt: DateTime!
    updatedAt: DateTime
    createdBy: User!
    createdById: Int!
    updatedBy: User
    updatedById: Int
  }

  type Organizations {
    data: [Organization!]!
    count: Int!
    isEmpty: Boolean
  }

  type Query {
    organizations(
      page: Int!
      size: Int!
      filter: String
      startDate: String
      endDate: String
      enabled: Boolean
    ): Organizations! @requireAuth
    organization(id: Int!): Organization @requireAuth
    organizationsByName(name: String!): [Organization!]! @requireAuth
  }

  input CreateOrganizationInput {
    name: String!
    enabled: Boolean
    createdById: Int!
    updatedById: Int
  }

  input UpdateOrganizationInput {
    name: String
    enabled: Boolean
    createdById: Int
    updatedById: Int!
  }

  type Mutation {
    createOrganization(input: CreateOrganizationInput!): Organization!
      @requireAuth
    updateOrganization(
      id: Int!
      input: UpdateOrganizationInput!
    ): Organization! @requireAuth
  }
`
