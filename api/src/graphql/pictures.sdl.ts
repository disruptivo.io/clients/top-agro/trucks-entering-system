export const schema = gql`
  type Picture {
    id: Int!
    url: String!
    createdAt: DateTime
    updatedAt: DateTime
    createdBy: User
    createdById: Int
    updatedBy: User
    updatedById: Int
    Driver: Driver
    driverId: Int
  }

  type Query {
    pictures: [Picture!]! @requireAuth
    picture(id: Int!): Picture @requireAuth
  }

  input CreatePictureInput {
    url: String!
    createdById: Int
    updatedById: Int
    driverId: Int
  }

  input UpdatePictureInput {
    id: Int
    url: String
    createdById: Int
    updatedById: Int
    driverId: Int
  }

  type Mutation {
    createPicture(input: CreatePictureInput!): Picture! @requireAuth
    updatePicture(id: Int!, input: UpdatePictureInput!): Picture! @requireAuth
    deletePicture(id: Int!): Picture! @requireAuth
  }
`
