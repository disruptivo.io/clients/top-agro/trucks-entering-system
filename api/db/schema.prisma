datasource db {
  provider             = "mysql"
  url                  = env("DATABASE_URL")
  referentialIntegrity = "prisma"
}

generator client {
  provider        = "prisma-client-js"
  binaryTargets   = "native"
  previewFeatures = ["referentialIntegrity"]
}

enum Role {
  doorman
  coordinator
  admin
}

model User {
  id                  Int       @id @default(autoincrement())
  firstName           String
  lastName            String
  username            String    @unique
  hashedPassword      String
  salt                String
  resetToken          String?
  resetTokenExpiresAt DateTime?
  webAuthnChallenge   String?   @unique
  roles               Role      @default(doorman)
  // credentials         UserCredential[] TODO: enable when this issue is solved https://github.com/redwoodjs/redwood/pull/5874
  enabled             Boolean?  @default(true)
  createdAt           DateTime? @default(now())
  updatedAt           DateTime? @updatedAt
  // Relations
  createdBy           User?     @relation("userCreatedBy", fields: [createdById], references: [id], onDelete: NoAction, onUpdate: NoAction)
  createdById         Int?
  updatedBy           User?     @relation("userUpdatedBy", fields: [updatedById], references: [id], onDelete: NoAction, onUpdate: NoAction)
  updatedById         Int?

  userCreatedBy         User[]         @relation("userCreatedBy")
  userUpdatedBy         User[]         @relation("userUpdatedBy")
  organizationCreatedBy Organization[] @relation("organizationCreatedBy")
  organizationUpdatedBy Organization[] @relation("organizationUpdatedBy")
  driverCreatedBy       Driver[]       @relation("driverCreatedBy")
  driverUpdatedBy       Driver[]       @relation("driverUpdatedBy")
  truckCreatedBy        Truck[]        @relation("truckCreatedBy")
  truckUpdatedBy        Truck[]        @relation("truckUpdatedBy")
  enteringCreatedBy     Entering[]     @relation("enteringCreatedBy")
  enteringUpdatedBy     Entering[]     @relation("enteringUpdatedBy")
  pictureCreatedBy      Picture[]      @relation("pictureCreatedBy")
  pictureUpdatedBy      Picture[]      @relation("pictureUpdatedBy")
}

model UserCredential {
  id         String  @id
  userId     Int
  // user       User    @relation(fields: [userId], references: [id]) TODO: enable when this issue is solved https://github.com/redwoodjs/redwood/pull/5874
  publicKey  Bytes
  transports String?
  counter    BigInt
}

model Organization {
  id          Int       @id @default(autoincrement())
  name        String    @unique
  enabled     Boolean?  @default(true)
  createdAt   DateTime? @default(now())
  updatedAt   DateTime? @updatedAt
  // Relations
  createdBy   User?     @relation("organizationCreatedBy", fields: [createdById], references: [id])
  createdById Int?
  updatedBy   User?     @relation("organizationUpdatedBy", fields: [updatedById], references: [id])
  updatedById Int?
  driver      Driver[]  @relation("driverOrganization")
}

model Driver {
  id             Int          @id @default(autoincrement())
  firstName      String
  lastName       String
  identification String       @unique
  dateOfBirth    DateTime?
  enabled        Boolean?     @default(true)
  createdAt      DateTime?    @default(now())
  updatedAt      DateTime?    @updatedAt
  // Relations
  createdBy      User?        @relation("driverCreatedBy", fields: [createdById], references: [id])
  createdById    Int?
  updatedBy      User?        @relation("driverUpdatedBy", fields: [updatedById], references: [id])
  updatedById    Int?
  organization   Organization @relation("driverOrganization", fields: [organizationId], references: [id])
  organizationId Int
  truck          Truck?       @relation("driverTruck", fields: [truckId], references: [id])
  truckId        Int?

  enterings Entering[] @relation("enteringDriver")
  pictures  Picture[]  @relation("pictureDriver")
}

model Truck {
  id          Int       @id @default(autoincrement())
  plate       String    @unique
  color       String
  brand       String
  model       String
  enabled     Boolean?  @default(true)
  createdAt   DateTime? @default(now())
  updatedAt   DateTime? @updatedAt
  // Relations
  createdBy   User?     @relation("truckCreatedBy", fields: [createdById], references: [id])
  createdById Int?
  updatedBy   User?     @relation("truckUpdatedBy", fields: [updatedById], references: [id])
  updatedById Int?

  driver    Driver[]   @relation("driverTruck")
  enterings Entering[] @relation("enteringTruck")
  pictures  Picture[]  @relation("pictureTruck")
}

model Entering {
  id          Int       @id @default(autoincrement())
  enteringAt  DateTime  @default(now())
  exitAt      DateTime?
  // Relations
  driver      Driver    @relation("enteringDriver", fields: [driverId], references: [id])
  driverId    Int
  truck       Truck     @relation("enteringTruck", fields: [truckId], references: [id])
  truckId     Int
  createdBy   User?     @relation("enteringCreatedBy", fields: [createdById], references: [id])
  createdById Int?
  updatedBy   User?     @relation("enteringUpdatedBy", fields: [updatedById], references: [id])
  updatedById Int?
}

model Picture {
  id          Int       @id @default(autoincrement())
  url         String    @db.VarChar(500)
  createdAt   DateTime? @default(now())
  updatedAt   DateTime? @updatedAt
  // Relations
  createdBy   User?     @relation("pictureCreatedBy", fields: [createdById], references: [id])
  createdById Int?
  updatedBy   User?     @relation("pictureUpdatedBy", fields: [updatedById], references: [id])
  updatedById Int?
  Driver      Driver?   @relation("pictureDriver", fields: [driverId], references: [id])
  driverId    Int?
  Truck       Truck?    @relation("pictureTruck", fields: [truckId], references: [id])
  truckId     Int?
}
