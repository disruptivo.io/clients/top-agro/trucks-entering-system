-- DropForeignKey
ALTER TABLE `Driver` DROP FOREIGN KEY `Driver_createdById_fkey`;

-- DropForeignKey
ALTER TABLE `Driver` DROP FOREIGN KEY `Driver_organizationId_fkey`;

-- DropForeignKey
ALTER TABLE `Driver` DROP FOREIGN KEY `Driver_truckId_fkey`;

-- DropForeignKey
ALTER TABLE `Driver` DROP FOREIGN KEY `Driver_updatedById_fkey`;

-- DropForeignKey
ALTER TABLE `Organization` DROP FOREIGN KEY `Organization_createdById_fkey`;

-- DropForeignKey
ALTER TABLE `Organization` DROP FOREIGN KEY `Organization_updatedById_fkey`;

-- DropForeignKey
ALTER TABLE `Truck` DROP FOREIGN KEY `Truck_createdById_fkey`;

-- DropForeignKey
ALTER TABLE `Truck` DROP FOREIGN KEY `Truck_updatedById_fkey`;

-- DropForeignKey
ALTER TABLE `User` DROP FOREIGN KEY `User_createdById_fkey`;

-- DropForeignKey
ALTER TABLE `User` DROP FOREIGN KEY `User_updatedById_fkey`;

-- CreateTable
CREATE TABLE `Entering` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `enteringAt` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `exitAt` DATETIME(3) NULL,
    `driverId` INTEGER NOT NULL,
    `truckId` INTEGER NOT NULL,
    `createdById` INTEGER NULL,
    `updatedById` INTEGER NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
