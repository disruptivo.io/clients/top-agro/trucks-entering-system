/*
  Warnings:

  - Made the column `organizationId` on table `Driver` required. This step will fail if there are existing NULL values in that column.

*/
-- AlterTable
ALTER TABLE `Driver` MODIFY `organizationId` INTEGER NOT NULL;
