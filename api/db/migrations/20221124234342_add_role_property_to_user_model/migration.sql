-- DropIndex
DROP INDEX `Driver_createdById_fkey` ON `Driver`;

-- DropIndex
DROP INDEX `Driver_organizationId_fkey` ON `Driver`;

-- DropIndex
DROP INDEX `Driver_truckId_fkey` ON `Driver`;

-- DropIndex
DROP INDEX `Driver_updatedById_fkey` ON `Driver`;

-- DropIndex
DROP INDEX `Organization_createdById_fkey` ON `Organization`;

-- DropIndex
DROP INDEX `Organization_updatedById_fkey` ON `Organization`;

-- DropIndex
DROP INDEX `Truck_createdById_fkey` ON `Truck`;

-- DropIndex
DROP INDEX `Truck_updatedById_fkey` ON `Truck`;

-- DropIndex
DROP INDEX `User_createdById_fkey` ON `User`;

-- DropIndex
DROP INDEX `User_updatedById_fkey` ON `User`;

-- AlterTable
ALTER TABLE `User` ADD COLUMN `roles` ENUM('doorman', 'coordinator', 'admin') NOT NULL DEFAULT 'doorman';
