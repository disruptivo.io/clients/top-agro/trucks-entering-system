/*
  Warnings:

  - You are about to drop the column `picture` on the `Truck` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE `Truck` DROP COLUMN `picture`;
