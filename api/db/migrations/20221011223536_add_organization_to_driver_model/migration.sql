-- AlterTable
ALTER TABLE `Driver` ADD COLUMN `organizationId` INTEGER NULL;

-- AddForeignKey
ALTER TABLE `Driver` ADD CONSTRAINT `Driver_organizationId_fkey` FOREIGN KEY (`organizationId`) REFERENCES `Organization`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;
