/*
  Warnings:

  - You are about to drop the column `roles` on the `User` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE `User` DROP COLUMN `roles`,
    ADD COLUMN `role` ENUM('doorman', 'coordinator', 'admin') NOT NULL DEFAULT 'doorman';
