# Trucks entering system
This projects was generated using [RedwoodJS](https://redwoodjs.com)

## How to run with Docker?
1. Clone the repo and go to the folder
2. Create an `.env` file based on `.env.example` file and populate it
3. Run `docker  compose up -d`
4. After running it you can open following urls
   1. localhost:8910
   2. localhost:8911/graphql
   3. localhost:5555

:warning
You have to wait some minutes till the whole system is up and running and to stop it you have to run `docker compose down`

## How to run without Docker?
1. Clone the repo and go to the folder
2. Install [Volta](https://volta.sh)
3. Install dependencies
```console
yarn install
```
4. Run the project
   1. You can run without caching using Redwood cli directly
   ```console
   yarn rw dev
   ```
   2. You can run with caching using Nx
   ```console
   npx nx run trucks-entering-system:serve
   ```
## How to commit?
It uses commitizen in order to standardize commit messages, so for committing you have to run the following command
```console
npx cz
```

## How to deploy to Vercel?
It has a pipeline for Gitlab in order to deploy the dashboard to Vercel, but the previous steps were executed before configuring the pipeline

1. Retrieve your [Vercel Access Token](https://vercel.com/support/articles/how-do-i-use-a-vercel-api-access-token)
2. Install the [Vercel CLI](https://vercel.com/cli) and run `vercel login`
3. Inside your folder, run `vercel link` to create a new Vercel project or if you have a project created, you can pull the the project settings by using `vercel pull`
4. Inside the generated `.vercel` folder, save the projectId and orgId from the `project.json`
5. Inside GitLab, add VERCEL_TOKEN, VERCEL_ORG_ ID, and VERCEL_PROJECT_ID as [CI/CD variables](https://docs.gitlab.com/ee/ci/variables/).

### Hints
- This project has a pipeline in order to deploy to Vercel when you push a tag by using the command yarn nx release:(mayor|minor|patch)
- You have to update the build command on Vercel in order to avoid running Prisma migration and Data migration
```
yarn rw deploy vercel --data-migrate=false --prisma=false --build=true
```

## Deploy schema changes to PlanetScale
1. Update prisma.schema for supporting PlanetScale

   Recently, there have been updates made to Planetscale compatability with Prisma. As a result, the referentialIntegrity configuration is now required
  ```console
  datasource db {
    provider = "mysql"
    url      = env("DATABASE_URL")
    referentialIntegrity = "prisma"
  }

  generator client {
    provider = "prisma-client-js"
    previewFeatures = ["referentialIntegrity"]
  }
  ```
2. Get credentials for Prisma from PlanetScale ui
3. Set the `DATABASE_URL` env var
`DATABASE_URL=mysql://user:password@host/database`
4. Create a branch
```
pscale branch create YOUR_DATABASE NEW_BRANCH
```
5. Connect to new created branch
`pscale connect YOUR_DATABASE YOUR_BRANCH --port=3309`
6. Run `yarn nx deploy:db trucks-entering-system`
7. Create deploy request
```
pscale deploy-request create YOUR_DATABASE NEW_BRANCH
```

### Hints
- First time you can push changes directly to your main branch

## Seed data to PlanetScale
1. Set the `DATABASE_URL`, `PASSWORD`, `USERNAME` and `SESSION_SECRET` env vars as follow
`SESSION_SECRET=XXX DATABASE_URL=XXX PASSWORD=XXX USERNAME=XXX yarn nx db:seed trucks-entering-system`
2. Run `yarn nx db:seed trucks-entering-system`